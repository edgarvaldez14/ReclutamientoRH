﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class User
    {
        [Key]
        public int ID { get; set; }
        public string Usuario { get; set; }
        public string Contrasenia { get; set; }


    }
}
