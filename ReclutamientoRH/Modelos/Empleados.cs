﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Empleados
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpleadoID { get; set; }        
        public string NombreEmpleado { get; set; }
        public string ApellidoEmpleado { get; set; }
        public long CedulaEmpleado { get; set; }
        public int NacionalidadIDEmpleado { get; set; }
        public DateTime FechaContratacionEmpleado { get; set; }
        public int PosicionID { get; set; }
        public int SalarioEmpleado { get; set; }
        public string EstadoEmpleado { get; set; }
        

    }
}
