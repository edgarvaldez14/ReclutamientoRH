﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Nacionalidades
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NacionalidadID { get; set; }
        public string NombreNacionalidad { get; set; }

        [ForeignKey("NacionalidadID")]
        public virtual ICollection<Candidatos> Candidatos { get; set; }
    }
}
