﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Departamentos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartamentoID { get; set; }
        public string NombreDepartamento { get; set; }
        public string EstadoDepartamento { get; set; }

        [ForeignKey("DepartamentoID")]
        public virtual ICollection<Posiciones> Posiciones { get; set; }
    }
}
    