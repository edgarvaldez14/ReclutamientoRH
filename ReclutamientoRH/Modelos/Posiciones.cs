﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Posiciones

    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PosicionID { get; set; }
        public string NombrePosicion { get; set; }
        public string RiesgoPosicion { get; set; }
        public int SalarioMinimoPosicion { get; set; }
        public int SalaroMaximoPosicion { get; set; }
        public string EstadoPosicion { get; set; }
        
        public int DepartamentoID { get; set; }
        public virtual Departamentos Departamento { get; set; }

        [ForeignKey("PosicionID")]
        public virtual ICollection<Candidatos> Candidatos { get; set; }
        

        
        
    }
}
