﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Candidatos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CandidatoID { get; set; }
        public string NombreCandidato { get; set; }
        public string ApellidosCandidato { get; set; }
        public long CedulaCandidato { get; set; }
        public int SalarioCandidato { get; set; }
        public string RecomendadoPor { get; set; }
        public string EstadoCandidato { get; set; }

        //Relacion de uno a Mucho
        public int NacionalidadID { get; set; }
        public int CapacitacionID { get; set; }
        public int CompetenciaID { get; set; }
        
        public int IdiomaID { get; set; }
        public int PosicionID { get; set; }

        //Virtual relacion de uno a mucho
        //public virtual  Nacionalidades Nacionalidad { get; set; }
        //public virtual Posiciones Posiciones { get; set; }
        //public virtual Capacitaciones Capacitaciones { get; set; }
        //public virtual  Competencias Competencias { get; set; }
        //public virtual Experiencias Experiencia { get; set; }
        //public virtual Idiomas Idiomas { get; set; }

        [ForeignKey("CandidatoID")]
        
        public virtual ICollection<Experiencias> Experiencias { get; set; }
        


    }
}
