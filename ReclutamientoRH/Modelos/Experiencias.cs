﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Experiencias
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ExperienciaID { get; set; }
        public int CandidatoID { get; set; }
        public string OrganizacionExperiencia { get; set; }
        public int PosicionID { get; set; }
        
        public DateTime FechaEntradaExperiencia { get; set; }
        public DateTime fechaSalidaExperiencia { get; set; }
        public int SalarioExperiencia { get; set; }

        
    }
}
