﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Capacitaciones
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CapacitacionID { get; set; }
        public string DescripcionCapacitacion { get; set; }
        public string NivelCapacitacion { get; set; }
        public DateTime FechaDeInicioCapacitacion { get; set; }
        public DateTime FechaFinCapacitacion { get; set; }

        public int InstitucionID { get; set; }
        public virtual Instituciones Instituciones { get; set; }

        [ForeignKey("CapacitacionID")]
        public virtual ICollection<Candidatos> Candidatos { get; set; }

    }
}
