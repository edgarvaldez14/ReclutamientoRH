﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Competencias
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompetenciaID { get; set; }
        public string NombreCompetencia { get; set; }
        public string EstadoCompetencia { get; set; }

        [ForeignKey ("CompetenciaID")]
        public virtual ICollection<Candidatos> Candidatos { get; set; }
    }
}
