﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Instituciones
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InstitucionID { get; set; }
        public string NombreInstitucion { get; set; }
        public string PaisInstitucion { get; set; }
        public string ProvinciaInstitucion { get; set; }
        public string MunicipioInstitucion { get; set; }
        public string DireccionInstitucion { get; set; }

        [ForeignKey("InstitucionID")]
        public virtual ICollection<Capacitaciones> Capacitaciones { get; set; }
    }
}
