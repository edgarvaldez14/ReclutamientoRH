﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoRH.Modelos
{
    public class Idiomas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdiomaID { get; set; }
        public string NombreIdioma { get; set; }
        public string EstadoIdioma { get; set; }

        [ForeignKey("IdiomaID")]
        public virtual ICollection<Candidatos> Candidatos { get; set; }

    }
}
