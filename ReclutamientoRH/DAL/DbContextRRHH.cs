using ReclutamientoRH.Modelos;

namespace ReclutamientoRH.DAL

{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DbContextRRHH : System.Data.Entity.DbContext
    {
        // Your context has been configured to use a 'ReclutamientoDbContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ReclutamientoRH.DAL.ReclutamientoDbContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ReclutamientoDbContext' 
        // connection string in the application configuration file.
        public DbContextRRHH()
            : base("name=DbContextRRHH")
        {
        }
        
        public virtual DbSet<Candidatos> Candidatos { get; set; }
        public virtual DbSet<Capacitaciones> Capacitaciones { get; set; }
        public virtual DbSet<Competencias> Competencias { get; set; }
        public virtual DbSet<Departamentos> Departamentos { get; set; }
        public virtual DbSet<Empleados> Empleados { get; set; }
        public virtual DbSet<Experiencias> Experiencias { get; set; }
        public virtual DbSet<Idiomas> Idiomas { get; set; }
        public virtual DbSet<Instituciones> Instituciones { get; set; }
        public virtual DbSet<Nacionalidades> Nacionalidades { get; set; }
        public virtual DbSet<Posiciones> Posiciones { get; set; }
        public virtual DbSet<User> User { get; set; }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}