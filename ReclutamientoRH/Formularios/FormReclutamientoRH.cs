﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormReclutamientoRH : MaterialForm
    {
        public FormReclutamientoRH()
        {
            InitializeComponent();

            
          
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void ReclutamientoRH_Load(object sender, EventArgs e)
        {

        }

        private void btnDepartamentos_Click(object sender, EventArgs e)
        {

            FormDepartamentos dep = new FormDepartamentos();
            dep.Show();
            this.Hide();
        }

        

        internal class Modelos
        {
        }

        private void btnPosiciones_Click(object sender, EventArgs e)
        {
            
            FormPosiciones pos = new FormPosiciones();
            pos.Show();
            this.Hide();
        }

        

        private void btnCapacitaciones_Click(object sender, EventArgs e)
        {
            FormCapacitaciones cap = new FormCapacitaciones();
            cap.Show();
            this.Hide();
        }

        private void btnNacionalidad_Click(object sender, EventArgs e)
        {
            FormNacionalidades nc = new FormNacionalidades();
            nc.Show();
            this.Hide();

        }

        private void btnIdiomas_Click(object sender, EventArgs e)
        {
            FormIdiomas idm = new FormIdiomas();
            idm.Show();
            this.Hide();
        }

        private void btnExperiencias_Click(object sender, EventArgs e)
        {
            FormExperiencia ex = new FormExperiencia();
            ex.Show();
            this.Hide();
        }

        private void btnCompetencias_Click(object sender, EventArgs e)
        {
            FormCompetencias cm = new FormCompetencias();
            cm.Show();
            this.Hide();
        }

        private void btnCandidatos_Click(object sender, EventArgs e)
        {
            FormCandidato cnd = new FormCandidato();
            cnd.Show();
            this.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnEmpleados_Click(object sender, EventArgs e)
        {
            FormEmpleados em = new FormEmpleados();
            em.Show();
            this.Hide();
        }

        private void btnCerrarSeccion_Click(object sender, EventArgs e)
        {
            FormLogin l = new FormLogin();
            l.Show();
            this.Close();
        }
    }
}
