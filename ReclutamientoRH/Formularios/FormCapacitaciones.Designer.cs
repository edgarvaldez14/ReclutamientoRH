﻿namespace ReclutamientoRH.Formularios
{
    partial class FormCapacitaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnVolver = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEditar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtFiltroCapacitaciones = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lbPosiciones = new MaterialSkin.Controls.MaterialLabel();
            this.boxPosiciones = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.capacitacionIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcionCapacitacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Institucion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InstitucionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nivelCapacitacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDeInicioCapacitacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaFinCapacitacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capacitacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnIntituciones = new MaterialSkin.Controls.MaterialFlatButton();
            this.boxPosiciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitacionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.AutoSize = true;
            this.btnVolver.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVolver.Depth = 0;
            this.btnVolver.Location = new System.Drawing.Point(857, 537);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnVolver.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Primary = false;
            this.btnVolver.Size = new System.Drawing.Size(63, 36);
            this.btnVolver.TabIndex = 20;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Location = new System.Drawing.Point(714, 537);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(74, 36);
            this.btnEliminar.TabIndex = 19;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.AutoSize = true;
            this.btnEditar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditar.Depth = 0;
            this.btnEditar.Location = new System.Drawing.Point(647, 537);
            this.btnEditar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEditar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Primary = false;
            this.btnEditar.Size = new System.Drawing.Size(59, 36);
            this.btnEditar.TabIndex = 18;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(565, 537);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 17;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtFiltroCapacitaciones
            // 
            this.txtFiltroCapacitaciones.Depth = 0;
            this.txtFiltroCapacitaciones.Hint = "";
            this.txtFiltroCapacitaciones.Location = new System.Drawing.Point(804, 94);
            this.txtFiltroCapacitaciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtFiltroCapacitaciones.Name = "txtFiltroCapacitaciones";
            this.txtFiltroCapacitaciones.PasswordChar = '\0';
            this.txtFiltroCapacitaciones.SelectedText = "";
            this.txtFiltroCapacitaciones.SelectionLength = 0;
            this.txtFiltroCapacitaciones.SelectionStart = 0;
            this.txtFiltroCapacitaciones.Size = new System.Drawing.Size(200, 23);
            this.txtFiltroCapacitaciones.TabIndex = 16;
            this.txtFiltroCapacitaciones.UseSystemPasswordChar = false;
            this.txtFiltroCapacitaciones.Click += new System.EventHandler(this.txtFiltroCapacitaciones_Click);
            this.txtFiltroCapacitaciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltroCapacitaciones_KeyPress);
            // 
            // lbPosiciones
            // 
            this.lbPosiciones.AutoSize = true;
            this.lbPosiciones.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbPosiciones.Depth = 0;
            this.lbPosiciones.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbPosiciones.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbPosiciones.Location = new System.Drawing.Point(697, 94);
            this.lbPosiciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbPosiciones.Name = "lbPosiciones";
            this.lbPosiciones.Size = new System.Drawing.Size(101, 19);
            this.lbPosiciones.TabIndex = 15;
            this.lbPosiciones.Text = "Capacitacion:";
            // 
            // boxPosiciones
            // 
            this.boxPosiciones.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.boxPosiciones.Controls.Add(this.dataGridView1);
            this.boxPosiciones.Location = new System.Drawing.Point(140, 134);
            this.boxPosiciones.Name = "boxPosiciones";
            this.boxPosiciones.Size = new System.Drawing.Size(865, 394);
            this.boxPosiciones.TabIndex = 14;
            this.boxPosiciones.TabStop = false;
            this.boxPosiciones.Text = "Capacitaciones";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.capacitacionIDDataGridViewTextBoxColumn,
            this.descripcionCapacitacionDataGridViewTextBoxColumn,
            this.Institucion,
            this.InstitucionID,
            this.nivelCapacitacionDataGridViewTextBoxColumn,
            this.fechaDeInicioCapacitacionDataGridViewTextBoxColumn,
            this.fechaFinCapacitacionDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.capacitacionesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(853, 346);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // capacitacionIDDataGridViewTextBoxColumn
            // 
            this.capacitacionIDDataGridViewTextBoxColumn.DataPropertyName = "CapacitacionID";
            this.capacitacionIDDataGridViewTextBoxColumn.HeaderText = "Codigo";
            this.capacitacionIDDataGridViewTextBoxColumn.Name = "capacitacionIDDataGridViewTextBoxColumn";
            this.capacitacionIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.capacitacionIDDataGridViewTextBoxColumn.Width = 60;
            // 
            // descripcionCapacitacionDataGridViewTextBoxColumn
            // 
            this.descripcionCapacitacionDataGridViewTextBoxColumn.DataPropertyName = "DescripcionCapacitacion";
            this.descripcionCapacitacionDataGridViewTextBoxColumn.HeaderText = "Descripcion";
            this.descripcionCapacitacionDataGridViewTextBoxColumn.Name = "descripcionCapacitacionDataGridViewTextBoxColumn";
            this.descripcionCapacitacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.descripcionCapacitacionDataGridViewTextBoxColumn.Width = 250;
            // 
            // Institucion
            // 
            this.Institucion.DataPropertyName = "NombreInstitucion";
            this.Institucion.HeaderText = "Institucion";
            this.Institucion.Name = "Institucion";
            this.Institucion.ReadOnly = true;
            this.Institucion.Width = 150;
            // 
            // InstitucionID
            // 
            this.InstitucionID.DataPropertyName = "InstitucionID";
            this.InstitucionID.HeaderText = "InstitucionID";
            this.InstitucionID.Name = "InstitucionID";
            this.InstitucionID.ReadOnly = true;
            this.InstitucionID.Visible = false;
            // 
            // nivelCapacitacionDataGridViewTextBoxColumn
            // 
            this.nivelCapacitacionDataGridViewTextBoxColumn.DataPropertyName = "NivelCapacitacion";
            this.nivelCapacitacionDataGridViewTextBoxColumn.HeaderText = "Nivel de Capacitacion";
            this.nivelCapacitacionDataGridViewTextBoxColumn.Name = "nivelCapacitacionDataGridViewTextBoxColumn";
            this.nivelCapacitacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.nivelCapacitacionDataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaDeInicioCapacitacionDataGridViewTextBoxColumn
            // 
            this.fechaDeInicioCapacitacionDataGridViewTextBoxColumn.DataPropertyName = "FechaDeInicioCapacitacion";
            this.fechaDeInicioCapacitacionDataGridViewTextBoxColumn.HeaderText = "Fecha Inicio";
            this.fechaDeInicioCapacitacionDataGridViewTextBoxColumn.Name = "fechaDeInicioCapacitacionDataGridViewTextBoxColumn";
            this.fechaDeInicioCapacitacionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaFinCapacitacionDataGridViewTextBoxColumn
            // 
            this.fechaFinCapacitacionDataGridViewTextBoxColumn.DataPropertyName = "FechaFinCapacitacion";
            this.fechaFinCapacitacionDataGridViewTextBoxColumn.HeaderText = "Fecha Fin";
            this.fechaFinCapacitacionDataGridViewTextBoxColumn.Name = "fechaFinCapacitacionDataGridViewTextBoxColumn";
            this.fechaFinCapacitacionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // capacitacionesBindingSource
            // 
            this.capacitacionesBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Capacitaciones);
            // 
            // btnIntituciones
            // 
            this.btnIntituciones.AutoSize = true;
            this.btnIntituciones.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnIntituciones.Depth = 0;
            this.btnIntituciones.Location = new System.Drawing.Point(146, 537);
            this.btnIntituciones.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnIntituciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnIntituciones.Name = "btnIntituciones";
            this.btnIntituciones.Primary = false;
            this.btnIntituciones.Size = new System.Drawing.Size(250, 36);
            this.btnIntituciones.TabIndex = 23;
            this.btnIntituciones.Text = "Instituciones de Capacitaciones";
            this.btnIntituciones.UseVisualStyleBackColor = true;
            this.btnIntituciones.Click += new System.EventHandler(this.btnIntituciones_Click);
            // 
            // FormCapacitaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 660);
            this.Controls.Add(this.btnIntituciones);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtFiltroCapacitaciones);
            this.Controls.Add(this.lbPosiciones);
            this.Controls.Add(this.boxPosiciones);
            this.Name = "FormCapacitaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario de Capacitaciones";
            this.Load += new System.EventHandler(this.FormCapacitaciones_Load);
            this.boxPosiciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitacionesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnVolver;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialFlatButton btnEditar;
        private MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtFiltroCapacitaciones;
        private MaterialSkin.Controls.MaterialLabel lbPosiciones;
        private System.Windows.Forms.GroupBox boxPosiciones;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource capacitacionesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn institucionesDataGridViewTextBoxColumn;
        private MaterialSkin.Controls.MaterialFlatButton btnIntituciones;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacitacionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcionCapacitacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Institucion;
        private System.Windows.Forms.DataGridViewTextBoxColumn InstitucionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn nivelCapacitacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDeInicioCapacitacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaFinCapacitacionDataGridViewTextBoxColumn;
    }
}