﻿namespace ReclutamientoRH.Formularios
{
    partial class FormReclutamientoRH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnCandidatos = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnDepartamentos = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnCompetencias = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnCapacitaciones = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEmpleados = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnIdiomas = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnPosiciones = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnNacionalidad = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pctReclutamiento = new System.Windows.Forms.PictureBox();
            this.button1 = new MetroFramework.Controls.MetroButton();
            this.btnCerrarSeccion = new MetroFramework.Controls.MetroButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctReclutamiento)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(211, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(720, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bienvenidos A la App de Reclutamiento y seleccion de RH";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnCandidatos
            // 
            this.btnCandidatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCandidatos.AutoSize = true;
            this.btnCandidatos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCandidatos.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnCandidatos.Depth = 0;
            this.btnCandidatos.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCandidatos.Location = new System.Drawing.Point(25, 70);
            this.btnCandidatos.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCandidatos.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCandidatos.Name = "btnCandidatos";
            this.btnCandidatos.Primary = false;
            this.btnCandidatos.Size = new System.Drawing.Size(97, 36);
            this.btnCandidatos.TabIndex = 14;
            this.btnCandidatos.Text = "Candidatos";
            this.btnCandidatos.UseVisualStyleBackColor = false;
            this.btnCandidatos.Click += new System.EventHandler(this.btnCandidatos_Click);
            // 
            // btnDepartamentos
            // 
            this.btnDepartamentos.AutoSize = true;
            this.btnDepartamentos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDepartamentos.Depth = 0;
            this.btnDepartamentos.Location = new System.Drawing.Point(25, 310);
            this.btnDepartamentos.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnDepartamentos.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnDepartamentos.Name = "btnDepartamentos";
            this.btnDepartamentos.Primary = false;
            this.btnDepartamentos.Size = new System.Drawing.Size(128, 36);
            this.btnDepartamentos.TabIndex = 15;
            this.btnDepartamentos.Text = "Departamentos";
            this.btnDepartamentos.UseVisualStyleBackColor = true;
            this.btnDepartamentos.Click += new System.EventHandler(this.btnDepartamentos_Click);
            // 
            // btnCompetencias
            // 
            this.btnCompetencias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCompetencias.AutoSize = true;
            this.btnCompetencias.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCompetencias.Depth = 0;
            this.btnCompetencias.Location = new System.Drawing.Point(25, 214);
            this.btnCompetencias.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCompetencias.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCompetencias.Name = "btnCompetencias";
            this.btnCompetencias.Primary = false;
            this.btnCompetencias.Size = new System.Drawing.Size(115, 36);
            this.btnCompetencias.TabIndex = 17;
            this.btnCompetencias.Text = "Competencias";
            this.btnCompetencias.UseVisualStyleBackColor = true;
            this.btnCompetencias.Click += new System.EventHandler(this.btnCompetencias_Click);
            // 
            // btnCapacitaciones
            // 
            this.btnCapacitaciones.AutoSize = true;
            this.btnCapacitaciones.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCapacitaciones.Depth = 0;
            this.btnCapacitaciones.Location = new System.Drawing.Point(25, 118);
            this.btnCapacitaciones.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCapacitaciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCapacitaciones.Name = "btnCapacitaciones";
            this.btnCapacitaciones.Primary = false;
            this.btnCapacitaciones.Size = new System.Drawing.Size(126, 36);
            this.btnCapacitaciones.TabIndex = 16;
            this.btnCapacitaciones.Text = "Capacitaciones";
            this.btnCapacitaciones.UseVisualStyleBackColor = true;
            this.btnCapacitaciones.Click += new System.EventHandler(this.btnCapacitaciones_Click);
            // 
            // btnEmpleados
            // 
            this.btnEmpleados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEmpleados.AutoSize = true;
            this.btnEmpleados.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEmpleados.Depth = 0;
            this.btnEmpleados.Location = new System.Drawing.Point(25, 22);
            this.btnEmpleados.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEmpleados.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEmpleados.Name = "btnEmpleados";
            this.btnEmpleados.Primary = false;
            this.btnEmpleados.Size = new System.Drawing.Size(92, 36);
            this.btnEmpleados.TabIndex = 18;
            this.btnEmpleados.Text = "Empleados";
            this.btnEmpleados.UseVisualStyleBackColor = true;
            this.btnEmpleados.Click += new System.EventHandler(this.btnEmpleados_Click);
            // 
            // btnIdiomas
            // 
            this.btnIdiomas.AutoSize = true;
            this.btnIdiomas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnIdiomas.Depth = 0;
            this.btnIdiomas.Location = new System.Drawing.Point(25, 166);
            this.btnIdiomas.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnIdiomas.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnIdiomas.Name = "btnIdiomas";
            this.btnIdiomas.Primary = false;
            this.btnIdiomas.Size = new System.Drawing.Size(199, 36);
            this.btnIdiomas.TabIndex = 20;
            this.btnIdiomas.Text = "Configuracion de Idiomas";
            this.btnIdiomas.UseVisualStyleBackColor = true;
            this.btnIdiomas.Click += new System.EventHandler(this.btnIdiomas_Click);
            // 
            // btnPosiciones
            // 
            this.btnPosiciones.AutoSize = true;
            this.btnPosiciones.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPosiciones.Depth = 0;
            this.btnPosiciones.Location = new System.Drawing.Point(25, 262);
            this.btnPosiciones.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnPosiciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPosiciones.Name = "btnPosiciones";
            this.btnPosiciones.Primary = false;
            this.btnPosiciones.Size = new System.Drawing.Size(90, 36);
            this.btnPosiciones.TabIndex = 23;
            this.btnPosiciones.Text = "Posiciones";
            this.btnPosiciones.UseVisualStyleBackColor = true;
            this.btnPosiciones.Click += new System.EventHandler(this.btnPosiciones_Click);
            // 
            // btnNacionalidad
            // 
            this.btnNacionalidad.AutoSize = true;
            this.btnNacionalidad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNacionalidad.Depth = 0;
            this.btnNacionalidad.Location = new System.Drawing.Point(25, 358);
            this.btnNacionalidad.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnNacionalidad.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNacionalidad.Name = "btnNacionalidad";
            this.btnNacionalidad.Primary = false;
            this.btnNacionalidad.Size = new System.Drawing.Size(126, 36);
            this.btnNacionalidad.TabIndex = 22;
            this.btnNacionalidad.Text = "Nacionalidades";
            this.btnNacionalidad.UseVisualStyleBackColor = true;
            this.btnNacionalidad.Click += new System.EventHandler(this.btnNacionalidad_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.btnCandidatos);
            this.groupBox1.Controls.Add(this.btnPosiciones);
            this.groupBox1.Controls.Add(this.btnCapacitaciones);
            this.groupBox1.Controls.Add(this.btnDepartamentos);
            this.groupBox1.Controls.Add(this.btnNacionalidad);
            this.groupBox1.Controls.Add(this.btnCompetencias);
            this.groupBox1.Controls.Add(this.btnIdiomas);
            this.groupBox1.Controls.Add(this.btnEmpleados);
            this.groupBox1.Location = new System.Drawing.Point(48, 139);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 414);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opciones";
            // 
            // pctReclutamiento
            // 
            this.pctReclutamiento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pctReclutamiento.Image = global::ReclutamientoRH.Properties.Resources.reclutamiento_personal;
            this.pctReclutamiento.Location = new System.Drawing.Point(373, 139);
            this.pctReclutamiento.Name = "pctReclutamiento";
            this.pctReclutamiento.Size = new System.Drawing.Size(655, 460);
            this.pctReclutamiento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctReclutamiento.TabIndex = 25;
            this.pctReclutamiento.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(923, 619);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 23);
            this.button1.TabIndex = 26;
            this.button1.Text = "Salir";
            this.button1.UseSelectable = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnCerrarSeccion
            // 
            this.btnCerrarSeccion.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCerrarSeccion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCerrarSeccion.Location = new System.Drawing.Point(775, 619);
            this.btnCerrarSeccion.Name = "btnCerrarSeccion";
            this.btnCerrarSeccion.Size = new System.Drawing.Size(105, 23);
            this.btnCerrarSeccion.TabIndex = 27;
            this.btnCerrarSeccion.Text = "Cerrar Seccion";
            this.btnCerrarSeccion.UseSelectable = true;
            this.btnCerrarSeccion.Click += new System.EventHandler(this.btnCerrarSeccion_Click);
            // 
            // FormReclutamientoRH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1160, 660);
            this.Controls.Add(this.btnCerrarSeccion);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pctReclutamiento);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "FormReclutamientoRH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reclutamiento y Seleccion de RH ";
            this.Load += new System.EventHandler(this.ReclutamientoRH_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctReclutamiento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialFlatButton btnCandidatos;
        private MaterialSkin.Controls.MaterialFlatButton btnDepartamentos;
        private MaterialSkin.Controls.MaterialFlatButton btnCompetencias;
        private MaterialSkin.Controls.MaterialFlatButton btnCapacitaciones;
        private MaterialSkin.Controls.MaterialFlatButton btnEmpleados;
        private MaterialSkin.Controls.MaterialFlatButton btnIdiomas;
        private MaterialSkin.Controls.MaterialFlatButton btnPosiciones;
        private MaterialSkin.Controls.MaterialFlatButton btnNacionalidad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pctReclutamiento;
        private MetroFramework.Controls.MetroButton button1;
        private MetroFramework.Controls.MetroButton btnCerrarSeccion;
    }
}