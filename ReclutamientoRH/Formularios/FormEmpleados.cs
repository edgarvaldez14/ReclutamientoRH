﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using System;
using DGV2Printer; 
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using System.Drawing.Printing;

namespace ReclutamientoRH.Formularios
{
    public partial class FormEmpleados : MaterialForm
    {
        public FormEmpleados()
        {
            InitializeComponent();
        }

        public string FiltroEmp;

        private void FormEmpleados_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                var query = from c in db.Empleados 
                            join n in db.Nacionalidades on c.NacionalidadIDEmpleado equals n.NacionalidadID
                            join p in db.Posiciones on c.PosicionID equals p.PosicionID
                            select new
                            {
                                c.EmpleadoID,
                                c.NombreEmpleado,
                                c.ApellidoEmpleado,
                                c.CedulaEmpleado,
                                c.NacionalidadIDEmpleado,
                                n.NombreNacionalidad,
                                c.FechaContratacionEmpleado,
                                c.PosicionID,
                                p.NombrePosicion,
                                c.SalarioEmpleado,
                                c.EstadoEmpleado



                            };
                dataGridView1.DataSource = query.ToList();
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH rh = new FormReclutamientoRH();
            rh.Show();
            this.Close();
        }

        private void btnTerminacion_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

            
            DataGridViewRow row = this.dataGridView1.SelectedRows[0];
            int id = int.Parse(row.Cells[0].Value.ToString());
            using (DbContextRRHH db = new DbContextRRHH())
            {
                var n = db.Empleados.SingleOrDefault(d => d.EmpleadoID == id);

                n.EstadoEmpleado = "Inactivo";

                var c = db.Candidatos.SingleOrDefault(t => t.CandidatoID == id);
                c.EstadoCandidato = "Inactivo";
                db.SaveChanges();

                dataGridView1.DataSource = db.Empleados.ToList();
            }
            }
            else
            {
                return;
            }
        }
        
        

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            PrintDataGridView pr = new PrintDataGridView(dataGridView1);
            pr.isRightToLeft = false;
            pr.ReportHeader = "Reporte de Empleados";
            pr.ReportFooter = "Reclutamiento y RRHH";
            pr.Print();
        }

        private void txtFiltroEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {

                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroEmpleado.Text == "")
                    {


                        empleadosBindingSource.DataSource = db.Empleados.ToList();




                        dataGridView1.DataSource = db.Empleados.ToList();
                    }
                    else
                    {
                        FiltroEmp = txtFiltroEmpleado.Text;

                        var query = from c in db.Empleados
                                    where c.EmpleadoID.ToString() == FiltroEmp || c.NombreEmpleado == FiltroEmp || c.EstadoEmpleado == FiltroEmp
                                    select new
                                    {
                                        c.EmpleadoID,
                                        c.NombreEmpleado,
                                        c.ApellidoEmpleado,
                                        c.CedulaEmpleado,
                                        //c.NacionalidadIDEmpleado,
                                        c.FechaContratacionEmpleado,
                                        c.PosicionID,
                                        c.SalarioEmpleado,
                                        c.EstadoEmpleado



                                    };
                        dataGridView1.DataSource = query.ToList();
                    
                    }
                };
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
