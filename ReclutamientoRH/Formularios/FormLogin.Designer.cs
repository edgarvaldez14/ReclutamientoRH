﻿namespace ReclutamientoRH.Formularios
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labUsuario = new MaterialSkin.Controls.MaterialLabel();
            this.labContrasenia = new MaterialSkin.Controls.MaterialLabel();
            this.txtUsuario = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtContrasenia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.userBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnEntrar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.linkRegistrar = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // labUsuario
            // 
            this.labUsuario.AutoSize = true;
            this.labUsuario.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labUsuario.Depth = 0;
            this.labUsuario.Font = new System.Drawing.Font("Roboto", 11F);
            this.labUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labUsuario.Location = new System.Drawing.Point(359, 260);
            this.labUsuario.MouseState = MaterialSkin.MouseState.HOVER;
            this.labUsuario.Name = "labUsuario";
            this.labUsuario.Size = new System.Drawing.Size(61, 19);
            this.labUsuario.TabIndex = 0;
            this.labUsuario.Text = "Usuario";
            // 
            // labContrasenia
            // 
            this.labContrasenia.AutoSize = true;
            this.labContrasenia.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labContrasenia.Depth = 0;
            this.labContrasenia.Font = new System.Drawing.Font("Roboto", 11F);
            this.labContrasenia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labContrasenia.Location = new System.Drawing.Point(359, 338);
            this.labContrasenia.MouseState = MaterialSkin.MouseState.HOVER;
            this.labContrasenia.Name = "labContrasenia";
            this.labContrasenia.Size = new System.Drawing.Size(86, 19);
            this.labContrasenia.TabIndex = 1;
            this.labContrasenia.Text = "Contraseña";
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtUsuario.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource, "Usuario", true));
            this.txtUsuario.Depth = 0;
            this.txtUsuario.ForeColor = System.Drawing.SystemColors.Control;
            this.txtUsuario.Hint = "";
            this.txtUsuario.Location = new System.Drawing.Point(363, 283);
            this.txtUsuario.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.PasswordChar = '\0';
            this.txtUsuario.SelectedText = "";
            this.txtUsuario.SelectionLength = 0;
            this.txtUsuario.SelectionStart = 0;
            this.txtUsuario.Size = new System.Drawing.Size(183, 23);
            this.txtUsuario.TabIndex = 2;
            this.txtUsuario.UseSystemPasswordChar = false;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.User);
            // 
            // txtContrasenia
            // 
            this.txtContrasenia.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtContrasenia.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.userBindingSource1, "Contrasenia", true));
            this.txtContrasenia.Depth = 0;
            this.txtContrasenia.Hint = "";
            this.txtContrasenia.Location = new System.Drawing.Point(363, 360);
            this.txtContrasenia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtContrasenia.Name = "txtContrasenia";
            this.txtContrasenia.PasswordChar = '\0';
            this.txtContrasenia.SelectedText = "";
            this.txtContrasenia.SelectionLength = 0;
            this.txtContrasenia.SelectionStart = 0;
            this.txtContrasenia.Size = new System.Drawing.Size(183, 23);
            this.txtContrasenia.TabIndex = 3;
            this.txtContrasenia.UseSystemPasswordChar = true;
            this.txtContrasenia.Click += new System.EventHandler(this.txtContrasenia_Click);
            this.txtContrasenia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContrasenia_KeyPress);
            // 
            // userBindingSource1
            // 
            this.userBindingSource1.DataSource = typeof(ReclutamientoRH.Modelos.User);
            // 
            // btnEntrar
            // 
            this.btnEntrar.AutoSize = true;
            this.btnEntrar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEntrar.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnEntrar.Depth = 0;
            this.btnEntrar.Location = new System.Drawing.Point(482, 417);
            this.btnEntrar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEntrar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Primary = false;
            this.btnEntrar.Size = new System.Drawing.Size(64, 36);
            this.btnEntrar.TabIndex = 4;
            this.btnEntrar.Text = "Entrar";
            this.btnEntrar.UseVisualStyleBackColor = false;
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSalir.Depth = 0;
            this.btnSalir.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnSalir.Location = new System.Drawing.Point(363, 417);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Primary = false;
            this.btnSalir.Size = new System.Drawing.Size(49, 36);
            this.btnSalir.TabIndex = 5;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // linkRegistrar
            // 
            this.linkRegistrar.AutoSize = true;
            this.linkRegistrar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.linkRegistrar.Location = new System.Drawing.Point(833, 76);
            this.linkRegistrar.Name = "linkRegistrar";
            this.linkRegistrar.Size = new System.Drawing.Size(79, 13);
            this.linkRegistrar.TabIndex = 6;
            this.linkRegistrar.TabStop = true;
            this.linkRegistrar.Text = "Registrate Aqui";
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 657);
            this.Controls.Add(this.linkRegistrar);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.txtContrasenia);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.labContrasenia);
            this.Controls.Add(this.labUsuario);
            this.Name = "FormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.FormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel labUsuario;
        private MaterialSkin.Controls.MaterialLabel labContrasenia;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtUsuario;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtContrasenia;
        private MaterialSkin.Controls.MaterialFlatButton btnEntrar;
        private MaterialSkin.Controls.MaterialFlatButton btnSalir;
        private System.Windows.Forms.LinkLabel linkRegistrar;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.BindingSource userBindingSource1;
    }
}