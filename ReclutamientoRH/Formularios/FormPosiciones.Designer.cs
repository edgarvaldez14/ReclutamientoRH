﻿using ReclutamientoRH.Modelos;
namespace ReclutamientoRH.Formularios
{
    partial class FormPosiciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnVolver = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEditar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtFiltroPosiciones = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lbPosiciones = new MaterialSkin.Controls.MaterialLabel();
            this.boxPosiciones = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.posicionIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombrePosicionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartamentoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salarioMinimoPosicionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaroMaximoPosicionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.riesgoPosicionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoPosicionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posicionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.boxPosiciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posicionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.AutoSize = true;
            this.btnVolver.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVolver.Depth = 0;
            this.btnVolver.Location = new System.Drawing.Point(929, 539);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnVolver.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Primary = false;
            this.btnVolver.Size = new System.Drawing.Size(63, 36);
            this.btnVolver.TabIndex = 13;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Location = new System.Drawing.Point(786, 539);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(74, 36);
            this.btnEliminar.TabIndex = 12;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.AutoSize = true;
            this.btnEditar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditar.Depth = 0;
            this.btnEditar.Location = new System.Drawing.Point(719, 539);
            this.btnEditar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEditar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Primary = false;
            this.btnEditar.Size = new System.Drawing.Size(59, 36);
            this.btnEditar.TabIndex = 11;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(637, 539);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 10;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtFiltroPosiciones
            // 
            this.txtFiltroPosiciones.Depth = 0;
            this.txtFiltroPosiciones.Hint = "";
            this.txtFiltroPosiciones.Location = new System.Drawing.Point(804, 88);
            this.txtFiltroPosiciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtFiltroPosiciones.Name = "txtFiltroPosiciones";
            this.txtFiltroPosiciones.PasswordChar = '\0';
            this.txtFiltroPosiciones.SelectedText = "";
            this.txtFiltroPosiciones.SelectionLength = 0;
            this.txtFiltroPosiciones.SelectionStart = 0;
            this.txtFiltroPosiciones.Size = new System.Drawing.Size(200, 23);
            this.txtFiltroPosiciones.TabIndex = 9;
            this.txtFiltroPosiciones.UseSystemPasswordChar = false;
            this.txtFiltroPosiciones.Click += new System.EventHandler(this.txtFiltroPosiciones_Click);
            this.txtFiltroPosiciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltroPosiciones_KeyPress);
            // 
            // lbPosiciones
            // 
            this.lbPosiciones.AutoSize = true;
            this.lbPosiciones.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbPosiciones.Depth = 0;
            this.lbPosiciones.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbPosiciones.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbPosiciones.Location = new System.Drawing.Point(732, 93);
            this.lbPosiciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbPosiciones.Name = "lbPosiciones";
            this.lbPosiciones.Size = new System.Drawing.Size(72, 19);
            this.lbPosiciones.TabIndex = 8;
            this.lbPosiciones.Text = "Posicion:";
            this.lbPosiciones.Click += new System.EventHandler(this.lbPosiciones_Click);
            // 
            // boxPosiciones
            // 
            this.boxPosiciones.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.boxPosiciones.Controls.Add(this.dataGridView1);
            this.boxPosiciones.Location = new System.Drawing.Point(110, 125);
            this.boxPosiciones.Name = "boxPosiciones";
            this.boxPosiciones.Size = new System.Drawing.Size(894, 405);
            this.boxPosiciones.TabIndex = 7;
            this.boxPosiciones.TabStop = false;
            this.boxPosiciones.Text = "Posiciones";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.posicionIDDataGridViewTextBoxColumn,
            this.nombrePosicionDataGridViewTextBoxColumn,
            this.DepartamentoID,
            this.dataGridViewTextBoxColumn1,
            this.salarioMinimoPosicionDataGridViewTextBoxColumn,
            this.salaroMaximoPosicionDataGridViewTextBoxColumn,
            this.riesgoPosicionDataGridViewTextBoxColumn,
            this.estadoPosicionDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.posicionesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(28, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(838, 346);
            this.dataGridView1.TabIndex = 0;
            // 
            // posicionIDDataGridViewTextBoxColumn
            // 
            this.posicionIDDataGridViewTextBoxColumn.DataPropertyName = "PosicionID";
            this.posicionIDDataGridViewTextBoxColumn.HeaderText = "Posicion";
            this.posicionIDDataGridViewTextBoxColumn.Name = "posicionIDDataGridViewTextBoxColumn";
            this.posicionIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nombrePosicionDataGridViewTextBoxColumn
            // 
            this.nombrePosicionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nombrePosicionDataGridViewTextBoxColumn.DataPropertyName = "NombrePosicion";
            this.nombrePosicionDataGridViewTextBoxColumn.HeaderText = "Descripcion";
            this.nombrePosicionDataGridViewTextBoxColumn.Name = "nombrePosicionDataGridViewTextBoxColumn";
            this.nombrePosicionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // DepartamentoID
            // 
            this.DepartamentoID.DataPropertyName = "DepartamentoID";
            this.DepartamentoID.HeaderText = "DepartamentoID";
            this.DepartamentoID.Name = "DepartamentoID";
            this.DepartamentoID.ReadOnly = true;
            this.DepartamentoID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NombreDepartamento";
            this.dataGridViewTextBoxColumn1.HeaderText = "Departamento ";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // salarioMinimoPosicionDataGridViewTextBoxColumn
            // 
            this.salarioMinimoPosicionDataGridViewTextBoxColumn.DataPropertyName = "SalarioMinimoPosicion";
            this.salarioMinimoPosicionDataGridViewTextBoxColumn.HeaderText = "Salario Minimo";
            this.salarioMinimoPosicionDataGridViewTextBoxColumn.Name = "salarioMinimoPosicionDataGridViewTextBoxColumn";
            this.salarioMinimoPosicionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // salaroMaximoPosicionDataGridViewTextBoxColumn
            // 
            this.salaroMaximoPosicionDataGridViewTextBoxColumn.DataPropertyName = "SalaroMaximoPosicion";
            this.salaroMaximoPosicionDataGridViewTextBoxColumn.HeaderText = "Salaro Maximo";
            this.salaroMaximoPosicionDataGridViewTextBoxColumn.Name = "salaroMaximoPosicionDataGridViewTextBoxColumn";
            this.salaroMaximoPosicionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // riesgoPosicionDataGridViewTextBoxColumn
            // 
            this.riesgoPosicionDataGridViewTextBoxColumn.DataPropertyName = "RiesgoPosicion";
            this.riesgoPosicionDataGridViewTextBoxColumn.HeaderText = "Riesgo";
            this.riesgoPosicionDataGridViewTextBoxColumn.Name = "riesgoPosicionDataGridViewTextBoxColumn";
            this.riesgoPosicionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoPosicionDataGridViewTextBoxColumn
            // 
            this.estadoPosicionDataGridViewTextBoxColumn.DataPropertyName = "EstadoPosicion";
            this.estadoPosicionDataGridViewTextBoxColumn.HeaderText = "Estado ";
            this.estadoPosicionDataGridViewTextBoxColumn.Name = "estadoPosicionDataGridViewTextBoxColumn";
            this.estadoPosicionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // posicionesBindingSource
            // 
            this.posicionesBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Posiciones);
            // 
            // FormPosiciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 660);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtFiltroPosiciones);
            this.Controls.Add(this.lbPosiciones);
            this.Controls.Add(this.boxPosiciones);
            this.Name = "FormPosiciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario de Posiciones";
            this.Load += new System.EventHandler(this.FormPosiciones_Load);
            this.boxPosiciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posicionesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnVolver;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialFlatButton btnEditar;
        private MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtFiltroPosiciones;
        private MaterialSkin.Controls.MaterialLabel lbPosiciones;
        private System.Windows.Forms.GroupBox boxPosiciones;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn riesgoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salarioMinimoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaroMaximoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn estadoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.BindingSource posicionesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn posicionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombrePosicionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartamentoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn salarioMinimoPosicionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaroMaximoPosicionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn riesgoPosicionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoPosicionDataGridViewTextBoxColumn;
    }
}