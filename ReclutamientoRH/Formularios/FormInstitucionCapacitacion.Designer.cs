﻿namespace ReclutamientoRH.Formularios
{
    partial class FormInstitucionCapacitacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnVolver = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEditar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtFiltroInstitucion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lbPosiciones = new MaterialSkin.Controls.MaterialLabel();
            this.boxPosiciones = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.institucionIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreInstitucionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paisInstitucionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provinciaInstitucionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.municipioInstitucionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionInstitucionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.institucionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.boxPosiciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.institucionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.AutoSize = true;
            this.btnVolver.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVolver.Depth = 0;
            this.btnVolver.Location = new System.Drawing.Point(884, 534);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnVolver.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Primary = false;
            this.btnVolver.Size = new System.Drawing.Size(63, 36);
            this.btnVolver.TabIndex = 20;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Location = new System.Drawing.Point(741, 534);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(74, 36);
            this.btnEliminar.TabIndex = 19;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.AutoSize = true;
            this.btnEditar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditar.Depth = 0;
            this.btnEditar.Location = new System.Drawing.Point(674, 534);
            this.btnEditar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEditar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Primary = false;
            this.btnEditar.Size = new System.Drawing.Size(59, 36);
            this.btnEditar.TabIndex = 18;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(592, 534);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 17;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtFiltroInstitucion
            // 
            this.txtFiltroInstitucion.Depth = 0;
            this.txtFiltroInstitucion.Hint = "";
            this.txtFiltroInstitucion.Location = new System.Drawing.Point(897, 101);
            this.txtFiltroInstitucion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtFiltroInstitucion.Name = "txtFiltroInstitucion";
            this.txtFiltroInstitucion.PasswordChar = '\0';
            this.txtFiltroInstitucion.SelectedText = "";
            this.txtFiltroInstitucion.SelectionLength = 0;
            this.txtFiltroInstitucion.SelectionStart = 0;
            this.txtFiltroInstitucion.Size = new System.Drawing.Size(200, 23);
            this.txtFiltroInstitucion.TabIndex = 16;
            this.txtFiltroInstitucion.UseSystemPasswordChar = false;
            this.txtFiltroInstitucion.Click += new System.EventHandler(this.txtFiltroInstitucion_Click);
            this.txtFiltroInstitucion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltroInstitucion_KeyPress);
            // 
            // lbPosiciones
            // 
            this.lbPosiciones.AutoSize = true;
            this.lbPosiciones.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbPosiciones.Depth = 0;
            this.lbPosiciones.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbPosiciones.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbPosiciones.Location = new System.Drawing.Point(807, 101);
            this.lbPosiciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbPosiciones.Name = "lbPosiciones";
            this.lbPosiciones.Size = new System.Drawing.Size(84, 19);
            this.lbPosiciones.TabIndex = 15;
            this.lbPosiciones.Text = "Institucion:";
            // 
            // boxPosiciones
            // 
            this.boxPosiciones.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.boxPosiciones.Controls.Add(this.dataGridView1);
            this.boxPosiciones.Location = new System.Drawing.Point(56, 130);
            this.boxPosiciones.Name = "boxPosiciones";
            this.boxPosiciones.Size = new System.Drawing.Size(1047, 395);
            this.boxPosiciones.TabIndex = 14;
            this.boxPosiciones.TabStop = false;
            this.boxPosiciones.Text = "Instituciones";
            this.boxPosiciones.Enter += new System.EventHandler(this.boxPosiciones_Enter);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.institucionIDDataGridViewTextBoxColumn,
            this.nombreInstitucionDataGridViewTextBoxColumn,
            this.paisInstitucionDataGridViewTextBoxColumn,
            this.provinciaInstitucionDataGridViewTextBoxColumn,
            this.municipioInstitucionDataGridViewTextBoxColumn,
            this.direccionInstitucionDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.institucionesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(40, 32);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(954, 346);
            this.dataGridView1.TabIndex = 0;
            // 
            // institucionIDDataGridViewTextBoxColumn
            // 
            this.institucionIDDataGridViewTextBoxColumn.DataPropertyName = "InstitucionID";
            this.institucionIDDataGridViewTextBoxColumn.HeaderText = "Codigo";
            this.institucionIDDataGridViewTextBoxColumn.Name = "institucionIDDataGridViewTextBoxColumn";
            this.institucionIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.institucionIDDataGridViewTextBoxColumn.Width = 50;
            // 
            // nombreInstitucionDataGridViewTextBoxColumn
            // 
            this.nombreInstitucionDataGridViewTextBoxColumn.DataPropertyName = "NombreInstitucion";
            this.nombreInstitucionDataGridViewTextBoxColumn.HeaderText = "Nombre de Institucion";
            this.nombreInstitucionDataGridViewTextBoxColumn.Name = "nombreInstitucionDataGridViewTextBoxColumn";
            this.nombreInstitucionDataGridViewTextBoxColumn.ReadOnly = true;
            this.nombreInstitucionDataGridViewTextBoxColumn.Width = 300;
            // 
            // paisInstitucionDataGridViewTextBoxColumn
            // 
            this.paisInstitucionDataGridViewTextBoxColumn.DataPropertyName = "PaisInstitucion";
            this.paisInstitucionDataGridViewTextBoxColumn.HeaderText = "Pais";
            this.paisInstitucionDataGridViewTextBoxColumn.Name = "paisInstitucionDataGridViewTextBoxColumn";
            this.paisInstitucionDataGridViewTextBoxColumn.ReadOnly = true;
            this.paisInstitucionDataGridViewTextBoxColumn.Width = 120;
            // 
            // provinciaInstitucionDataGridViewTextBoxColumn
            // 
            this.provinciaInstitucionDataGridViewTextBoxColumn.DataPropertyName = "ProvinciaInstitucion";
            this.provinciaInstitucionDataGridViewTextBoxColumn.HeaderText = "Provincia o Estado";
            this.provinciaInstitucionDataGridViewTextBoxColumn.Name = "provinciaInstitucionDataGridViewTextBoxColumn";
            this.provinciaInstitucionDataGridViewTextBoxColumn.ReadOnly = true;
            this.provinciaInstitucionDataGridViewTextBoxColumn.Width = 120;
            // 
            // municipioInstitucionDataGridViewTextBoxColumn
            // 
            this.municipioInstitucionDataGridViewTextBoxColumn.DataPropertyName = "MunicipioInstitucion";
            this.municipioInstitucionDataGridViewTextBoxColumn.HeaderText = "Municipio";
            this.municipioInstitucionDataGridViewTextBoxColumn.Name = "municipioInstitucionDataGridViewTextBoxColumn";
            this.municipioInstitucionDataGridViewTextBoxColumn.ReadOnly = true;
            this.municipioInstitucionDataGridViewTextBoxColumn.Width = 120;
            // 
            // direccionInstitucionDataGridViewTextBoxColumn
            // 
            this.direccionInstitucionDataGridViewTextBoxColumn.DataPropertyName = "DireccionInstitucion";
            this.direccionInstitucionDataGridViewTextBoxColumn.HeaderText = "Direccion";
            this.direccionInstitucionDataGridViewTextBoxColumn.Name = "direccionInstitucionDataGridViewTextBoxColumn";
            this.direccionInstitucionDataGridViewTextBoxColumn.ReadOnly = true;
            this.direccionInstitucionDataGridViewTextBoxColumn.Width = 200;
            // 
            // institucionesBindingSource
            // 
            this.institucionesBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Instituciones);
            // 
            // FormInstitucionCapacitacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 660);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtFiltroInstitucion);
            this.Controls.Add(this.lbPosiciones);
            this.Controls.Add(this.boxPosiciones);
            this.Name = "FormInstitucionCapacitacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario Para Intituciones de las Capacitaciones";
            this.Load += new System.EventHandler(this.FormInstitucionCapacitacion_Load);
            this.boxPosiciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.institucionesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnVolver;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialFlatButton btnEditar;
        private MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtFiltroInstitucion;
        private MaterialSkin.Controls.MaterialLabel lbPosiciones;
        private System.Windows.Forms.GroupBox boxPosiciones;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn institucionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreInstitucionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paisInstitucionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn provinciaInstitucionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn municipioInstitucionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccionInstitucionDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource institucionesBindingSource;
    }
}