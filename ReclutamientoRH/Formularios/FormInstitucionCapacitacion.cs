﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReclutamientoRH.Formularios.CRUD;
using ReclutamientoRH.DAL;

namespace ReclutamientoRH.Formularios
{
    public partial class FormInstitucionCapacitacion : MaterialForm
    {
        public FormInstitucionCapacitacion()
        {
            InitializeComponent();
        }

        public string FiltroIns;

        private void FormInstitucionCapacitacion_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                institucionesBindingSource.DataSource = db.Instituciones.ToList();
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarInstitucion agi = new AgregarInstitucion();
            agi.Show();
            this.Hide();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormCapacitaciones cp = new FormCapacitaciones();
            cp.Show();
            this.Close();
        }

        private void txtFiltroInstitucion_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltroInstitucion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroInstitucion.Text == "")
                    {

                        dataGridView1.DataSource = db.Instituciones.ToList();
                        institucionesBindingSource.DataSource = db.Instituciones.ToList();

                    }
                    else
                    {
                        FiltroIns = txtFiltroInstitucion.Text;



                        var result = from d in db.Instituciones
                                     where d.InstitucionID.ToString() == FiltroIns || d.NombreInstitucion == FiltroIns
                                     select d;


                        dataGridView1.DataSource = result.ToList();
                    }
                }
            }
        }

        private void boxPosiciones_Enter(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                    if (bd.Capacitaciones.Any(q => q.InstitucionID == id))
                    {
                        MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                    }
                    else
                    {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Instituciones.SingleOrDefault(d => d.InstitucionID == id);

                            db.Instituciones.Remove(n);

                            db.SaveChanges();

                            dataGridView1.DataSource = db.Instituciones.ToList();
                        }
                    }
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarInstitucion ins = new AgregarInstitucion();
                ins.id = int.Parse(row.Cells[0].Value.ToString());
                ins.nombre = row.Cells[1].Value.ToString();
                ins.pais = row.Cells[2].Value.ToString();
                ins.provincia = row.Cells[3].Value.ToString();
                ins.municipio = row.Cells[4].Value.ToString();
                ins.direccion = row.Cells[5].Value.ToString();
                ins.editar = true;
                ins.groupBox1.Text = "Editar Institucion ";
                ins.Text = "Editar - Institucion";
                ins.btnAgregar.Text = "Editar";
                ins.txtCodigoInstitucion.Enabled = false;
                ins.Show();
                this.Hide();
            }
        }
    }
}
