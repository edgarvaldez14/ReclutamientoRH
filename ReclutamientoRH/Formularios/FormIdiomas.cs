﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.Create;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormIdiomas : MaterialForm
    {
        public FormIdiomas()
        {
            InitializeComponent();
        }
        public string FiltroIdi;

        private void FormIdiomas_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                idiomasBindingSource.DataSource = db.Idiomas.ToList();
            }
        }

        private void idiomasBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH rh = new FormReclutamientoRH();
            rh.Show();
            this.Hide();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarIdioma agr = new AgregarIdioma();
            agr.Show();
            this.Hide();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarIdioma idi = new AgregarIdioma();
                idi.id = int.Parse(row.Cells[0].Value.ToString());
                idi.dep = row.Cells[1].Value.ToString();
                idi.estado = row.Cells[2].Value.ToString();
                idi.editar = true;
                idi.groupBox1.Text = "Editar Idioma";
                idi.txtCodigoIdioma.Enabled = false;
                idi.Text = "Editar Idioma";
                idi.btnAgregar.Text = "Editar";
                idi.Show();
                this.Hide();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                    if (bd.Candidatos.Any(q => q.IdiomaID == id))
                    {
                        MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                    }
                    else
                    {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Idiomas.SingleOrDefault(d => d.IdiomaID == id);

                            db.Idiomas.Remove(n);

                            db.SaveChanges();

                            dataGridView1.DataSource = db.Idiomas.ToList();
                        }
                    }
                }
            }
        }

        private void txtFiltroIdioma_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroIdioma.Text == "")
                    {

                        dataGridView1.DataSource = db.Idiomas.ToList();
                        idiomasBindingSource.DataSource = db.Idiomas.ToList();

                    }
                    else
                    {
                        FiltroIdi = txtFiltroIdioma.Text;



                        var result = from d in db.Idiomas
                                     where d.IdiomaID.ToString() == FiltroIdi || d.NombreIdioma == FiltroIdi
                                     select d;


                        dataGridView1.DataSource = result.ToList();
                    }
                }

            }
        }
    }
}
