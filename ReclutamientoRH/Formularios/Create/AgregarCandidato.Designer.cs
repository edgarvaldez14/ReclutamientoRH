﻿namespace ReclutamientoRH.Formularios.Create
{
    partial class AgregarCandidato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCedula = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.candidatosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.boxEstado = new MetroFramework.Controls.MetroComboBox();
            this.txtRecomendadoPor = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.boxPosicion = new MetroFramework.Controls.MetroComboBox();
            this.boxCompetencia = new MetroFramework.Controls.MetroComboBox();
            this.boxIdioma = new MetroFramework.Controls.MetroComboBox();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.boxNacionalidad = new MetroFramework.Controls.MetroComboBox();
            this.txtSalario = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.boxCapacitacion = new MetroFramework.Controls.MetroComboBox();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtApellidos = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtNombreCondidato = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.NombreCandidato = new MaterialSkin.Controls.MaterialLabel();
            this.txtCodigoCandidato = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblCodigo = new MaterialSkin.Controls.MaterialLabel();
            this.labDescripcion = new MaterialSkin.Controls.MaterialLabel();
            this.candidatosBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSalir.Depth = 0;
            this.btnSalir.Location = new System.Drawing.Point(706, 569);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Primary = false;
            this.btnSalir.Size = new System.Drawing.Size(49, 36);
            this.btnSalir.TabIndex = 15;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.txtCedula);
            this.groupBox1.Controls.Add(this.materialLabel6);
            this.groupBox1.Controls.Add(this.boxEstado);
            this.groupBox1.Controls.Add(this.txtRecomendadoPor);
            this.groupBox1.Controls.Add(this.boxPosicion);
            this.groupBox1.Controls.Add(this.boxCompetencia);
            this.groupBox1.Controls.Add(this.boxIdioma);
            this.groupBox1.Controls.Add(this.materialLabel10);
            this.groupBox1.Controls.Add(this.materialLabel9);
            this.groupBox1.Controls.Add(this.materialLabel7);
            this.groupBox1.Controls.Add(this.materialLabel5);
            this.groupBox1.Controls.Add(this.materialLabel1);
            this.groupBox1.Controls.Add(this.boxNacionalidad);
            this.groupBox1.Controls.Add(this.txtSalario);
            this.groupBox1.Controls.Add(this.boxCapacitacion);
            this.groupBox1.Controls.Add(this.materialLabel4);
            this.groupBox1.Controls.Add(this.txtApellidos);
            this.groupBox1.Controls.Add(this.materialLabel3);
            this.groupBox1.Controls.Add(this.materialLabel2);
            this.groupBox1.Controls.Add(this.txtNombreCondidato);
            this.groupBox1.Controls.Add(this.NombreCandidato);
            this.groupBox1.Controls.Add(this.txtCodigoCandidato);
            this.groupBox1.Controls.Add(this.lblCodigo);
            this.groupBox1.Controls.Add(this.labDescripcion);
            this.groupBox1.Location = new System.Drawing.Point(159, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 467);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Agregar Candidato";
            // 
            // txtCedula
            // 
            this.txtCedula.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatosBindingSource, "ApellidosCandidato", true));
            this.txtCedula.Depth = 0;
            this.txtCedula.Hint = "";
            this.txtCedula.Location = new System.Drawing.Point(203, 103);
            this.txtCedula.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.PasswordChar = '\0';
            this.txtCedula.SelectedText = "";
            this.txtCedula.SelectionLength = 0;
            this.txtCedula.SelectionStart = 0;
            this.txtCedula.Size = new System.Drawing.Size(139, 23);
            this.txtCedula.TabIndex = 38;
            this.txtCedula.UseSystemPasswordChar = false;
            this.txtCedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCedula_KeyPress);
            // 
            // candidatosBindingSource
            // 
            this.candidatosBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Candidatos);
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(96, 107);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(104, 19);
            this.materialLabel6.TabIndex = 37;
            this.materialLabel6.Text = "Identificacion:";
            // 
            // boxEstado
            // 
            this.boxEstado.FormattingEnabled = true;
            this.boxEstado.ItemHeight = 23;
            this.boxEstado.Items.AddRange(new object[] {
            "En Espera",
            "Descartado"});
            this.boxEstado.Location = new System.Drawing.Point(203, 373);
            this.boxEstado.Name = "boxEstado";
            this.boxEstado.Size = new System.Drawing.Size(139, 29);
            this.boxEstado.TabIndex = 36;
            this.boxEstado.UseSelectable = true;
            // 
            // txtRecomendadoPor
            // 
            this.txtRecomendadoPor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatosBindingSource, "RecomendadoPor", true));
            this.txtRecomendadoPor.Depth = 0;
            this.txtRecomendadoPor.Hint = "";
            this.txtRecomendadoPor.Location = new System.Drawing.Point(203, 339);
            this.txtRecomendadoPor.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtRecomendadoPor.Name = "txtRecomendadoPor";
            this.txtRecomendadoPor.PasswordChar = '\0';
            this.txtRecomendadoPor.SelectedText = "";
            this.txtRecomendadoPor.SelectionLength = 0;
            this.txtRecomendadoPor.SelectionStart = 0;
            this.txtRecomendadoPor.Size = new System.Drawing.Size(141, 23);
            this.txtRecomendadoPor.TabIndex = 35;
            this.txtRecomendadoPor.UseSystemPasswordChar = false;
            // 
            // boxPosicion
            // 
            this.boxPosicion.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.candidatosBindingSource, "PosicionID", true));
            this.boxPosicion.DataSource = this.candidatosBindingSource;
            this.boxPosicion.DisplayMember = "PosicionID";
            this.boxPosicion.FormattingEnabled = true;
            this.boxPosicion.ItemHeight = 23;
            this.boxPosicion.Location = new System.Drawing.Point(203, 269);
            this.boxPosicion.Name = "boxPosicion";
            this.boxPosicion.Size = new System.Drawing.Size(139, 29);
            this.boxPosicion.TabIndex = 34;
            this.boxPosicion.UseSelectable = true;
            this.boxPosicion.ValueMember = "PosicionID";
            // 
            // boxCompetencia
            // 
            this.boxCompetencia.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.candidatosBindingSource, "CompetenciaID", true));
            this.boxCompetencia.DataSource = this.candidatosBindingSource;
            this.boxCompetencia.DisplayMember = "CompetenciaID";
            this.boxCompetencia.FormattingEnabled = true;
            this.boxCompetencia.ItemHeight = 23;
            this.boxCompetencia.Location = new System.Drawing.Point(203, 304);
            this.boxCompetencia.Name = "boxCompetencia";
            this.boxCompetencia.Size = new System.Drawing.Size(139, 29);
            this.boxCompetencia.TabIndex = 33;
            this.boxCompetencia.UseSelectable = true;
            this.boxCompetencia.ValueMember = "CompetenciaID";
            // 
            // boxIdioma
            // 
            this.boxIdioma.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.candidatosBindingSource, "IdiomaID", true));
            this.boxIdioma.DataSource = this.candidatosBindingSource;
            this.boxIdioma.DisplayMember = "IdiomaID";
            this.boxIdioma.FormattingEnabled = true;
            this.boxIdioma.ItemHeight = 23;
            this.boxIdioma.Location = new System.Drawing.Point(203, 234);
            this.boxIdioma.Name = "boxIdioma";
            this.boxIdioma.Size = new System.Drawing.Size(139, 29);
            this.boxIdioma.TabIndex = 31;
            this.boxIdioma.UseSelectable = true;
            this.boxIdioma.ValueMember = "IdiomaID";
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(63, 339);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(136, 19);
            this.materialLabel10.TabIndex = 30;
            this.materialLabel10.Text = "Recomendado Por:";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(125, 279);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(72, 19);
            this.materialLabel9.TabIndex = 29;
            this.materialLabel9.Text = "Posicion:";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(140, 242);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(59, 19);
            this.materialLabel7.TabIndex = 27;
            this.materialLabel7.Text = "Idioma:";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(97, 314);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(102, 19);
            this.materialLabel5.TabIndex = 25;
            this.materialLabel5.Text = "Competencia:";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(99, 168);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(101, 19);
            this.materialLabel1.TabIndex = 24;
            this.materialLabel1.Text = "Capacitacion:";
            // 
            // boxNacionalidad
            // 
            this.boxNacionalidad.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.candidatosBindingSource, "NacionalidadID", true));
            this.boxNacionalidad.DataSource = this.candidatosBindingSource;
            this.boxNacionalidad.DisplayMember = "NacionalidadID";
            this.boxNacionalidad.FormattingEnabled = true;
            this.boxNacionalidad.ItemHeight = 23;
            this.boxNacionalidad.Location = new System.Drawing.Point(203, 199);
            this.boxNacionalidad.Name = "boxNacionalidad";
            this.boxNacionalidad.Size = new System.Drawing.Size(139, 29);
            this.boxNacionalidad.TabIndex = 23;
            this.boxNacionalidad.UseSelectable = true;
            this.boxNacionalidad.ValueMember = "NacionalidadID";
            // 
            // txtSalario
            // 
            this.txtSalario.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatosBindingSource, "SalarioCandidato", true));
            this.txtSalario.Depth = 0;
            this.txtSalario.Hint = "";
            this.txtSalario.Location = new System.Drawing.Point(203, 135);
            this.txtSalario.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.PasswordChar = '\0';
            this.txtSalario.SelectedText = "";
            this.txtSalario.SelectionLength = 0;
            this.txtSalario.SelectionStart = 0;
            this.txtSalario.Size = new System.Drawing.Size(139, 23);
            this.txtSalario.TabIndex = 22;
            this.txtSalario.UseSystemPasswordChar = false;
            this.txtSalario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalario_KeyPress);
            // 
            // boxCapacitacion
            // 
            this.boxCapacitacion.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.candidatosBindingSource, "CapacitacionID", true));
            this.boxCapacitacion.DataSource = this.candidatosBindingSource;
            this.boxCapacitacion.DisplayMember = "CapacitacionID";
            this.boxCapacitacion.FormattingEnabled = true;
            this.boxCapacitacion.ItemHeight = 23;
            this.boxCapacitacion.Location = new System.Drawing.Point(203, 164);
            this.boxCapacitacion.Name = "boxCapacitacion";
            this.boxCapacitacion.Size = new System.Drawing.Size(139, 29);
            this.boxCapacitacion.TabIndex = 21;
            this.boxCapacitacion.UseSelectable = true;
            this.boxCapacitacion.ValueMember = "CapacitacionID";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(139, 139);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(64, 19);
            this.materialLabel4.TabIndex = 13;
            this.materialLabel4.Text = "Salario: ";
            // 
            // txtApellidos
            // 
            this.txtApellidos.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatosBindingSource, "ApellidosCandidato", true));
            this.txtApellidos.Depth = 0;
            this.txtApellidos.Hint = "";
            this.txtApellidos.Location = new System.Drawing.Point(203, 74);
            this.txtApellidos.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.PasswordChar = '\0';
            this.txtApellidos.SelectedText = "";
            this.txtApellidos.SelectionLength = 0;
            this.txtApellidos.SelectionStart = 0;
            this.txtApellidos.Size = new System.Drawing.Size(139, 23);
            this.txtApellidos.TabIndex = 12;
            this.txtApellidos.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(124, 80);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(76, 19);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Apellidos:";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(138, 383);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(60, 19);
            this.materialLabel2.TabIndex = 9;
            this.materialLabel2.Text = "Estado:";
            // 
            // txtNombreCondidato
            // 
            this.txtNombreCondidato.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatosBindingSource, "NombreCandidato", true));
            this.txtNombreCondidato.Depth = 0;
            this.txtNombreCondidato.Hint = "";
            this.txtNombreCondidato.Location = new System.Drawing.Point(203, 45);
            this.txtNombreCondidato.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNombreCondidato.Name = "txtNombreCondidato";
            this.txtNombreCondidato.PasswordChar = '\0';
            this.txtNombreCondidato.SelectedText = "";
            this.txtNombreCondidato.SelectionLength = 0;
            this.txtNombreCondidato.SelectionStart = 0;
            this.txtNombreCondidato.Size = new System.Drawing.Size(139, 23);
            this.txtNombreCondidato.TabIndex = 8;
            this.txtNombreCondidato.UseSystemPasswordChar = false;
            // 
            // NombreCandidato
            // 
            this.NombreCandidato.AutoSize = true;
            this.NombreCandidato.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.NombreCandidato.Depth = 0;
            this.NombreCandidato.Font = new System.Drawing.Font("Roboto", 11F);
            this.NombreCandidato.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.NombreCandidato.Location = new System.Drawing.Point(134, 52);
            this.NombreCandidato.MouseState = MaterialSkin.MouseState.HOVER;
            this.NombreCandidato.Name = "NombreCandidato";
            this.NombreCandidato.Size = new System.Drawing.Size(67, 19);
            this.NombreCandidato.TabIndex = 7;
            this.NombreCandidato.Text = "Nombre:";
            // 
            // txtCodigoCandidato
            // 
            this.txtCodigoCandidato.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatosBindingSource, "CandidatoID", true));
            this.txtCodigoCandidato.Depth = 0;
            this.txtCodigoCandidato.Hint = "";
            this.txtCodigoCandidato.Location = new System.Drawing.Point(203, 16);
            this.txtCodigoCandidato.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCodigoCandidato.Name = "txtCodigoCandidato";
            this.txtCodigoCandidato.PasswordChar = '\0';
            this.txtCodigoCandidato.SelectedText = "";
            this.txtCodigoCandidato.SelectionLength = 0;
            this.txtCodigoCandidato.SelectionStart = 0;
            this.txtCodigoCandidato.Size = new System.Drawing.Size(139, 23);
            this.txtCodigoCandidato.TabIndex = 1;
            this.txtCodigoCandidato.UseSystemPasswordChar = false;
            this.txtCodigoCandidato.Click += new System.EventHandler(this.txtCodigoCandidato_Click);
            this.txtCodigoCandidato.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoCandidato_KeyPress);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Depth = 0;
            this.lblCodigo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCodigo.Location = new System.Drawing.Point(140, 23);
            this.lblCodigo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(61, 19);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.Text = "Codigo:";
            // 
            // labDescripcion
            // 
            this.labDescripcion.AutoSize = true;
            this.labDescripcion.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labDescripcion.Depth = 0;
            this.labDescripcion.Font = new System.Drawing.Font("Roboto", 11F);
            this.labDescripcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labDescripcion.Location = new System.Drawing.Point(97, 209);
            this.labDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.labDescripcion.Name = "labDescripcion";
            this.labDescripcion.Size = new System.Drawing.Size(101, 19);
            this.labDescripcion.TabIndex = 2;
            this.labDescripcion.Text = "Nacionalidad:";
            // 
            // candidatosBindingSource1
            // 
            this.candidatosBindingSource1.DataSource = typeof(ReclutamientoRH.Modelos.Candidatos);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(601, 569);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 14;
            this.btnAgregar.Text = "Agregar ";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // AgregarCandidato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 620);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAgregar);
            this.Name = "AgregarCandidato";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar - Candidato";
            this.Load += new System.EventHandler(this.AgregarCandidato_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnSalir;
        public System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtApellidos;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNombreCondidato;
        private MaterialSkin.Controls.MaterialLabel NombreCandidato;
        public MaterialSkin.Controls.MaterialSingleLineTextField txtCodigoCandidato;
        private MaterialSkin.Controls.MaterialLabel lblCodigo;
        private MaterialSkin.Controls.MaterialLabel labDescripcion;
        public MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private System.Windows.Forms.BindingSource candidatosBindingSource;
        private MetroFramework.Controls.MetroComboBox boxNacionalidad;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSalario;
        private MetroFramework.Controls.MetroComboBox boxCapacitacion;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MetroFramework.Controls.MetroComboBox boxEstado;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtRecomendadoPor;
        private MetroFramework.Controls.MetroComboBox boxPosicion;
        private MetroFramework.Controls.MetroComboBox boxCompetencia;
        private MetroFramework.Controls.MetroComboBox boxIdioma;
        private System.Windows.Forms.BindingSource candidatosBindingSource1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCedula;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
    }
}