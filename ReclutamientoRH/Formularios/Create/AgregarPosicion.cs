﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.CRUD
{
    public partial class AgregarPosicion : MaterialForm
    {
        public AgregarPosicion()
        {
            InitializeComponent();
        }

        public int id;
        public int dep;
        public string posicion;
        public int minimo;
        public int maximo;
        public string riesgo;
        public string estado;
        public bool editar = false;


        private void AgregarPosicion_Load(object sender, EventArgs e)
        {

            using (DbContextRRHH db = new DbContextRRHH())
            {
                var result = from d in db.Departamentos
                             where d.EstadoDepartamento == "Activo"
                             select d;

                cbxDepartamentos.DataSource = result.ToList();
                cbxDepartamentos.ValueMember = "DepartamentoID";
                cbxDepartamentos.DisplayMember = "NombreDepartamento";
                    }
            if (editar == true)
            {
                txtCodigoPosicion.Text = id.ToString();
                txtDescripcion.Text = posicion;
                cbxDepartamentos.SelectedValue = dep ;
                txtMaximo.Text = maximo.ToString();
                txtMinimo.Text = minimo.ToString();
                cmbRiesgo.Text = riesgo;
                boxEstado.Text = estado;
            }
            else
            {

            }

        }

        private void btnAgregar_Click(object sender, EventArgs e)
           
        {
            if (editar == false)
            {
                DbContextRRHH db = new DbContextRRHH();
                if (txtCodigoPosicion.Text.Length < 4)
                {
                    MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    if (txtCodigoPosicion.Text == "" || txtDescripcion.Text == "" || boxEstado.Text == "" || txtCodigoPosicion.Text == "" || cbxDepartamentos.Text == "" || txtMinimo.Text == "" || txtMaximo.Text == "" || cmbRiesgo.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {
                        id = int.Parse(txtCodigoPosicion.Text);
                        dep = int.Parse(cbxDepartamentos.SelectedValue.ToString());
                        posicion = txtDescripcion.Text;
                        minimo = int.Parse(txtMinimo.Text);
                        maximo = int.Parse(txtMaximo.Text);
                        riesgo = cmbRiesgo.SelectedItem.ToString();
                        estado = boxEstado.SelectedItem.ToString();
                    }
                    Posiciones ps = new Posiciones();

                    ps.PosicionID = id;
                    if (db.Posiciones.Any(d => d.PosicionID == id))
                    {
                        MessageBox.Show("Posicion ya existe o Clave duplicada", "ERROR AL GUARDAR");
                        txtCodigoPosicion.Focus();
                    }
                    else
                    {
                        ps.DepartamentoID = dep;
                        ps.NombrePosicion = posicion;
                        if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Posiciones.Any(t => t.NombrePosicion == posicion))
                        {
                            MessageBox.Show("No es permitido Caracteres numericos o Departmento ya existe", "ERROR AL GUARDAR");

                            return;
                        }
                        ps.SalarioMinimoPosicion = minimo;
                        if(txtMinimo.Text.Length < 4)
                        {
                            MessageBox.Show("El Salario minimo no puede tener menos de 4 Digitos", "ERROR AL GUARDAR");
                            return;
                        }
                    
                        ps.SalaroMaximoPosicion = maximo;
                        if (int.Parse(txtMaximo.Text) < int.Parse(txtMinimo.Text))
                        {
                            MessageBox.Show("El Salario Maximo no puede ser menor al Minimo", "ERROR AL GUARDAR");
                            return;
                        }
                        ps.RiesgoPosicion = riesgo;
                     
                        ps.EstadoPosicion = estado;
                    


                        db.Posiciones.Add(ps);
                        db.SaveChanges();
                        this.Limpiar();

                    }
                }

            }
            else
            {

                posicion = txtDescripcion.Text;
                dep = int.Parse(cbxDepartamentos.SelectedValue.ToString());
                minimo = int.Parse(txtMinimo.Text);
                maximo = int.Parse(txtMaximo.Text);
                riesgo = cmbRiesgo.SelectedItem.ToString();
                estado = boxEstado.SelectedItem.ToString();

                DbContextRRHH db = new DbContextRRHH();
                var n = db.Posiciones.First(d => d.PosicionID == id);

                    n.DepartamentoID = dep;
                    n.NombrePosicion = posicion;
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Posiciones.Any(t => t.NombrePosicion == posicion && t.PosicionID != id))
                {
                    MessageBox.Show("No es permitido Caracteres numericos o Departmento ya existe", "ERROR AL GUARDAR");

                    return;
                }
                else
                {

                
                    n.SalarioMinimoPosicion = minimo;
                    if (txtMinimo.Text.Length < 4)
                    {
                        MessageBox.Show("El Salario minimo no puede tener menos de 4 Digitos", "ERROR AL GUARDAR");
                        return;
                    }
                        n.SalaroMaximoPosicion = maximo;
                    if (int.Parse(txtMaximo.Text) < int.Parse(txtMinimo.Text))
                    {
                        MessageBox.Show("El Salario Maximo no puede ser menor al Minimo", "ERROR AL GUARDAR");
                        return;
                    }
                    n.RiesgoPosicion = riesgo;
                    n.EstadoPosicion = estado;
                    if (db.Candidatos.Any(c => c.PosicionID == id) && estado == "Inactivo")
                    {
                        MessageBox.Show("Esta Posicion Esta asociada a un Candidato", "ERROR AL EDITAR");
                        return;
                    }



                    db.SaveChanges();
                    FormPosiciones posi = new FormPosiciones();
                    posi.Show();
                    this.Close();
                }



            }

        }
        public void Limpiar ()
        {
            txtCodigoPosicion.Text = "";
            cbxDepartamentos.Text = "";
            txtDescripcion.Text = "";
            txtMaximo.Text = "";
            txtMinimo.Text = "";
            cmbRiesgo.Text = "";
            boxEstado.Text = "";
            cbxDepartamentos.Text = "";
            
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormPosiciones ps = new FormPosiciones();
            ps.Show();
            this.Hide();
        }

        private void posicionesBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigoPosicion_Click(object sender, EventArgs e)
        {

        }

        private void txtCodigoPosicion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMinimo_Click(object sender, EventArgs e)
        {

        }

        private void txtMinimo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaximo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
