﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.Create
{
    public partial class AgregarCompetencias : MaterialForm
    {
        public AgregarCompetencias()
        {
            InitializeComponent();
        }
        public int id;
        public string des;
        public string est;
        public bool editar = false;

        private void AgregarCompetencias_Load(object sender, EventArgs e)
        {
            if (editar == true)
            {
                txtCodigoCompetencia.Text = id.ToString();
                txtDescripcion.Text = des;
                boxEstado.Text = est;
            }
            else
            {
                txtCodigoCompetencia.Text = "";
                boxEstado.Text = "Activo";
            }

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (editar == false)
            {
                if (txtCodigoCompetencia.Text == "" || txtDescripcion.Text == "" || boxEstado.Text == "")
                {
                    MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    DbContextRRHH db = new DbContextRRHH();
                    if (txtCodigoCompetencia.Text.Length < 4)
                    {
                        MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {
                        
                        id = int.Parse(txtCodigoCompetencia.Text);
                        des = txtDescripcion.Text;
                        est = boxEstado.SelectedItem.ToString();
                    }

                    Competencias comp = new Competencias();

                    comp.CompetenciaID = id;
                    if (db.Competencias.Any(d => d.CompetenciaID == id))
                    {
                        MessageBox.Show("Competencia ya existe o Clave duplicada", "ERROR AL GUARDAR");
                        txtCodigoCompetencia.Focus();
                    }
                    else
                    {
                        comp.NombreCompetencia = des;
                        if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Competencias.Any(t => t.NombreCompetencia == des))
                        {
                            MessageBox.Show("Competencia ya existe", "ERROR AL GUARDAR");

                            return;
                        }
 
                        else                 
                        {
                            comp.EstadoCompetencia = est;
                            db.Competencias.Add(comp);
                            db.SaveChanges();
                            this.Limpiar();
                        }
                    }
                }
            }
            else
            {
                des = txtDescripcion.Text;
                est = boxEstado.SelectedItem.ToString();
                DbContextRRHH db = new DbContextRRHH();
                var n = db.Competencias.First(d => d.CompetenciaID == id);

                if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Competencias.Any(t => t.NombreCompetencia == des && t.CompetenciaID != id))
                {

                    MessageBox.Show("Competencia ya existe", "ERROR AL GUARDAR");
                    return;
                }
                else
                {

                    n.NombreCompetencia = des;
                    n.EstadoCompetencia = est;



                    db.SaveChanges();
                    FormCompetencias com = new FormCompetencias();

                    this.Close();
                    com.Show();
                }


            }
        }
        public void Limpiar()
        {
            txtCodigoCompetencia.Text = "";
            txtDescripcion.Text = "";
            boxEstado.Text = "";      
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormCompetencias cp = new FormCompetencias();
            cp.Show();
            this.Hide();
        }
    }
}
