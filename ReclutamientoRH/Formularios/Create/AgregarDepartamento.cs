﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReclutamientoRH.Modelos;
using ReclutamientoRH.DAL;


namespace ReclutamientoRH.Formularios.CRUD
{
    public partial class AgregarDepartamento : MaterialForm
    {
        
        public int Id;
        public string Nombre;
        public string Estado;
        public bool editar = false;
        
        public AgregarDepartamento()
        {
            InitializeComponent();
            
        }

        private void AgregarDepartamento_Load(object sender, EventArgs e)
        {
            if (editar == true)
            {
                txtCodigoDepartamento.Text = Id.ToString();
                txtDescripcion.Text = Nombre;
                boxEstado.Text = Estado;
            }
            else
            {
                boxEstado.SelectedItem = "Activo";
            }         
        }

        private void lblCodigo_Click(object sender, EventArgs e)
        {

        }
        private void txtCodigoDepartamento_Click(object sender, EventArgs e)
        {

        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (editar == false)
            {
                if (txtCodigoDepartamento.Text == "" || txtDescripcion.Text == "" || boxEstado.Text == "")
                {
                    MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    DbContextRRHH db = new DbContextRRHH();
                    if (txtCodigoDepartamento.Text.Length < 4 )
                    {
                        MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    { 
                        Id = int.Parse(txtCodigoDepartamento.Text);
                        Nombre = txtDescripcion.Text;
                        Estado = boxEstado.SelectedItem.ToString();
                    }

                    Departamentos dp = new Departamentos();

                    dp.DepartamentoID = Id;
                    if(db.Departamentos.Any(d => d.DepartamentoID == Id))
                    {
                        MessageBox.Show( "Departamento ya existe o Clave duplicada", "ERROR AL GUARDAR");
                        txtCodigoDepartamento.Focus();
                    }
                    else
                    { 
                    
                        dp.NombreDepartamento = Nombre;

                        if(!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Departamentos.Any(t => t.NombreDepartamento == Nombre))
                        {
                            MessageBox.Show("No es permitido Caracteres numericos o Departmento ya existe", "ERROR AL GUARDAR");
                       
                            return;
                        }

                        dp.EstadoDepartamento = Estado;

                    
                        db.Departamentos.Add(dp);
                        db.SaveChanges();
                        db.Dispose();
                        this.Limpiar();
                    }             
                }
            }
            else
            {
                DbContextRRHH db = new DbContextRRHH();

                
                Nombre = txtDescripcion.Text;  
                Estado = boxEstado.SelectedItem.ToString();                
                var n = db.Departamentos.First(d => d.DepartamentoID == Id);

                if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Departamentos.Any(t => t.NombreDepartamento == Nombre && t.DepartamentoID != Id))
                {     
                    
                    MessageBox.Show("Departmento ya existe", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    n.NombreDepartamento = Nombre;
                    n.EstadoDepartamento = Estado;

                    db.SaveChanges();
                    FormDepartamentos depar = new FormDepartamentos();
                
                    this.Close();
                    depar.Show();
                }
            }          
        }

        public void Limpiar()
        {
            txtCodigoDepartamento.Text = "";
            txtDescripcion.Text = "";
            boxEstado.Text = "";
        }
       
        private void CheckEstado_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormDepartamentos dp = new FormDepartamentos();
            dp.Show();
            this.Hide();
        }

        private void departamentosBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void boxEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigoDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
             
        }
    }
}
