﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.CRUD;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.Create
{
    public partial class AgregarCandidato : MaterialForm
    {
        public AgregarCandidato()
        {
            InitializeComponent();
        }
        DbContextRRHH db = new DbContextRRHH();
        public int id;
        public string nombre;
        public string apellido;
        public int salario;
        public int nacionalidad;
        public int capacitacion;
        public int competencia;
        public int idioma;
        public int posicion;
        public int experiencia;
        public string recomendacion;
        public string estado;
        public long cedula;
        public bool editar = false;
        
        

        private void AgregarCandidato_Load(object sender, EventArgs e)
        {
            
            using (DbContextRRHH db = new DbContextRRHH())
            {
                
               
                

                boxNacionalidad.DataSource = db.Nacionalidades.ToList();
                boxNacionalidad.ValueMember = "NacionalidadID";
                boxNacionalidad.DisplayMember = "NombreNacionalidad";

                boxCapacitacion.DataSource = db.Capacitaciones.ToList();
                boxCapacitacion.ValueMember = "CapacitacionID";
                boxCapacitacion.DisplayMember = "DescripcionCapacitacion";

                boxCompetencia.DataSource = db.Competencias.ToList();
                boxCompetencia.ValueMember = "CompetenciaID";
                boxCompetencia.DisplayMember = "NombreCompetencia";

                var result1 = from i in db.Idiomas
                              where i.EstadoIdioma == "Activo"
                              select i;

                boxIdioma.DataSource = result1.ToList();
                boxIdioma.ValueMember = "IdiomaID";
                boxIdioma.DisplayMember = "NombreIdioma";

                var result = from d in db.Posiciones
                             where d.EstadoPosicion == "Activo"
                             select d;

                boxPosicion.DataSource = result.ToList();
                boxPosicion.ValueMember = "PosicionID";
                boxPosicion.DisplayMember = "NombrePosicion";
            }
            if(editar == true)
            {
                txtCodigoCandidato.Text = id.ToString();
                txtNombreCondidato.Text = nombre.ToString();
                txtApellidos.Text = apellido.ToString();
                txtCedula.Text = cedula.ToString();
                txtSalario.Text = salario.ToString();
                txtRecomendadoPor.Text = recomendacion.ToString();
                boxCapacitacion.Text = capacitacion.ToString();
                boxCompetencia.Text = competencia.ToString();
                boxEstado.Text = estado.ToString();
                boxNacionalidad.Text = nacionalidad.ToString();
                boxPosicion.Text = posicion.ToString();
                boxIdioma.SelectedValue = idioma;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormCandidato ca = new FormCandidato();
            ca.Show();
            this.Close();

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(editar == false)
            {
                DbContextRRHH db = new DbContextRRHH();
                if (txtCodigoCandidato.Text.Length < 4)
                {
                    MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    if (txtCodigoCandidato.Text == "" || txtNombreCondidato.Text == "" || txtApellidos.Text == "" || txtCedula.Text == "" || txtSalario.Text == "" || boxEstado.Text == "" || txtRecomendadoPor.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {

                        id = int.Parse(txtCodigoCandidato.Text);

                        nombre = txtNombreCondidato.Text;
                        apellido = txtApellidos.Text;
                        cedula = long.Parse(txtCedula.Text);
                        salario = int.Parse(txtSalario.Text);
                        recomendacion = txtRecomendadoPor.Text;
                        estado = boxEstado.SelectedItem.ToString();
                        nacionalidad = int.Parse(boxNacionalidad.SelectedValue.ToString());
                        capacitacion = int.Parse(boxCapacitacion.SelectedValue.ToString());
                        competencia = int.Parse(boxCompetencia.SelectedValue.ToString());
                        idioma = int.Parse(boxIdioma.SelectedValue.ToString());
                        posicion = int.Parse(boxPosicion.SelectedValue.ToString());
                        estado = boxEstado.SelectedItem.ToString();



                        Candidatos ca = new Candidatos();

                        ca.CandidatoID = id;
                        if (db.Candidatos.Any(d => d.CandidatoID == id))
                        {
                            MessageBox.Show("Candidato ya existe o Clave duplicada", "ERROR AL GUARDAR");
                            txtCodigoCandidato.Focus();
                        }
                        else
                        {
                            ca.NombreCandidato = nombre;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(nombre, "^[a-zA-Z]") )
                            {
                                MessageBox.Show("No es permitido Caracteres numericos ", "ERROR AL GUARDAR");

                                return;
                            }
                            ca.ApellidosCandidato = apellido;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(apellido, "^[a-zA-Z]"))
                            {
                                MessageBox.Show("No es permitido Caracteres numericos ", "ERROR AL GUARDAR");

                                return;
                            }
                            ca.CedulaCandidato = cedula;
                            if(txtCedula.Text.Length != 11)
                            {
                                MessageBox.Show("Error Identificacion solo puede ser de 11 digitos","ERROR AL GUARDAR");
                                return;
                            }
                            if(!validaCedula(txtCedula.Text))
                            {
                                MessageBox.Show("Error Cedula No es valida", "ERROR AL GUARDAR");
                                return;
                            }
                            if ( db.Candidatos.Any(t => t.CedulaCandidato == cedula) || db.Empleados.Any(q => q.CedulaEmpleado == cedula))
                            {
                                MessageBox.Show("Identificacion ya existe", "ERROR AL GUARDAR");

                                return;
                            }
                            ca.SalarioCandidato = salario;
                            

                            ca.RecomendadoPor = recomendacion;
                            ca.NacionalidadID = nacionalidad;
                            ca.CapacitacionID = capacitacion;
                            ca.CompetenciaID = competencia;
                            ca.IdiomaID = idioma;
                            ca.PosicionID = posicion;
                            var result = db.Posiciones.Where(t => t.PosicionID == ca.PosicionID).FirstOrDefault();
                            if (salario < result.SalarioMinimoPosicion || salario > result.SalaroMaximoPosicion)
                            {
                                MessageBox.Show("El Salario no puede ser Minimo de " + result.SalarioMinimoPosicion + "ni mayor que" + result.SalaroMaximoPosicion);
                                return;
                            }
                            ca.EstadoCandidato = estado;
                           



                            db.Candidatos.Add(ca);
                            db.SaveChanges();
                            this.Limpiar();
                            
                        }
                    }
                }
                


            }
            else
            {
                nombre = txtNombreCondidato.Text;
                apellido = txtApellidos.Text;
                cedula = long.Parse(txtCedula.Text);
                salario = int.Parse(txtSalario.Text);
                recomendacion = txtRecomendadoPor.Text;
                estado = boxEstado.SelectedItem.ToString();
                nacionalidad = int.Parse(boxNacionalidad.SelectedValue.ToString());
                capacitacion = int.Parse(boxCapacitacion.SelectedValue.ToString());
                competencia = int.Parse(boxCompetencia.SelectedValue.ToString());
                idioma = int.Parse(boxIdioma.SelectedValue.ToString());
                posicion = int.Parse(boxPosicion.SelectedValue.ToString());
                estado = boxEstado.SelectedItem.ToString();

                DbContextRRHH db = new DbContextRRHH();

                var c = db.Candidatos.First(t => t.CandidatoID == id);

                c.NombreCandidato = nombre;
                if (!System.Text.RegularExpressions.Regex.IsMatch(nombre, "^[a-zA-Z]"))
                {
                    MessageBox.Show("No es permitido Caracteres numericos ", "ERROR AL GUARDAR");

                    return;
                }
                c.ApellidosCandidato = apellido;
                if (!System.Text.RegularExpressions.Regex.IsMatch(apellido, "^[a-zA-Z]"))
                {
                    MessageBox.Show("No es permitido Caracteres numericos ", "ERROR AL GUARDAR");

                    return;
                }
                c.CedulaCandidato = cedula;
                if (txtCedula.Text.Length != 11)
                {
                    MessageBox.Show("Error Identificacion solo puede ser de 11 digitos", "ERROR AL GUARDAR");
                    return;

                }
                if (!validaCedula(txtCedula.Text))
                {
                    MessageBox.Show("Error Cedula No es valida", "ERROR AL GUARDAR");
                    return;
                }


                c.SalarioCandidato = salario;
                c.RecomendadoPor = recomendacion;
                c.NacionalidadID = nacionalidad;
                c.CapacitacionID = capacitacion;
                c.CompetenciaID = competencia;
                c.IdiomaID = idioma;
                c.PosicionID = posicion;
                var result = db.Posiciones.Where(t => t.PosicionID == c.PosicionID).FirstOrDefault();
                if(salario < result.SalarioMinimoPosicion || salario > result.SalaroMaximoPosicion)
                {
                    MessageBox.Show("El Salario no puede ser Minimo de " + result.SalarioMinimoPosicion + "ni mayor que" + result.SalaroMaximoPosicion);
                    return;
                }
                c.EstadoCandidato = estado;

                db.SaveChanges();
                FormCandidato ca = new FormCandidato();
                ca.Show();
                this.Close();

            }

        }

        public void Limpiar()
        {
            txtCodigoCandidato.Text = "";
            txtNombreCondidato.Text = "";
            txtCedula.Text = "";
            txtApellidos.Text = "";
            txtSalario.Text = "";
            txtRecomendadoPor.Text = "";

        }

        private void txtCodigoCandidato_Click(object sender, EventArgs e)
        {
            //if(txtCodigoCandidato.Text.Trim() == "")
            //{
            //    MessageBox.Show("Digite un Codigo para el Empleado");
            //}
        }

        private void txtCodigoCandidato_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        public static bool validaCedula(string pCedula)



        {

            int vnTotal = 0;

            string vcCedula = pCedula.Replace("-", "");

            int pLongCed = vcCedula.Trim().Length;

            int[] digitoMult = new int[11] { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1 };



            if (pLongCed < 11 || pLongCed > 11)

                return false;



            for (int vDig = 1; vDig <= pLongCed; vDig++)

            {

                int vCalculo = Int32.Parse(vcCedula.Substring(vDig - 1, 1)) * digitoMult[vDig - 1];

                if (vCalculo < 10)

                    vnTotal += vCalculo;

                else

                    vnTotal += Int32.Parse(vCalculo.ToString().Substring(0, 1)) + Int32.Parse(vCalculo.ToString().Substring(1, 1));

            }



            if (vnTotal % 10 == 0)

                return true;

            else

                return false;

        }
    }

}

