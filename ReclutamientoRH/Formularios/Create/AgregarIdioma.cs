﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.Create
{
    public partial class AgregarIdioma : MaterialForm
    {
        public AgregarIdioma()
        {
            InitializeComponent();
        }

        public int id;
        public string dep;
        public string estado;
        public bool editar = false;

        private void AgregarIdioma_Load(object sender, EventArgs e)
        {
            if(editar == true)
            {
                txtCodigoIdioma.Text = id.ToString();
                txtDescripcion.Text = dep;
                boxEstado.Text = estado;
            }
            else
            {
                txtCodigoIdioma.Text = "";
                boxEstado.Text = "Activo";
            }
            


        }

        public void Limpiar()
        {
            txtCodigoIdioma.Text = "";
            txtDescripcion.Text = "";
            boxEstado.Text = "";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormIdiomas idms = new FormIdiomas();
            idms.Show();
            this.Hide();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(editar == false) {

                DbContextRRHH db = new DbContextRRHH();

                if (txtCodigoIdioma.Text == "" || txtDescripcion.Text == "" || boxEstado.Text == "")
                {
                    MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    if (txtCodigoIdioma.Text.Length > 4)
                    {
                        MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {
                        if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Idiomas.Any(t => t.NombreIdioma == dep))
                        {
                            MessageBox.Show(" Idioma ya existe", "ERROR AL GUARDAR");

                            return;
                        }

                        id = int.Parse(txtCodigoIdioma.Text);
                        dep = txtDescripcion.Text;
                        estado = boxEstado.SelectedItem.ToString();

                        Idiomas idm = new Idiomas();

                        idm.IdiomaID = id;
                        if (db.Idiomas.Any(d => d.IdiomaID == id))
                        {
                            MessageBox.Show("Idioma ya existe o Clave duplicada", "ERROR AL GUARDAR");
                            txtCodigoIdioma.Focus();
                        }
                        else
                        {
                            idm.NombreIdioma = dep;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Idiomas.Any(t => t.NombreIdioma == dep))
                            {
                                MessageBox.Show("Idioma ya existe", "ERROR AL GUARDAR");

                                return;
                            }   
                        
                            idm.EstadoIdioma = estado;

                            db.Idiomas.Add(idm);
                            db.SaveChanges();
                            this.Limpiar();
                        }
                    }
                }
            }
            else
            {
                DbContextRRHH db = new DbContextRRHH();
                dep = txtDescripcion.Text;
                estado = boxEstado.SelectedItem.ToString();
                var n = db.Idiomas.First(d => d.IdiomaID == id);

                if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Idiomas.Any(t => t.NombreIdioma == dep && t.IdiomaID != id))
                {

                    MessageBox.Show("No es permitido Caracteres numericos o Idioma ya existe", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    n.NombreIdioma = dep;
                    n.EstadoIdioma = estado;



                    db.SaveChanges();
                    FormIdiomas idi = new FormIdiomas();

                    this.Close();
                    idi.Show();
                }

            }
        }

        private void txtCodigoIdioma_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
