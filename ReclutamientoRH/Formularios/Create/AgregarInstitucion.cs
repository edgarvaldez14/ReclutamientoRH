﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.CRUD
{
    public partial class AgregarInstitucion : MaterialForm
    {
        public AgregarInstitucion()
        {
            InitializeComponent();
        }

        public int id;
        public string nombre;
        public string pais;
        public string provincia;
        public string municipio;
        public string direccion;
        public bool editar = false;


        private void AgregarInstitucion_Load(object sender, EventArgs e)
        {
            txtCodigoInstitucion.Text = id.ToString();
            txtDescripcionInstitucion.Text = nombre;
            cxbPais.Text = pais;
            txtProvincia.Text = provincia;
            txtMunicipio.Text = municipio;
            txtDireccion.Text = direccion;          
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(editar == false)
            {
                DbContextRRHH db = new DbContextRRHH();
                
                if (txtCodigoInstitucion.Text == "" || txtDescripcionInstitucion.Text == "" || cxbPais.Text == "" || txtProvincia.Text == "" || txtMunicipio.Text == "" || txtDireccion.Text == "")
                {
                    MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    return;
                }
                else if (txtCodigoInstitucion.Text.Length < 4)
                {
                    MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    id = int.Parse(txtCodigoInstitucion.Text);
                    nombre = txtDescripcionInstitucion.Text;
                    if(cxbPais.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son requeridos");
                    }
                    pais = cxbPais.SelectedItem.ToString();
                    provincia = txtProvincia.Text;
                    municipio = txtMunicipio.Text;
                    direccion = txtDireccion.Text;
                }

                Instituciones it = new Instituciones();

                it.InstitucionID = id;
                if (db.Instituciones.Any(d => d.InstitucionID == id))
                {
                    MessageBox.Show("Departamento ya existe o Clave duplicada", "ERROR AL GUARDAR");
                    txtCodigoInstitucion.Focus();
                }
                else
                {
                    if (txtCodigoInstitucion.Text == "" || txtDescripcionInstitucion.Text == "" || cxbPais.Text == "" || txtProvincia.Text == "" || txtMunicipio.Text == "" || txtDireccion.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                        return;
                    }
                    if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcionInstitucion.Text, "^[a-zA-Z]") || db.Instituciones.Any(t => t.NombreInstitucion == nombre))
                    {
                        MessageBox.Show(" Ya Existe una Institucion con este Nombre ", "ERROR AL GUARDAR");

                        return;
                    }
                    it.NombreInstitucion = nombre;                    
                    it.PaisInstitucion = pais;
                    it.ProvinciaInstitucion = provincia;
                    it.MunicipioInstitucion = municipio;
                    it.DireccionInstitucion = direccion;
                    if (txtDescripcionInstitucion.Text == "" || cxbPais.Text == "" || txtProvincia.Text == "" || txtMunicipio.Text == "" || txtDireccion.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    }
                    else
                    {
                        db.Instituciones.Add(it);
                        db.SaveChanges();
                        this.Limpiar();
                    }
                
                }    
            }
            else
            {
                if (txtCodigoInstitucion.Text == "" || txtDescripcionInstitucion.Text == "" || cxbPais.Text == "" || txtProvincia.Text == "" || txtMunicipio.Text == "" || txtDireccion.Text == "")
                {
                    MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    return;
                }

                nombre = txtDescripcionInstitucion.Text;
                pais = cxbPais.SelectedItem.ToString();
                provincia = txtProvincia.Text;
                municipio = txtMunicipio.Text;
                direccion = txtDireccion.Text;

                DbContextRRHH db = new DbContextRRHH();
                var n = db.Instituciones.First(d => d.InstitucionID == id);

                if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcionInstitucion.Text, "^[a-zA-Z]") || db.Instituciones.Any(t => t.NombreInstitucion == nombre && t.InstitucionID != id))
                {
                    MessageBox.Show("No es permitido Caracteres numericos o Departmento ya existe", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    n.NombreInstitucion = nombre;
                    n.PaisInstitucion = pais;
                    n.ProvinciaInstitucion = provincia;
                    n.MunicipioInstitucion = municipio;
                    n.DireccionInstitucion = direccion;

                    db.SaveChanges();
                    FormInstitucionCapacitacion depar = new FormInstitucionCapacitacion();

                    this.Close();
                    depar.Show();
                }
            }
        }
        public void Limpiar()
        {
            txtCodigoInstitucion.Text = "";
            txtDescripcionInstitucion.Text = "";
            txtDireccion.Text = "";
            txtMunicipio.Text = "";
            txtProvincia.Text = "";
            cxbPais.Text = "";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormInstitucionCapacitacion itc = new FormInstitucionCapacitacion();
            itc.Show();
            this.Close();
        }
        private void institucionesBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
        private void txtCodigoInstitucion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
