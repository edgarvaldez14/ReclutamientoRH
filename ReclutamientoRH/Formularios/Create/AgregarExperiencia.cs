﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.Create
{
    public partial class AgregarExperiencia : MaterialForm
    {
        public AgregarExperiencia()
        {
            InitializeComponent();
        }
        public int id;
        public string org;
        public int pos;
        public int can;
        public DateTime inicio;
        public DateTime fin;
        public int salario;
        public bool editar = false;

        private void AgragarCapacitacion_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db  = new DbContextRRHH())
            {
                boxPosicion.DataSource = db.Posiciones.ToList();
                boxPosicion.ValueMember = "PosicionID";
                boxPosicion.DisplayMember = "NombrePosicion";
                boxCandidato.DataSource = db.Candidatos.ToList();
                boxCandidato.ValueMember = "CandidatoID";
                
            }
            if (editar == true)
            {
                txtCodigoExperiencia.Text = id.ToString();
                txtOrganizacion.Text = org.ToString();
                boxPosicion.Text = pos.ToString();
                boxCandidato.Text = can.ToString();
                dtFechaInicio.Text = inicio.ToString();
                dtFechaFin.Text = fin.ToString();
                txtSalario.Text = salario.ToString();
            }
            else
            {

            }

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (editar == false)
            {
                DbContextRRHH db = new DbContextRRHH();
                if (txtCodigoExperiencia.Text.Length < 4)
                {
                    MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    if (txtCodigoExperiencia.Text == "" || txtOrganizacion.Text == "" || boxPosicion.Text == "" || dtFechaInicio.Text == "" || dtFechaFin.Text == "" || txtSalario.Text == "" || boxCandidato.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {

                        id = int.Parse(txtCodigoExperiencia.Text);
                        org = txtOrganizacion.Text;
                        pos = int.Parse(boxPosicion.SelectedValue.ToString());
                        inicio = dtFechaInicio.Value;
                        fin = dtFechaFin.Value;
                        salario = int.Parse(txtSalario.Text);
                        can = int.Parse(boxCandidato.SelectedValue.ToString());

                        Experiencias ex = new Experiencias();

                        ex.ExperienciaID = id;
                        if (db.Experiencias.Any(d => d.ExperienciaID == id))
                        {
                            MessageBox.Show("Departamento ya existe o Clave duplicada", "ERROR AL GUARDAR");
                            txtCodigoExperiencia.Focus();
                        }
                        else
                        {
                            ex.OrganizacionExperiencia = org;
                            ex.PosicionID = pos;
                            ex.CandidatoID = can;
                            if ( db.Experiencias.Any(t => t.CandidatoID == can))
                            {
                                MessageBox.Show("Este Colaborador ya tiene experiencia asiganda", "ERROR AL GUARDAR");

                                return;
                            }
                            ex.FechaEntradaExperiencia = inicio;
                            ex.fechaSalidaExperiencia = fin;
                            if (fin <= inicio)
                            {
                                MessageBox.Show("La fecha de Fin no puede ser menor que la Fecha Inicio", "ERROR AL GUARDAR");
                                return;
                            }
                            ex.SalarioExperiencia = salario;
                            if (txtSalario.Text.Length < 4)
                            {
                                MessageBox.Show("El Salario no puede tener menos de 4 Digitos", "ERROR AL GUARDAR");
                                return;
                            }

                            db.Experiencias.Add(ex);
                            db.SaveChanges();
                            this.Limpiar();
                        }
                    }
                }
            }
            else
            {
                org = txtOrganizacion.Text;
                pos = int.Parse(boxPosicion.SelectedValue.ToString());
                inicio = dtFechaInicio.Value;
                fin = dtFechaFin.Value;
                salario = int.Parse(txtSalario.Text);
                can = int.Parse(boxCandidato.Text);

                DbContextRRHH db = new DbContextRRHH();
                var n = db.Experiencias.First(d => d.ExperienciaID == id);
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtOrganizacion.Text, "^[a-zA-Z]") || db.Experiencias.Any(t =>  t.ExperienciaID != id && t.CandidatoID == can))
                {
                    MessageBox.Show("Codigo ya existe", "ERROR AL GUARDAR");

                    return;
                }
                else
                {
                    n.ExperienciaID = id;
                    n.OrganizacionExperiencia = org;
                    n.CandidatoID = can;
                    n.FechaEntradaExperiencia = inicio;
                    if (fin <= inicio)
                    {
                        MessageBox.Show("La fecha de Fin no puede ser menor que la Fecha Inicio", "ERROR AL GUARDAR");
                        return;
                    }
                    n.fechaSalidaExperiencia = fin;
                    if (txtSalario.Text.Length < 4)
                    {
                        MessageBox.Show("El Salario no puede tener menos de 4 Digitos", "ERROR AL GUARDAR");
                        return;
                    }
                    n.SalarioExperiencia = salario;

                    db.SaveChanges();
                    FormExperiencia ex = new FormExperiencia();
                    ex.Show();
                    this.Close();
                }
            }
        }
        public void Limpiar()
        {
            txtCodigoExperiencia.Text = "";
            txtOrganizacion.Text = "";
            txtSalario.Text = "";
            boxCandidato.Text = "";
            boxPosicion.Text = "";
            dtFechaFin.Text = "";
            dtFechaInicio.Text = "";

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormExperiencia exp = new FormExperiencia();
            exp.Show();
            this.Hide();
        }

        private void txtCodigoExperiencia_Click(object sender, EventArgs e)
        {

        }

        private void txtCodigoExperiencia_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
