﻿namespace ReclutamientoRH.Formularios.Create
{
    partial class AgregarExperiencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.boxCandidato = new MetroFramework.Controls.MetroComboBox();
            this.experienciasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.candidatosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.txtSalario = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.dtFechaFin = new MetroFramework.Controls.MetroDateTime();
            this.dtFechaInicio = new MetroFramework.Controls.MetroDateTime();
            this.boxPosicion = new MetroFramework.Controls.MetroComboBox();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtOrganizacion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCodigoExperiencia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblCodigo = new MaterialSkin.Controls.MaterialLabel();
            this.labDescripcion = new MaterialSkin.Controls.MaterialLabel();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.experienciasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSalir.Depth = 0;
            this.btnSalir.Location = new System.Drawing.Point(694, 457);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Primary = false;
            this.btnSalir.Size = new System.Drawing.Size(49, 36);
            this.btnSalir.TabIndex = 18;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.boxCandidato);
            this.groupBox1.Controls.Add(this.materialLabel5);
            this.groupBox1.Controls.Add(this.txtSalario);
            this.groupBox1.Controls.Add(this.dtFechaFin);
            this.groupBox1.Controls.Add(this.dtFechaInicio);
            this.groupBox1.Controls.Add(this.boxPosicion);
            this.groupBox1.Controls.Add(this.materialLabel4);
            this.groupBox1.Controls.Add(this.materialLabel3);
            this.groupBox1.Controls.Add(this.materialLabel2);
            this.groupBox1.Controls.Add(this.txtOrganizacion);
            this.groupBox1.Controls.Add(this.materialLabel1);
            this.groupBox1.Controls.Add(this.txtCodigoExperiencia);
            this.groupBox1.Controls.Add(this.lblCodigo);
            this.groupBox1.Controls.Add(this.labDescripcion);
            this.groupBox1.Location = new System.Drawing.Point(159, 128);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 325);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Experiencias Laborales";
            // 
            // boxCandidato
            // 
            this.boxCandidato.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.experienciasBindingSource, "PosicionID", true));
            this.boxCandidato.DataSource = this.candidatosBindingSource;
            this.boxCandidato.DisplayMember = "CandidatoID";
            this.boxCandidato.FormattingEnabled = true;
            this.boxCandidato.ItemHeight = 23;
            this.boxCandidato.Location = new System.Drawing.Point(201, 53);
            this.boxCandidato.Name = "boxCandidato";
            this.boxCandidato.Size = new System.Drawing.Size(137, 29);
            this.boxCandidato.TabIndex = 26;
            this.boxCandidato.UseSelectable = true;
            this.boxCandidato.ValueMember = "CandidatoID";
            // 
            // experienciasBindingSource
            // 
            this.experienciasBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Experiencias);
            // 
            // candidatosBindingSource
            // 
            this.candidatosBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Candidatos);
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(120, 56);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(81, 19);
            this.materialLabel5.TabIndex = 25;
            this.materialLabel5.Text = "Candidato:";
            // 
            // txtSalario
            // 
            this.txtSalario.Depth = 0;
            this.txtSalario.Hint = "";
            this.txtSalario.Location = new System.Drawing.Point(201, 254);
            this.txtSalario.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.PasswordChar = '\0';
            this.txtSalario.SelectedText = "";
            this.txtSalario.SelectionLength = 0;
            this.txtSalario.SelectionStart = 0;
            this.txtSalario.Size = new System.Drawing.Size(135, 23);
            this.txtSalario.TabIndex = 24;
            this.txtSalario.UseSystemPasswordChar = false;
            this.txtSalario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalario_KeyPress);
            // 
            // dtFechaFin
            // 
            this.dtFechaFin.CustomFormat = "dd-MM-yyyy";
            this.dtFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFechaFin.Location = new System.Drawing.Point(201, 207);
            this.dtFechaFin.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtFechaFin.Name = "dtFechaFin";
            this.dtFechaFin.Size = new System.Drawing.Size(137, 29);
            this.dtFechaFin.TabIndex = 23;
            this.dtFechaFin.Value = new System.DateTime(2018, 2, 4, 0, 0, 0, 0);
            // 
            // dtFechaInicio
            // 
            this.dtFechaInicio.CustomFormat = "dd-MM-yyyy";
            this.dtFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFechaInicio.Location = new System.Drawing.Point(201, 166);
            this.dtFechaInicio.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtFechaInicio.Name = "dtFechaInicio";
            this.dtFechaInicio.Size = new System.Drawing.Size(137, 29);
            this.dtFechaInicio.TabIndex = 22;
            this.dtFechaInicio.Value = new System.DateTime(2018, 2, 4, 0, 0, 0, 0);
            // 
            // boxPosicion
            // 
            this.boxPosicion.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.experienciasBindingSource, "PosicionID", true));
            this.boxPosicion.DataSource = this.experienciasBindingSource;
            this.boxPosicion.DisplayMember = "PosicionID";
            this.boxPosicion.FormattingEnabled = true;
            this.boxPosicion.ItemHeight = 23;
            this.boxPosicion.Location = new System.Drawing.Point(201, 127);
            this.boxPosicion.Name = "boxPosicion";
            this.boxPosicion.Size = new System.Drawing.Size(137, 29);
            this.boxPosicion.TabIndex = 20;
            this.boxPosicion.UseSelectable = true;
            this.boxPosicion.ValueMember = "PosicionID";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(40, 217);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(161, 19);
            this.materialLabel4.TabIndex = 13;
            this.materialLabel4.Text = "Fecha de Terminacion:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(87, 176);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(114, 19);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Fecha de Inicio:";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(141, 258);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(60, 19);
            this.materialLabel2.TabIndex = 9;
            this.materialLabel2.Text = "Salario:";
            // 
            // txtOrganizacion
            // 
            this.txtOrganizacion.Depth = 0;
            this.txtOrganizacion.Hint = "";
            this.txtOrganizacion.Location = new System.Drawing.Point(203, 88);
            this.txtOrganizacion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtOrganizacion.Name = "txtOrganizacion";
            this.txtOrganizacion.PasswordChar = '\0';
            this.txtOrganizacion.SelectedText = "";
            this.txtOrganizacion.SelectionLength = 0;
            this.txtOrganizacion.SelectionStart = 0;
            this.txtOrganizacion.Size = new System.Drawing.Size(207, 23);
            this.txtOrganizacion.TabIndex = 8;
            this.txtOrganizacion.UseSystemPasswordChar = false;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(101, 92);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(100, 19);
            this.materialLabel1.TabIndex = 7;
            this.materialLabel1.Text = "Organizacion:";
            // 
            // txtCodigoExperiencia
            // 
            this.txtCodigoExperiencia.Depth = 0;
            this.txtCodigoExperiencia.Hint = "";
            this.txtCodigoExperiencia.Location = new System.Drawing.Point(203, 19);
            this.txtCodigoExperiencia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCodigoExperiencia.Name = "txtCodigoExperiencia";
            this.txtCodigoExperiencia.PasswordChar = '\0';
            this.txtCodigoExperiencia.SelectedText = "";
            this.txtCodigoExperiencia.SelectionLength = 0;
            this.txtCodigoExperiencia.SelectionStart = 0;
            this.txtCodigoExperiencia.Size = new System.Drawing.Size(135, 23);
            this.txtCodigoExperiencia.TabIndex = 1;
            this.txtCodigoExperiencia.UseSystemPasswordChar = false;
            this.txtCodigoExperiencia.Click += new System.EventHandler(this.txtCodigoExperiencia_Click);
            this.txtCodigoExperiencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoExperiencia_KeyPress);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Depth = 0;
            this.lblCodigo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCodigo.Location = new System.Drawing.Point(140, 23);
            this.lblCodigo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(61, 19);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.Text = "Codigo:";
            // 
            // labDescripcion
            // 
            this.labDescripcion.AutoSize = true;
            this.labDescripcion.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labDescripcion.Depth = 0;
            this.labDescripcion.Font = new System.Drawing.Font("Roboto", 11F);
            this.labDescripcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labDescripcion.Location = new System.Drawing.Point(129, 137);
            this.labDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.labDescripcion.Name = "labDescripcion";
            this.labDescripcion.Size = new System.Drawing.Size(72, 19);
            this.labDescripcion.TabIndex = 2;
            this.labDescripcion.Text = "Posicion:";
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(589, 457);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 17;
            this.btnAgregar.Text = "Agregar ";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // AgregarExperiencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 620);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAgregar);
            this.Name = "AgregarExperiencia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agragar - Experiencia Laboral";
            this.Load += new System.EventHandler(this.AgragarCapacitacion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.experienciasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnSalir;
        public System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroDateTime dtFechaFin;
        private MetroFramework.Controls.MetroDateTime dtFechaInicio;
        private MetroFramework.Controls.MetroComboBox boxPosicion;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtOrganizacion;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        public MaterialSkin.Controls.MaterialSingleLineTextField txtCodigoExperiencia;
        private MaterialSkin.Controls.MaterialLabel lblCodigo;
        private MaterialSkin.Controls.MaterialLabel labDescripcion;
        public MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSalario;
        private System.Windows.Forms.BindingSource experienciasBindingSource;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        public MetroFramework.Controls.MetroComboBox boxCandidato;
        private System.Windows.Forms.BindingSource candidatosBindingSource;
    }
}