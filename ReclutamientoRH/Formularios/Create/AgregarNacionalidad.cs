﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.Create
{
    public partial class AgregarNacionalidad : MaterialForm
    {
        public AgregarNacionalidad()
        {
            InitializeComponent();
        }

        public int id;
        public string desp;
        public bool editar = false;

        private void AgregarNacionalidad_Load(object sender, EventArgs e)
        {
            if(editar == true)
            {
                txtCodigoNacionalidad.Text = id.ToString();
                txtNacionalidad.Text = desp;
            }
            else
            {
                txtCodigoNacionalidad.Text = "";
            }
            
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(editar == false)
            {
                if (txtCodigoNacionalidad.Text == "" || txtNacionalidad.Text == "")
                {
                    MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    DbContextRRHH db = new DbContextRRHH();
                    if (txtCodigoNacionalidad.Text.Length < 4)
                    {
                        MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {

                        id = int.Parse(txtCodigoNacionalidad.Text);
                        desp = txtNacionalidad.Text;

                        Nacionalidades nc = new Nacionalidades();

                        nc.NacionalidadID = id;
                        if (db.Nacionalidades.Any(d => d.NacionalidadID == id))
                        {
                            MessageBox.Show("Nacionalidad ya existe o Clave duplicada", "ERROR AL GUARDAR");
                            txtCodigoNacionalidad.Focus();
                        }
                        else
                        {
                            nc.NombreNacionalidad = desp;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(txtNacionalidad.Text, "^[a-zA-Z]") || db.Nacionalidades.Any(t => t.NombreNacionalidad == desp))
                            {
                                MessageBox.Show("Nacionalidad ya existe", "ERROR AL GUARDAR");

                                return;
                            }


                            db.Nacionalidades.Add(nc);
                            db.SaveChanges();
                            this.Limpiar();
                        }
                    }
                }
            }
            else
            {
                desp = txtNacionalidad.Text;
                
                DbContextRRHH db = new DbContextRRHH();
                var n = db.Nacionalidades.First(d => d.NacionalidadID == id);
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtNacionalidad.Text, "^[a-zA-Z]") || db.Nacionalidades.Any(t => t.NombreNacionalidad == desp))
                {
                    MessageBox.Show("Nacionalidad ya existe", "ERROR AL GUARDAR");

                    return;
                }
                else
                {
                    n.NombreNacionalidad = desp;
                
                    db.SaveChanges();
                    FormNacionalidades nac = new FormNacionalidades();  
                    nac.Show();
                    this.Close();
                }
            }
        }
        public void Limpiar()
        {
            txtCodigoNacionalidad.Text = "";
            txtNacionalidad.Text = "";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormNacionalidades nc = new FormNacionalidades();
            nc.Show();
            this.Close();
        }

        private void txtCodigoNacionalidad_Click(object sender, EventArgs e)
        {
            
        }

        private void txtCodigoNacionalidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
