﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios.CRUD
{
    public partial class AgregarCapacitacion : MaterialForm
    {
        public AgregarCapacitacion()
        {
            InitializeComponent();
        }

        public int id;
        public string descripcion;
        public int institucion;
        public string nivel;
        public DateTime fechaInicio;
        public DateTime fechaFin;
        public bool editar = false;

        private void AgregarCapacitacion_Load(object sender, EventArgs e)
        {

            using (DbContextRRHH db = new DbContextRRHH())
            {
                cbxInstitucion.DataSource = db.Instituciones.ToList();
                cbxInstitucion.ValueMember = "InstitucionID";
                cbxInstitucion.DisplayMember = "NombreInstitucion";
            }
            if (editar == true)
            {
                txtCodigoCapacitacion.Text = id.ToString();
                txtDescripcion.Text = descripcion.ToString();
                cbxInstitucion.SelectedValue = institucion;
                cxbNivel.Text = nivel.ToString();
                dtFechaInicio.Text = fechaInicio.ToString();
                dtFechaFin.Text = fechaFin.ToString();
            }
            else
            {

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(editar == false)
            {
                DbContextRRHH db = new DbContextRRHH();
                if (txtCodigoCapacitacion.Text.Length < 4)
                {
                    MessageBox.Show("El Codigo Digitado No es Valido", "ERROR AL GUARDAR");
                    return;
                }
                else
                {
                    if (txtCodigoCapacitacion.Text == "" || txtDescripcion.Text == "" || cbxInstitucion.Text == "" || cxbNivel.Text == "" || dtFechaInicio.Text == "" || dtFechaFin.Text == "")
                    {
                        MessageBox.Show("Todos Los Campos son Requeridos", "ERROR AL GUARDAR");
                        return;
                    }
                    else
                    {


                        id = int.Parse(txtCodigoCapacitacion.Text);
                        descripcion = txtDescripcion.Text;
                        institucion = int.Parse(cbxInstitucion.SelectedValue.ToString());
                        nivel = cxbNivel.Text;
                        fechaInicio = dtFechaInicio.Value;
                        fechaFin = dtFechaFin.Value;
                    }

                    Capacitaciones cp = new Capacitaciones();

                    cp.CapacitacionID = id;
                    if (db.Capacitaciones.Any(d => d.CapacitacionID == id))
                    {
                        MessageBox.Show("Capacitacion ya existe o Clave duplicada", "ERROR AL GUARDAR");
                        txtCodigoCapacitacion.Focus();
                        return;
                    }
                    else
                    {
                        cp.DescripcionCapacitacion = descripcion;
                        if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Capacitaciones.Any(t => t.DescripcionCapacitacion == descripcion && t.InstitucionID == institucion && t.CapacitacionID != id))
                        {
                            MessageBox.Show("No es permitido Caracteres numericos o Departmento ya existe", "ERROR AL GUARDAR");

                            return;
                        }
                        cp.InstitucionID = institucion;
                        cp.NivelCapacitacion = nivel;
                        cp.FechaDeInicioCapacitacion = fechaInicio;
                        cp.FechaFinCapacitacion = fechaFin;
                        if(fechaFin <= fechaInicio)
                        {
                            MessageBox.Show("La fecha de Fin no puede ser menor que la Fecha Inicio", "ERROR AL GUARDAR");
                            return;
                        }

                        

                        db.Capacitaciones.Add(cp);
                        db.SaveChanges();
                        this.Limpiar();
                    }

                }

            }
            else
            {
                descripcion = txtDescripcion.Text;
                institucion = int.Parse(cbxInstitucion.SelectedValue.ToString());
                nivel = cxbNivel.Text;
                fechaInicio = dtFechaInicio.Value;
                fechaFin = dtFechaFin.Value;
                DbContextRRHH db = new DbContextRRHH();
                var n = db.Capacitaciones.First(d => d.CapacitacionID == id);

                n.CapacitacionID = id;
                n.DescripcionCapacitacion = descripcion;
                n.InstitucionID = institucion;
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtDescripcion.Text, "^[a-zA-Z]") || db.Capacitaciones.Any(t => t.DescripcionCapacitacion == descripcion && t.InstitucionID == institucion && t.CapacitacionID != id ))
                {
                    MessageBox.Show("No es permitido Caracteres numericos o Capacitacion ya existe", "ERROR AL GUARDAR");

                    return;
                }
                else
                {
                    n.FechaDeInicioCapacitacion = fechaInicio;
                    n.FechaFinCapacitacion = fechaFin;
                    if (fechaFin <= fechaInicio)
                    {
                        MessageBox.Show("La fecha de Fin no puede ser menor que la Fecha Inicio", "ERROR AL GUARDAR");
                        return;
                    }
                    n.NivelCapacitacion = nivel;

                    db.SaveChanges();
                    FormCapacitaciones cap = new FormCapacitaciones();
                    cap.Show();
                    this.Close();
                }
            }




        }
        public void Limpiar()
        {
            txtCodigoCapacitacion.Text = "";
            txtDescripcion.Text = "";
            cxbNivel.Text = "";
            cbxInstitucion.Text = "";
            dtFechaFin.Text = "";
            dtFechaInicio.Text = "";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FormCapacitaciones cp = new FormCapacitaciones();
            cp.Show();
            this.Close();
        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void btnIntituciones_Click(object sender, EventArgs e)
        {
            AgregarInstitucion ai = new AgregarInstitucion();
            ai.Show();
            this.Hide();
        }
    }
}
