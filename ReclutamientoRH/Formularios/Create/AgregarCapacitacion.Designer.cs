﻿namespace ReclutamientoRH.Formularios.CRUD
{
    partial class AgregarCapacitacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtFechaFin = new MetroFramework.Controls.MetroDateTime();
            this.capacitacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtFechaInicio = new MetroFramework.Controls.MetroDateTime();
            this.cxbNivel = new MetroFramework.Controls.MetroComboBox();
            this.cbxInstitucion = new MetroFramework.Controls.MetroComboBox();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtDescripcion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCodigoCapacitacion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblCodigo = new MaterialSkin.Controls.MaterialLabel();
            this.labDescripcion = new MaterialSkin.Controls.MaterialLabel();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.capacitacionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSalir.Depth = 0;
            this.btnSalir.Location = new System.Drawing.Point(693, 456);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Primary = false;
            this.btnSalir.Size = new System.Drawing.Size(49, 36);
            this.btnSalir.TabIndex = 15;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.dtFechaFin);
            this.groupBox1.Controls.Add(this.dtFechaInicio);
            this.groupBox1.Controls.Add(this.cxbNivel);
            this.groupBox1.Controls.Add(this.cbxInstitucion);
            this.groupBox1.Controls.Add(this.materialLabel4);
            this.groupBox1.Controls.Add(this.materialLabel3);
            this.groupBox1.Controls.Add(this.materialLabel2);
            this.groupBox1.Controls.Add(this.txtDescripcion);
            this.groupBox1.Controls.Add(this.materialLabel1);
            this.groupBox1.Controls.Add(this.txtCodigoCapacitacion);
            this.groupBox1.Controls.Add(this.lblCodigo);
            this.groupBox1.Controls.Add(this.labDescripcion);
            this.groupBox1.Location = new System.Drawing.Point(158, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 325);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Capacitaciones";
            // 
            // dtFechaFin
            // 
            this.dtFechaFin.CustomFormat = "dd-MM-yyyy";
            this.dtFechaFin.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.capacitacionesBindingSource, "FechaFinCapacitacion", true));
            this.dtFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFechaFin.Location = new System.Drawing.Point(201, 202);
            this.dtFechaFin.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtFechaFin.Name = "dtFechaFin";
            this.dtFechaFin.Size = new System.Drawing.Size(137, 29);
            this.dtFechaFin.TabIndex = 23;
            this.dtFechaFin.Value = new System.DateTime(2018, 2, 4, 0, 0, 0, 0);
            // 
            // capacitacionesBindingSource
            // 
            this.capacitacionesBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Capacitaciones);
            // 
            // dtFechaInicio
            // 
            this.dtFechaInicio.CustomFormat = "dd-MM-yyyy";
            this.dtFechaInicio.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.capacitacionesBindingSource, "FechaDeInicioCapacitacion", true));
            this.dtFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFechaInicio.Location = new System.Drawing.Point(201, 168);
            this.dtFechaInicio.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtFechaInicio.Name = "dtFechaInicio";
            this.dtFechaInicio.Size = new System.Drawing.Size(137, 29);
            this.dtFechaInicio.TabIndex = 22;
            this.dtFechaInicio.Value = new System.DateTime(2018, 2, 4, 0, 0, 0, 0);
            // 
            // cxbNivel
            // 
            this.cxbNivel.FormattingEnabled = true;
            this.cxbNivel.ItemHeight = 23;
            this.cxbNivel.Items.AddRange(new object[] {
            "Grado",
            "Post-Grado",
            "Maestria",
            "Doctorado",
            "Tecnico",
            "Gestion"});
            this.cxbNivel.Location = new System.Drawing.Point(201, 133);
            this.cxbNivel.Name = "cxbNivel";
            this.cxbNivel.Size = new System.Drawing.Size(137, 29);
            this.cxbNivel.TabIndex = 21;
            this.cxbNivel.UseSelectable = true;
            // 
            // cbxInstitucion
            // 
            this.cbxInstitucion.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.capacitacionesBindingSource, "InstitucionID", true));
            this.cbxInstitucion.DataSource = this.capacitacionesBindingSource;
            this.cbxInstitucion.DisplayMember = "Instituciones";
            this.cbxInstitucion.FormattingEnabled = true;
            this.cbxInstitucion.ItemHeight = 23;
            this.cbxInstitucion.Location = new System.Drawing.Point(201, 98);
            this.cbxInstitucion.Name = "cbxInstitucion";
            this.cbxInstitucion.Size = new System.Drawing.Size(137, 29);
            this.cbxInstitucion.TabIndex = 20;
            this.cbxInstitucion.UseSelectable = true;
            this.cbxInstitucion.ValueMember = "InstitucionID";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(40, 208);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(161, 19);
            this.materialLabel4.TabIndex = 13;
            this.materialLabel4.Text = "Fecha de Terminacion:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(87, 171);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(114, 19);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Fecha de Inicio:";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(154, 137);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(47, 19);
            this.materialLabel2.TabIndex = 9;
            this.materialLabel2.Text = "Nivel:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.capacitacionesBindingSource, "DescripcionCapacitacion", true));
            this.txtDescripcion.Depth = 0;
            this.txtDescripcion.Hint = "";
            this.txtDescripcion.Location = new System.Drawing.Point(203, 59);
            this.txtDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.PasswordChar = '\0';
            this.txtDescripcion.SelectedText = "";
            this.txtDescripcion.SelectionLength = 0;
            this.txtDescripcion.SelectionStart = 0;
            this.txtDescripcion.Size = new System.Drawing.Size(207, 23);
            this.txtDescripcion.TabIndex = 8;
            this.txtDescripcion.UseSystemPasswordChar = false;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(108, 59);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(93, 19);
            this.materialLabel1.TabIndex = 7;
            this.materialLabel1.Text = "Descripcion:";
            this.materialLabel1.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // txtCodigoCapacitacion
            // 
            this.txtCodigoCapacitacion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.capacitacionesBindingSource, "CapacitacionID", true));
            this.txtCodigoCapacitacion.Depth = 0;
            this.txtCodigoCapacitacion.Hint = "";
            this.txtCodigoCapacitacion.Location = new System.Drawing.Point(203, 19);
            this.txtCodigoCapacitacion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCodigoCapacitacion.Name = "txtCodigoCapacitacion";
            this.txtCodigoCapacitacion.PasswordChar = '\0';
            this.txtCodigoCapacitacion.SelectedText = "";
            this.txtCodigoCapacitacion.SelectionLength = 0;
            this.txtCodigoCapacitacion.SelectionStart = 0;
            this.txtCodigoCapacitacion.Size = new System.Drawing.Size(135, 23);
            this.txtCodigoCapacitacion.TabIndex = 1;
            this.txtCodigoCapacitacion.UseSystemPasswordChar = false;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Depth = 0;
            this.lblCodigo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCodigo.Location = new System.Drawing.Point(140, 23);
            this.lblCodigo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(61, 19);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.Text = "Codigo:";
            // 
            // labDescripcion
            // 
            this.labDescripcion.AutoSize = true;
            this.labDescripcion.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labDescripcion.Depth = 0;
            this.labDescripcion.Font = new System.Drawing.Font("Roboto", 11F);
            this.labDescripcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labDescripcion.Location = new System.Drawing.Point(117, 98);
            this.labDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.labDescripcion.Name = "labDescripcion";
            this.labDescripcion.Size = new System.Drawing.Size(84, 19);
            this.labDescripcion.TabIndex = 2;
            this.labDescripcion.Text = "Institucion:";
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(588, 456);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 14;
            this.btnAgregar.Text = "Agregar ";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // AgregarCapacitacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 620);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAgregar);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.capacitacionesBindingSource, "FechaDeInicioCapacitacion", true));
            this.Name = "AgregarCapacitacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar - Capacitacion";
            this.Load += new System.EventHandler(this.AgregarCapacitacion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.capacitacionesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnSalir;
        public System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDescripcion;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        public MaterialSkin.Controls.MaterialSingleLineTextField txtCodigoCapacitacion;
        private MaterialSkin.Controls.MaterialLabel lblCodigo;
        private MaterialSkin.Controls.MaterialLabel labDescripcion;
        public MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private System.Windows.Forms.BindingSource capacitacionesBindingSource;
        private MetroFramework.Controls.MetroComboBox cbxInstitucion;
        private MetroFramework.Controls.MetroDateTime dtFechaFin;
        private MetroFramework.Controls.MetroDateTime dtFechaInicio;
        private MetroFramework.Controls.MetroComboBox cxbNivel;
    }
}