﻿namespace ReclutamientoRH.Formularios.CRUD
{
    partial class AgregarPosicion
    {
        

        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxDepartamentos = new MetroFramework.Controls.MetroComboBox();
            this.posicionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbRiesgo = new MetroFramework.Controls.MetroComboBox();
            this.boxEstado = new MetroFramework.Controls.MetroComboBox();
            this.txtMaximo = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtMinimo = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtDescripcion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.lblEstado = new MaterialSkin.Controls.MaterialLabel();
            this.txtCodigoPosicion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblCodigo = new MaterialSkin.Controls.MaterialLabel();
            this.labDescripcion = new MaterialSkin.Controls.MaterialLabel();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posicionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSalir.Depth = 0;
            this.btnSalir.Location = new System.Drawing.Point(678, 469);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Primary = false;
            this.btnSalir.Size = new System.Drawing.Size(49, 36);
            this.btnSalir.TabIndex = 12;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.cbxDepartamentos);
            this.groupBox1.Controls.Add(this.cmbRiesgo);
            this.groupBox1.Controls.Add(this.boxEstado);
            this.groupBox1.Controls.Add(this.txtMaximo);
            this.groupBox1.Controls.Add(this.materialLabel4);
            this.groupBox1.Controls.Add(this.txtMinimo);
            this.groupBox1.Controls.Add(this.materialLabel3);
            this.groupBox1.Controls.Add(this.materialLabel2);
            this.groupBox1.Controls.Add(this.txtDescripcion);
            this.groupBox1.Controls.Add(this.materialLabel1);
            this.groupBox1.Controls.Add(this.lblEstado);
            this.groupBox1.Controls.Add(this.txtCodigoPosicion);
            this.groupBox1.Controls.Add(this.lblCodigo);
            this.groupBox1.Controls.Add(this.labDescripcion);
            this.groupBox1.Location = new System.Drawing.Point(143, 140);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 325);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Posicion";
            // 
            // cbxDepartamentos
            // 
            this.cbxDepartamentos.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.posicionesBindingSource, "DepartamentoID", true));
            this.cbxDepartamentos.DataSource = this.posicionesBindingSource;
            this.cbxDepartamentos.DisplayMember = "Departamento";
            this.cbxDepartamentos.FormattingEnabled = true;
            this.cbxDepartamentos.ItemHeight = 23;
            this.cbxDepartamentos.Location = new System.Drawing.Point(203, 48);
            this.cbxDepartamentos.Name = "cbxDepartamentos";
            this.cbxDepartamentos.Size = new System.Drawing.Size(135, 29);
            this.cbxDepartamentos.TabIndex = 19;
            this.cbxDepartamentos.UseSelectable = true;
            this.cbxDepartamentos.ValueMember = "DepartamentoID";
            // 
            // posicionesBindingSource
            // 
            this.posicionesBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Posiciones);
            this.posicionesBindingSource.CurrentChanged += new System.EventHandler(this.posicionesBindingSource_CurrentChanged);
            // 
            // cmbRiesgo
            // 
            this.cmbRiesgo.FormattingEnabled = true;
            this.cmbRiesgo.ItemHeight = 23;
            this.cmbRiesgo.Items.AddRange(new object[] {
            "Alto",
            "Medio",
            "Bajo"});
            this.cmbRiesgo.Location = new System.Drawing.Point(203, 212);
            this.cmbRiesgo.Name = "cmbRiesgo";
            this.cmbRiesgo.Size = new System.Drawing.Size(135, 29);
            this.cmbRiesgo.TabIndex = 18;
            this.cmbRiesgo.UseSelectable = true;
            // 
            // boxEstado
            // 
            this.boxEstado.FormattingEnabled = true;
            this.boxEstado.ItemHeight = 23;
            this.boxEstado.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.boxEstado.Location = new System.Drawing.Point(203, 247);
            this.boxEstado.Name = "boxEstado";
            this.boxEstado.Size = new System.Drawing.Size(135, 29);
            this.boxEstado.TabIndex = 17;
            this.boxEstado.UseSelectable = true;
            // 
            // txtMaximo
            // 
            this.txtMaximo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.posicionesBindingSource, "SalaroMaximoPosicion", true));
            this.txtMaximo.Depth = 0;
            this.txtMaximo.Hint = "";
            this.txtMaximo.Location = new System.Drawing.Point(203, 175);
            this.txtMaximo.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMaximo.Name = "txtMaximo";
            this.txtMaximo.PasswordChar = '\0';
            this.txtMaximo.SelectedText = "";
            this.txtMaximo.SelectionLength = 0;
            this.txtMaximo.SelectionStart = 0;
            this.txtMaximo.Size = new System.Drawing.Size(135, 23);
            this.txtMaximo.TabIndex = 14;
            this.txtMaximo.UseSystemPasswordChar = false;
            this.txtMaximo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaximo_KeyPress);
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(86, 175);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(122, 19);
            this.materialLabel4.TabIndex = 13;
            this.materialLabel4.Text = "Salario Maximo: ";
            // 
            // txtMinimo
            // 
            this.txtMinimo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.posicionesBindingSource, "SalarioMinimoPosicion", true));
            this.txtMinimo.Depth = 0;
            this.txtMinimo.Hint = "";
            this.txtMinimo.Location = new System.Drawing.Point(203, 138);
            this.txtMinimo.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMinimo.Name = "txtMinimo";
            this.txtMinimo.PasswordChar = '\0';
            this.txtMinimo.SelectedText = "";
            this.txtMinimo.SelectionLength = 0;
            this.txtMinimo.SelectionStart = 0;
            this.txtMinimo.Size = new System.Drawing.Size(135, 23);
            this.txtMinimo.TabIndex = 12;
            this.txtMinimo.UseSystemPasswordChar = false;
            this.txtMinimo.Click += new System.EventHandler(this.txtMinimo_Click);
            this.txtMinimo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMinimo_KeyPress);
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(86, 138);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(115, 19);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Salario Minimo:";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(140, 221);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(59, 19);
            this.materialLabel2.TabIndex = 9;
            this.materialLabel2.Text = "Riesgo:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.posicionesBindingSource, "NombrePosicion", true));
            this.txtDescripcion.Depth = 0;
            this.txtDescripcion.Hint = "";
            this.txtDescripcion.Location = new System.Drawing.Point(203, 97);
            this.txtDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.PasswordChar = '\0';
            this.txtDescripcion.SelectedText = "";
            this.txtDescripcion.SelectionLength = 0;
            this.txtDescripcion.SelectionStart = 0;
            this.txtDescripcion.Size = new System.Drawing.Size(207, 23);
            this.txtDescripcion.TabIndex = 8;
            this.txtDescripcion.UseSystemPasswordChar = false;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(108, 97);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(93, 19);
            this.materialLabel1.TabIndex = 7;
            this.materialLabel1.Text = "Descripcion:";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEstado.Depth = 0;
            this.lblEstado.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblEstado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblEstado.Location = new System.Drawing.Point(139, 257);
            this.lblEstado.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(60, 19);
            this.lblEstado.TabIndex = 4;
            this.lblEstado.Text = "Estado:";
            // 
            // txtCodigoPosicion
            // 
            this.txtCodigoPosicion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.posicionesBindingSource, "PosicionID", true));
            this.txtCodigoPosicion.Depth = 0;
            this.txtCodigoPosicion.Hint = "";
            this.txtCodigoPosicion.Location = new System.Drawing.Point(203, 19);
            this.txtCodigoPosicion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCodigoPosicion.Name = "txtCodigoPosicion";
            this.txtCodigoPosicion.PasswordChar = '\0';
            this.txtCodigoPosicion.SelectedText = "";
            this.txtCodigoPosicion.SelectionLength = 0;
            this.txtCodigoPosicion.SelectionStart = 0;
            this.txtCodigoPosicion.Size = new System.Drawing.Size(135, 23);
            this.txtCodigoPosicion.TabIndex = 1;
            this.txtCodigoPosicion.UseSystemPasswordChar = false;
            this.txtCodigoPosicion.Click += new System.EventHandler(this.txtCodigoPosicion_Click);
            this.txtCodigoPosicion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoPosicion_KeyPress);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Depth = 0;
            this.lblCodigo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCodigo.Location = new System.Drawing.Point(140, 23);
            this.lblCodigo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(61, 19);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.Text = "Codigo:";
            // 
            // labDescripcion
            // 
            this.labDescripcion.AutoSize = true;
            this.labDescripcion.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labDescripcion.Depth = 0;
            this.labDescripcion.Font = new System.Drawing.Font("Roboto", 11F);
            this.labDescripcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labDescripcion.Location = new System.Drawing.Point(93, 58);
            this.labDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.labDescripcion.Name = "labDescripcion";
            this.labDescripcion.Size = new System.Drawing.Size(108, 19);
            this.labDescripcion.TabIndex = 2;
            this.labDescripcion.Text = "Departamento:";
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(573, 469);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 11;
            this.btnAgregar.Text = "Agregar ";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // AgregarPosicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 620);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAgregar);
            this.Name = "AgregarPosicion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar - Posiciones";
            this.Load += new System.EventHandler(this.AgregarPosicion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posicionesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnSalir;
        public System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialLabel lblEstado;
        public MaterialSkin.Controls.MaterialSingleLineTextField txtCodigoPosicion;
        private MaterialSkin.Controls.MaterialLabel lblCodigo;
        private MaterialSkin.Controls.MaterialLabel labDescripcion;
        public MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDescripcion;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        public MaterialSkin.Controls.MaterialSingleLineTextField txtMaximo;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        public MaterialSkin.Controls.MaterialSingleLineTextField txtMinimo;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private System.Windows.Forms.BindingSource posicionesBindingSource;
        private MetroFramework.Controls.MetroComboBox cbxDepartamentos;
        private MetroFramework.Controls.MetroComboBox cmbRiesgo;
        private MetroFramework.Controls.MetroComboBox boxEstado;
    }
    
}