﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.CRUD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormCapacitaciones : MaterialForm
    {
        public FormCapacitaciones()
        {
            InitializeComponent();
        }
        public string inicio ;
        public string fin;
        public string FiltroCap;

        private void FormCapacitaciones_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                var query1 = from c in db.Capacitaciones
                            join i in db.Instituciones on c.InstitucionID equals i.InstitucionID
                            select new
                            {
                                c.CapacitacionID,
                                c.DescripcionCapacitacion,
                                i.NombreInstitucion,
                                c.InstitucionID,
                                c.NivelCapacitacion,
                                c.FechaDeInicioCapacitacion,
                                c.FechaFinCapacitacion
                            };
                dataGridView1.DataSource = query1.ToList();
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH rh = new FormReclutamientoRH();
            rh.Show();
            this.Close();
        }

        private void txtFiltroCapacitaciones_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltroCapacitaciones_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if(txtFiltroCapacitaciones.Text == "")
                    {
                        var query1 = from c in db.Capacitaciones
                                     join i in db.Instituciones on c.InstitucionID equals i.InstitucionID
                                     select new
                                     {
                                         c.CapacitacionID,
                                         c.DescripcionCapacitacion,
                                         i.NombreInstitucion,
                                         c.InstitucionID,
                                         c.NivelCapacitacion,
                                         c.FechaDeInicioCapacitacion,
                                         c.FechaFinCapacitacion
                                     };
                        dataGridView1.DataSource = query1.ToList();
                    }
                    else
                    {
                        FiltroCap = txtFiltroCapacitaciones.Text;



                        var result = from c in db.Capacitaciones
                                     where c.CapacitacionID.ToString() == FiltroCap || c.DescripcionCapacitacion == FiltroCap
                                     join i in db.Instituciones on c.InstitucionID equals i.InstitucionID
                                     select new
                                     {
                                         c.CapacitacionID,
                                         c.DescripcionCapacitacion,
                                         i.NombreInstitucion,
                                         c.InstitucionID,
                                         c.NivelCapacitacion,
                                         c.FechaDeInicioCapacitacion,
                                         c.FechaFinCapacitacion

                                     };


                        dataGridView1.DataSource = result.ToList();
                    }
                }
            }

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarCapacitacion cp = new AgregarCapacitacion();
            cp.Show();
            this.Hide();
        }

        private void btnIntituciones_Click(object sender, EventArgs e)
        {
            FormInstitucionCapacitacion fic = new FormInstitucionCapacitacion();
            fic.Show();
            this.Hide();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarCapacitacion agc = new AgregarCapacitacion();
                agc.id = int.Parse(row.Cells[0].Value.ToString());
                agc.descripcion = row.Cells[1].Value.ToString();
                agc.institucion = int.Parse(row.Cells[3].Value.ToString());
                agc.nivel = row.Cells[4].Value.ToString();
                agc.fechaInicio = DateTime.Parse(row.Cells[5].Value.ToString());
                agc.fechaFin = DateTime.Parse(row.Cells[6].Value.ToString());
                agc.txtCodigoCapacitacion.Enabled = false;
                agc.groupBox1.Text = "Editar Capacitacion";
                agc.btnAgregar.Text = "Editar";
                agc.editar = true;
                agc.Text = "Editar - Capacitacion";
                agc.Show();
                this.Hide();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                if (bd.Candidatos.Any(q => q.CapacitacionID == id))
                {
                    MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                }
                else
                {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Capacitaciones.SingleOrDefault(d => d.CapacitacionID == id);

                            db.Capacitaciones.Remove(n);

                            db.SaveChanges();

                            var query1 = from c in db.Capacitaciones
                                         join i in db.Instituciones on c.InstitucionID equals i.InstitucionID
                                         select new
                                         {
                                             c.CapacitacionID,
                                             c.DescripcionCapacitacion,
                                             i.NombreInstitucion,
                                             c.InstitucionID,
                                             c.NivelCapacitacion,
                                             c.FechaDeInicioCapacitacion,
                                             c.FechaFinCapacitacion
                                         };
                            dataGridView1.DataSource = query1.ToList();
                        }
                    }
                }
            }
        }
    }
}
