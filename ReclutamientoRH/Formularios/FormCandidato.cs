﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.Create;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormCandidato : MaterialForm
    {
        public FormCandidato()
        {
            InitializeComponent();
        }
        public string FiltroCan;
        

        private void FormCandidato_Load(object sender, EventArgs e)
        {

            using (DbContextRRHH db = new DbContextRRHH())
            {

                var query = from c in db.Candidatos
                            join na in db.Nacionalidades on c.NacionalidadID equals na.NacionalidadID
                            join cp in db.Capacitaciones on c.CapacitacionID equals cp.CapacitacionID
                            join cm in db.Competencias on c.CompetenciaID equals cm.CompetenciaID
                            join len in db.Idiomas on c.IdiomaID equals len.IdiomaID
                            join ps in db.Posiciones on c.PosicionID equals ps.PosicionID
                            join dp in db.Departamentos on ps.DepartamentoID equals dp.DepartamentoID
                            
                            
                            select new
                            {
                                c.CandidatoID,
                                c.NombreCandidato,
                                c.ApellidosCandidato,
                                c.CedulaCandidato,
                                c.SalarioCandidato,
                                c.RecomendadoPor,
                                c.EstadoCandidato,
                                na.NombreNacionalidad,
                                cp.DescripcionCapacitacion,
                                c.NacionalidadID,
                                c.IdiomaID,
                                c.CompetenciaID,
                                c.CapacitacionID,
                                cm.NombreCompetencia,
                                len.NombreIdioma,
                                c.PosicionID,
                                ps.NombrePosicion,
                                dp.NombreDepartamento,
                                
                                


                            };
                dataGridView2.DataSource = query.ToList();
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH rh = new FormReclutamientoRH();
            rh.Show();
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarCandidato ca = new AgregarCandidato();
            ca.Show();
            this.Hide();
        }

        public void btnEditar_Click(object sender, EventArgs e)
        {
            AgregarCandidato cand = new AgregarCandidato();
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView2.SelectedRows[0];
                
                    cand.id = int.Parse(row.Cells[0].Value.ToString());
                    cand.nombre = row.Cells[1].Value.ToString();
                    cand.apellido = row.Cells[2].Value.ToString();
                    cand.cedula = long.Parse(row.Cells[3].Value.ToString());
                    cand.salario = int.Parse(row.Cells[4].Value.ToString());
                    cand.posicion = int.Parse(row.Cells[5].Value.ToString());
                    cand.capacitacion = int.Parse(row.Cells[8].Value.ToString());
                    cand.competencia = int.Parse(row.Cells[10].Value.ToString());
                    cand.nacionalidad = int.Parse(row.Cells[12].Value.ToString());
                    cand.idioma = int.Parse(row.Cells[14].Value.ToString());
                    cand.recomendacion = row.Cells[16].Value.ToString();
                    cand.estado = row.Cells[17].Value.ToString();
     
                if (bd.Candidatos.Any(q => q.EstadoCandidato != "Inactivo" || q.EstadoCandidato != "Descartado"))

                {
                    cand.editar = true;
                    cand.txtCodigoCandidato.Enabled = false;
                    cand.btnAgregar.Text = "Editar";
                    cand.groupBox1.Text = "Editar Candidato";
                    cand.Text = "Editar - Candidato";
                    cand.Show();
                    this.Hide();
                    

                }
                else
                {
                    MessageBox.Show("Este Candidato no puede ser Editado");
                    return;

                }
            }
            
            
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView2.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                    if(bd.Empleados.Any(y => y.EmpleadoID == id))
                    {
                        MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                        return;
                    }
                if (bd.Experiencias.Any(q => q.CandidatoID == id))
                {
                    MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                }
                else
                {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Candidatos.SingleOrDefault(d => d.CandidatoID == id);

                            db.Candidatos.Remove(n);

                            db.SaveChanges();

                            var query = from c in db.Candidatos
                                        join na in db.Nacionalidades on c.NacionalidadID equals na.NacionalidadID
                                        join cp in db.Capacitaciones on c.CapacitacionID equals cp.CapacitacionID
                                        join cm in db.Competencias on c.CompetenciaID equals cm.CompetenciaID
                                        join len in db.Idiomas on c.IdiomaID equals len.IdiomaID
                                        join ps in db.Posiciones on c.PosicionID equals ps.PosicionID
                                        join dp in db.Departamentos on ps.DepartamentoID equals dp.DepartamentoID
                                        select new
                                        {
                                            c.CandidatoID,
                                            c.NombreCandidato,
                                            c.ApellidosCandidato,
                                            c.CedulaCandidato,
                                            c.SalarioCandidato,
                                            c.RecomendadoPor,
                                            c.EstadoCandidato,
                                            na.NombreNacionalidad,
                                            cp.DescripcionCapacitacion,
                                            cm.NombreCompetencia,
                                            len.NombreIdioma,
                                            ps.NombrePosicion,
                                            dp.NombreDepartamento


                                        };
                            dataGridView2.DataSource = query.ToList();
                        }
                    }
                }
            }
        }

        private void txtFiltroCandidato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroCandidato.Text == "")
                    {


                        var query = from c in db.Candidatos
                                    join na in db.Nacionalidades on c.NacionalidadID equals na.NacionalidadID
                                    join cp in db.Capacitaciones on c.CapacitacionID equals cp.CapacitacionID
                                    join cm in db.Competencias on c.CompetenciaID equals cm.CompetenciaID
                                    join len in db.Idiomas on c.IdiomaID equals len.IdiomaID
                                    join ps in db.Posiciones on c.PosicionID equals ps.PosicionID
                                    join dp in db.Departamentos on ps.DepartamentoID equals dp.DepartamentoID
                                    join exp in db.Experiencias on c.CandidatoID equals exp.CandidatoID

                                    select new
                                    {
                                        c.CandidatoID,
                                        c.NombreCandidato,
                                        c.ApellidosCandidato,
                                        c.CedulaCandidato,
                                        c.SalarioCandidato,
                                        c.RecomendadoPor,
                                        c.EstadoCandidato,
                                        na.NombreNacionalidad,
                                        cp.DescripcionCapacitacion,
                                        c.NacionalidadID,
                                        c.IdiomaID,
                                        c.CompetenciaID,
                                        c.CapacitacionID,
                                        cm.NombreCompetencia,
                                        len.NombreIdioma,
                                        c.PosicionID,
                                        ps.NombrePosicion,
                                        dp.NombreDepartamento,
                                        exp.OrganizacionExperiencia



                                    };
                        dataGridView2.DataSource = query.ToList();
                    }
                    else
                    {
                        FiltroCan = txtFiltroCandidato.Text;

                        var query = from c in db.Candidatos
                                    where c.CandidatoID.ToString() == FiltroCan || c.NombreCandidato == FiltroCan || c.EstadoCandidato == FiltroCan
                                    join na in db.Nacionalidades on c.NacionalidadID equals na.NacionalidadID
                                    join cp in db.Capacitaciones on c.CapacitacionID equals cp.CapacitacionID
                                    join cm in db.Competencias on c.CompetenciaID equals cm.CompetenciaID
                                    join len in db.Idiomas on c.IdiomaID equals len.IdiomaID
                                    join ps in db.Posiciones on c.PosicionID equals ps.PosicionID
                                    join dp in db.Departamentos on ps.DepartamentoID equals dp.DepartamentoID
                                    join exp in db.Experiencias on c.CandidatoID equals exp.CandidatoID

                                    select new
                                    {
                                        c.CandidatoID,
                                        c.NombreCandidato,
                                        c.ApellidosCandidato,
                                        c.CedulaCandidato,
                                        c.SalarioCandidato,
                                        c.RecomendadoPor,
                                        c.EstadoCandidato,
                                        na.NombreNacionalidad,
                                        cp.DescripcionCapacitacion,
                                        c.NacionalidadID,
                                        c.IdiomaID,
                                        c.CompetenciaID,
                                        c.CapacitacionID,
                                        cm.NombreCompetencia,
                                        len.NombreIdioma,
                                        c.PosicionID,
                                        ps.NombrePosicion,
                                        dp.NombreDepartamento,
                                        exp.OrganizacionExperiencia



                                    };
                        dataGridView2.DataSource = query.ToList();
                    }
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnContratar_Click(object sender, EventArgs e)
        {
          
            
            DataGridViewRow row = this.dataGridView2.SelectedRows[0];
            Empleados emp = new Empleados();

            emp.EmpleadoID = int.Parse(row.Cells[0].Value.ToString());
            emp.NombreEmpleado = row.Cells[1].Value.ToString();
            emp.CedulaEmpleado = long.Parse(row.Cells[3].Value.ToString());
            emp.ApellidoEmpleado = row.Cells[2].Value.ToString();
            emp.SalarioEmpleado = int.Parse(row.Cells[4].Value.ToString());
            emp.PosicionID = int.Parse(row.Cells[5].Value.ToString());
            emp.NacionalidadIDEmpleado = int.Parse(row.Cells[12].Value.ToString());
            emp.EstadoEmpleado = "Contratado";
            emp.FechaContratacionEmpleado = DateTime.Now;
            

            


            AgregarCandidato cand = new AgregarCandidato();

            
            cand.estado = row.Cells[17].Value.ToString();


            if (cand.estado == "En Espera" )
            {
                DbContextRRHH db = new DbContextRRHH();

                db.Empleados.Add(emp);

                var can = db.Candidatos.Where(x => x.CandidatoID == emp.EmpleadoID).Select(x => x).FirstOrDefault();
                can.EstadoCandidato = "Contratado";
                
                db.SaveChanges();
                MessageBox.Show("El Candidato a Sido Aprovado Exitosamente!");
                var query = from c in db.Candidatos
                            join na in db.Nacionalidades on c.NacionalidadID equals na.NacionalidadID
                            join cp in db.Capacitaciones on c.CapacitacionID equals cp.CapacitacionID
                            join cm in db.Competencias on c.CompetenciaID equals cm.CompetenciaID
                            join len in db.Idiomas on c.IdiomaID equals len.IdiomaID
                            join ps in db.Posiciones on c.PosicionID equals ps.PosicionID
                            join dp in db.Departamentos on ps.DepartamentoID equals dp.DepartamentoID
                            select new
                            {
                                c.CandidatoID,
                                c.NombreCandidato,
                                c.ApellidosCandidato,
                                c.CedulaCandidato,
                                c.SalarioCandidato,
                                c.RecomendadoPor,
                                c.EstadoCandidato,
                                na.NombreNacionalidad,
                                cp.DescripcionCapacitacion,
                                cm.NombreCompetencia,
                                len.NombreIdioma,
                                ps.NombrePosicion,
                                dp.NombreDepartamento


                            };
                dataGridView2.DataSource = query.ToList();
                
                db.Dispose();
            }
            else
            {
                MessageBox.Show("El Candidato Fue rechasado o Ya fue contratado"," NO ES POSIBLE CREAR ESTA ACCION");
            }
            


        }

        private void btnExperiencias_Click(object sender, EventArgs e)
        {
            FormExperiencia ex = new FormExperiencia();
            ex.Show();
            this.Hide();
        }
    }
}
