﻿namespace ReclutamientoRH.Formularios
{
    partial class FormEmpleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnVolver = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnTerminacion = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtFiltroEmpleado = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnImprimir = new MaterialSkin.Controls.MaterialFlatButton();
            this.empleadosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.empleadoIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreEmpleadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidoEmpleadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cedulaEmpleadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salarioEmpleadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoEmpleadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empleadosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.AutoSize = true;
            this.btnVolver.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVolver.Depth = 0;
            this.btnVolver.Location = new System.Drawing.Point(985, 520);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnVolver.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Primary = false;
            this.btnVolver.Size = new System.Drawing.Size(63, 36);
            this.btnVolver.TabIndex = 21;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnTerminacion
            // 
            this.btnTerminacion.AutoSize = true;
            this.btnTerminacion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnTerminacion.Depth = 0;
            this.btnTerminacion.Location = new System.Drawing.Point(792, 520);
            this.btnTerminacion.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnTerminacion.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnTerminacion.Name = "btnTerminacion";
            this.btnTerminacion.Primary = false;
            this.btnTerminacion.Size = new System.Drawing.Size(155, 36);
            this.btnTerminacion.TabIndex = 20;
            this.btnTerminacion.Text = "Terminar Contrato";
            this.btnTerminacion.UseVisualStyleBackColor = true;
            this.btnTerminacion.Click += new System.EventHandler(this.btnTerminacion_Click);
            // 
            // txtFiltroEmpleado
            // 
            this.txtFiltroEmpleado.Depth = 0;
            this.txtFiltroEmpleado.Hint = "";
            this.txtFiltroEmpleado.Location = new System.Drawing.Point(851, 74);
            this.txtFiltroEmpleado.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtFiltroEmpleado.Name = "txtFiltroEmpleado";
            this.txtFiltroEmpleado.PasswordChar = '\0';
            this.txtFiltroEmpleado.SelectedText = "";
            this.txtFiltroEmpleado.SelectionLength = 0;
            this.txtFiltroEmpleado.SelectionStart = 0;
            this.txtFiltroEmpleado.Size = new System.Drawing.Size(200, 23);
            this.txtFiltroEmpleado.TabIndex = 17;
            this.txtFiltroEmpleado.UseSystemPasswordChar = false;
            this.txtFiltroEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltroEmpleado_KeyPress);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(764, 74);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(80, 19);
            this.materialLabel1.TabIndex = 16;
            this.materialLabel1.Text = "Empleado:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(28, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1043, 408);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Empleados";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.empleadoIDDataGridViewTextBoxColumn,
            this.nombreEmpleadoDataGridViewTextBoxColumn,
            this.apellidoEmpleadoDataGridViewTextBoxColumn,
            this.cedulaEmpleadoDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn2,
            this.salarioEmpleadoDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn3,
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn,
            this.estadoEmpleadoDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.empleadosBindingSource;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.Location = new System.Drawing.Point(16, 34);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1007, 354);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnImprimir
            // 
            this.btnImprimir.AutoSize = true;
            this.btnImprimir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnImprimir.Depth = 0;
            this.btnImprimir.Location = new System.Drawing.Point(604, 520);
            this.btnImprimir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnImprimir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Primary = false;
            this.btnImprimir.Size = new System.Drawing.Size(140, 36);
            this.btnImprimir.TabIndex = 22;
            this.btnImprimir.Text = "Imprimer Reporte";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // empleadosBindingSource
            // 
            this.empleadosBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Empleados);
            // 
            // empleadoIDDataGridViewTextBoxColumn
            // 
            this.empleadoIDDataGridViewTextBoxColumn.DataPropertyName = "EmpleadoID";
            this.empleadoIDDataGridViewTextBoxColumn.FillWeight = 365.4822F;
            this.empleadoIDDataGridViewTextBoxColumn.HeaderText = "Empleado";
            this.empleadoIDDataGridViewTextBoxColumn.Name = "empleadoIDDataGridViewTextBoxColumn";
            this.empleadoIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.empleadoIDDataGridViewTextBoxColumn.Width = 80;
            // 
            // nombreEmpleadoDataGridViewTextBoxColumn
            // 
            this.nombreEmpleadoDataGridViewTextBoxColumn.DataPropertyName = "NombreEmpleado";
            this.nombreEmpleadoDataGridViewTextBoxColumn.FillWeight = 66.81472F;
            this.nombreEmpleadoDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreEmpleadoDataGridViewTextBoxColumn.Name = "nombreEmpleadoDataGridViewTextBoxColumn";
            this.nombreEmpleadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // apellidoEmpleadoDataGridViewTextBoxColumn
            // 
            this.apellidoEmpleadoDataGridViewTextBoxColumn.DataPropertyName = "ApellidoEmpleado";
            this.apellidoEmpleadoDataGridViewTextBoxColumn.FillWeight = 66.81472F;
            this.apellidoEmpleadoDataGridViewTextBoxColumn.HeaderText = "Apellido";
            this.apellidoEmpleadoDataGridViewTextBoxColumn.Name = "apellidoEmpleadoDataGridViewTextBoxColumn";
            this.apellidoEmpleadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cedulaEmpleadoDataGridViewTextBoxColumn
            // 
            this.cedulaEmpleadoDataGridViewTextBoxColumn.DataPropertyName = "CedulaEmpleado";
            this.cedulaEmpleadoDataGridViewTextBoxColumn.FillWeight = 66.81472F;
            this.cedulaEmpleadoDataGridViewTextBoxColumn.HeaderText = "Indentificacion";
            this.cedulaEmpleadoDataGridViewTextBoxColumn.Name = "cedulaEmpleadoDataGridViewTextBoxColumn";
            this.cedulaEmpleadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NombreNacionalidad";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nacionalidad";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // salarioEmpleadoDataGridViewTextBoxColumn
            // 
            this.salarioEmpleadoDataGridViewTextBoxColumn.DataPropertyName = "SalarioEmpleado";
            this.salarioEmpleadoDataGridViewTextBoxColumn.FillWeight = 66.81472F;
            this.salarioEmpleadoDataGridViewTextBoxColumn.HeaderText = "Salario";
            this.salarioEmpleadoDataGridViewTextBoxColumn.Name = "salarioEmpleadoDataGridViewTextBoxColumn";
            this.salarioEmpleadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.salarioEmpleadoDataGridViewTextBoxColumn.Width = 80;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "NombrePosicion";
            this.dataGridViewTextBoxColumn3.HeaderText = "Posicion";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // fechaContratacionEmpleadoDataGridViewTextBoxColumn
            // 
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn.DataPropertyName = "FechaContratacionEmpleado";
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn.FillWeight = 66.81472F;
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn.HeaderText = "Fecha de Contratacion";
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn.Name = "fechaContratacionEmpleadoDataGridViewTextBoxColumn";
            this.fechaContratacionEmpleadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoEmpleadoDataGridViewTextBoxColumn
            // 
            this.estadoEmpleadoDataGridViewTextBoxColumn.DataPropertyName = "EstadoEmpleado";
            this.estadoEmpleadoDataGridViewTextBoxColumn.FillWeight = 66.81472F;
            this.estadoEmpleadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoEmpleadoDataGridViewTextBoxColumn.Name = "estadoEmpleadoDataGridViewTextBoxColumn";
            this.estadoEmpleadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.estadoEmpleadoDataGridViewTextBoxColumn.Width = 83;
            // 
            // FormEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 660);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnTerminacion);
            this.Controls.Add(this.txtFiltroEmpleado);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormEmpleados";
            this.Text = "Formulario de Empleados";
            this.Load += new System.EventHandler(this.FormEmpleados_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empleadosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnVolver;
        private MaterialSkin.Controls.MaterialFlatButton btnTerminacion;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtFiltroEmpleado;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MaterialSkin.Controls.MaterialFlatButton btnImprimir;
        private System.Windows.Forms.DataGridViewTextBoxColumn nacionalidadIDEmpleadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn posicionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource empleadosBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn empleadoIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreEmpleadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidoEmpleadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cedulaEmpleadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn salarioEmpleadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaContratacionEmpleadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoEmpleadoDataGridViewTextBoxColumn;
    }
}