﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormLogin : MaterialForm
    {
        public FormLogin()
        {
            InitializeComponent();

        }
        public string usuario;
        public string clave;

        private void btnEntrar_Click(object sender, EventArgs e)

        {
            usuario = txtUsuario.Text;
            clave = txtContrasenia.Text;

            User us = new User();

            us.Usuario = usuario;
            us.Contrasenia = clave;

            DbContextRRHH db = new DbContextRRHH();
            if(db.User.Any(q => q.Usuario == usuario && q.Contrasenia == clave))
            {               
                 FormReclutamientoRH RH = new FormReclutamientoRH();
                 RH.Show();
                 this.Hide();   
            }
            else
            {
                MessageBox.Show("Nombre de Usuario no existe o Contraseña incorrecta", "ERROR DE AUTENTIFICACION");
            }
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtContrasenia_Click(object sender, EventArgs e)
        {

        }

        private void FormLogin_Load(object sender, EventArgs e)
        {

        }

        private void txtContrasenia_KeyPress(object sender, KeyPressEventArgs e)
        {
            

        }
    }
}
