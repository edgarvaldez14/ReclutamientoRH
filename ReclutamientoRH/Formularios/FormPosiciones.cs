﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.CRUD;
using ReclutamientoRH.Modelos;

namespace ReclutamientoRH.Formularios
{
    public partial class FormPosiciones : MaterialForm
    {
        public FormPosiciones()
        {
            InitializeComponent();
        }
        public string FiltroPos;

        public void FormPosiciones_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {

                var query = from p in db.Posiciones
                            join d in db.Departamentos on p.DepartamentoID equals d.DepartamentoID
                            select new
                            {
                                p.PosicionID,
                                p.NombrePosicion,
                                p.DepartamentoID,
                                d.NombreDepartamento,
                                p.SalarioMinimoPosicion,
                                p.SalaroMaximoPosicion,
                                p.RiesgoPosicion,
                                p.EstadoPosicion

                            };
                dataGridView1.DataSource = query.ToList();
            }

        }
        private void posicionesBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH RH = new FormReclutamientoRH();
            RH.Show();
            this.Hide();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarPosicion agp = new AgregarPosicion();
            agp.Show();
            this.Hide();
        }

        private void txtFiltroPosiciones_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltroPosiciones_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroPosiciones.Text == "")
                    {

                        

                            var query = from p in db.Posiciones
                                        join d in db.Departamentos on p.DepartamentoID equals d.DepartamentoID
                                        select new
                                        {
                                            p.PosicionID,
                                            p.NombrePosicion,
                                            p.DepartamentoID,
                                            d.NombreDepartamento,
                                            p.SalarioMinimoPosicion,
                                            p.SalaroMaximoPosicion,
                                            p.RiesgoPosicion,
                                            p.EstadoPosicion

                                        };
                            dataGridView1.DataSource = query.ToList();
                        

                    }
                    else
                    {
                        FiltroPos = txtFiltroPosiciones.Text;



                        var result = from d in db.Posiciones
                                     where d.PosicionID.ToString() == FiltroPos || d.NombrePosicion == FiltroPos
                                     join t in db.Departamentos on d.DepartamentoID equals t.DepartamentoID
                                     select new 
                                     {
                                         d.PosicionID,
                                         d.NombrePosicion,
                                         t.DepartamentoID,
                                         t.NombreDepartamento,
                                         d.SalarioMinimoPosicion,
                                         d.SalaroMaximoPosicion,
                                         d.RiesgoPosicion,
                                         d.EstadoPosicion

                                     };


                        dataGridView1.DataSource = result.ToList();
                    }
                }

            }
        }

        private void lbPosiciones_Click(object sender, EventArgs e)
        {

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarPosicion pos = new AgregarPosicion();
                pos.id = int.Parse(row.Cells[0].Value.ToString());
                pos.posicion = row.Cells[1].Value.ToString();
                pos.dep = int.Parse(row.Cells[2].Value.ToString());
                pos.minimo = int.Parse(row.Cells[4].Value.ToString());
                pos.maximo = int.Parse(row.Cells[5].Value.ToString());
                pos.riesgo = row.Cells[6].Value.ToString();
                pos.estado = row.Cells[7].Value.ToString();
                pos.editar = true;
                pos.txtCodigoPosicion.Enabled = false;
                pos.btnAgregar.Text = "Editar";
                pos.groupBox1.Text = "Editar Posicion";
                pos.Text = "Editar - Posiciones";
                pos.Show();
                this.Hide();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
                {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
                }
                else
                {
                    DbContextRRHH bd = new DbContextRRHH();
                    DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                    int id = int.Parse(row.Cells[0].Value.ToString());
                    if (bd.Candidatos.Any(q => q.PosicionID == id))
                {
                    MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                }
                else
                {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Posiciones.SingleOrDefault(d => d.PosicionID == id);

                            db.Posiciones.Remove(n);

                            db.SaveChanges();

                            var query = from p in db.Posiciones
                                        join d in db.Departamentos on p.DepartamentoID equals d.DepartamentoID
                                        select new
                                        {
                                            p.PosicionID,
                                            p.NombrePosicion,
                                            p.DepartamentoID,
                                            d.NombreDepartamento,
                                            p.SalarioMinimoPosicion,
                                            p.SalaroMaximoPosicion,
                                            p.RiesgoPosicion,
                                            p.EstadoPosicion

                                        };
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                }
            }
        }
    }
}