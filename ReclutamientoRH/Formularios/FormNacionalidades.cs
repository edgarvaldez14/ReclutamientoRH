﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.Create;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormNacionalidades : MaterialForm
    {
        public FormNacionalidades()
        {
            InitializeComponent();
        }
        public string FiltroNac;

        private void FormNacionalidades_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                nacionalidadesBindingSource.DataSource = db.Nacionalidades.ToList();
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH rh = new FormReclutamientoRH();
            rh.Show();
            this.Hide();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarNacionalidad nc = new AgregarNacionalidad();
            nc.Show();
            this.Hide();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                    if (bd.Candidatos.Any(q => q.NacionalidadID == id))
                    {
                        MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                    }
                    else
                    {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Nacionalidades.SingleOrDefault(d => d.NacionalidadID == id);

                            db.Nacionalidades.Remove(n);

                            db.SaveChanges();

                            dataGridView1.DataSource = db.Nacionalidades.ToList();
                        }
                    }
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarNacionalidad nac = new AgregarNacionalidad();
                nac.id = int.Parse(row.Cells[0].Value.ToString());
                nac.desp = row.Cells[1].Value.ToString();
                nac.editar = true;
                nac.groupBox1.Text = "Editar Nacionalidad";
                nac.txtCodigoNacionalidad.Enabled = false;
                nac.Text = "Editar Nacionalidad";
                nac.btnAgregar.Text = "Editar";
                nac.Show();
                this.Hide();
            }
        }

        private void txtFiltroNacionalidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroNacionalidad.Text == "")
                    {

                        dataGridView1.DataSource = db.Nacionalidades.ToList();
                        nacionalidadesBindingSource.DataSource = db.Nacionalidades.ToList();

                    }
                    else
                    {
                        FiltroNac = txtFiltroNacionalidad.Text;



                        var result = from d in db.Nacionalidades
                                     where d.NacionalidadID.ToString() == FiltroNac || d.NombreNacionalidad == FiltroNac
                                     select d;


                        dataGridView1.DataSource = result.ToList();
                    }
                }

            }
        }
    }
}
