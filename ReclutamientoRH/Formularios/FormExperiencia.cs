﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.Create;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormExperiencia : MaterialForm
    {
        public FormExperiencia()
        {
            InitializeComponent();
        }
        public string FiltroExp;

        private void FormExperiencia_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                var query = from exp in db.Experiencias
                            join p in db.Posiciones on exp.PosicionID equals p.PosicionID
                            join c in db.Candidatos on exp.CandidatoID equals c.CandidatoID
                            select new
                            {
                                exp.ExperienciaID,
                                exp.CandidatoID,
                                c.NombreCandidato,
                                exp.OrganizacionExperiencia,
                                exp.FechaEntradaExperiencia,
                                exp.fechaSalidaExperiencia,
                                exp.SalarioExperiencia,
                                p.NombrePosicion

                            };
                dataGridView1.DataSource = query.ToList();
            }
        }

       

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormCandidato rh = new FormCandidato();
            rh.Show();
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarExperiencia ex = new AgregarExperiencia();
            ex.Show();
            this.Hide();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarExperiencia exp = new AgregarExperiencia();
                exp.id = int.Parse(row.Cells[0].Value.ToString());
                exp.can = int.Parse(row.Cells[1].Value.ToString());
                exp.org = row.Cells[3].Value.ToString();
                exp.inicio = DateTime.Parse(row.Cells[4].Value.ToString());
                exp.fin = DateTime.Parse(row.Cells[5].Value.ToString());
                exp.salario = int.Parse(row.Cells[6].Value.ToString());
                exp.txtCodigoExperiencia.Enabled = false;
                exp.boxCandidato.Enabled = false;
                exp.groupBox1.Text = "Editar Experiencia Laboral";
                exp.btnAgregar.Text = "Editar";
                exp.editar = true;
                exp.Text = "Editar - Capacitacion";
                exp.Show();
                this.Hide();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                if (bd.Candidatos.Any(q => q.CandidatoID == id))
                {
                    MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                }
                else
                {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Experiencias.SingleOrDefault(d => d.ExperienciaID == id);

                            db.Experiencias.Remove(n);

                            db.SaveChanges();

                            var query = from exp in db.Experiencias

                                        join p in db.Posiciones on exp.PosicionID equals p.PosicionID
                                        select new
                                        {
                                            exp.ExperienciaID,
                                            exp.CandidatoID,
                                            exp.OrganizacionExperiencia,
                                            exp.FechaEntradaExperiencia,
                                            exp.fechaSalidaExperiencia,
                                            exp.SalarioExperiencia,
                                            p.NombrePosicion

                                        };
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                }
            }

        }

       

        private void txtFiltroExperiencia_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltroExperiencia_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroExperiencia.Text == "")
                    {
                        var query = from exp in db.Experiencias
                                    join p in db.Posiciones on exp.PosicionID equals p.PosicionID
                                    select new
                                    {
                                        exp.ExperienciaID,
                                        exp.CandidatoID,
                                        exp.OrganizacionExperiencia,
                                        exp.FechaEntradaExperiencia,
                                        exp.fechaSalidaExperiencia,
                                        exp.SalarioExperiencia,
                                        p.NombrePosicion

                                    };
                        dataGridView1.DataSource = query.ToList();
                    }
                    else
                    {



                        FiltroExp = txtFiltroExperiencia.Text;

                        var query = from exp in db.Experiencias
                                    where exp.ExperienciaID.ToString() == FiltroExp || exp.OrganizacionExperiencia == FiltroExp
                                    join p in db.Posiciones on exp.PosicionID equals p.PosicionID
                                    select new
                                    {
                                        exp.ExperienciaID,
                                        exp.CandidatoID,
                                        exp.OrganizacionExperiencia,
                                        exp.FechaEntradaExperiencia,
                                        exp.fechaSalidaExperiencia,
                                        exp.SalarioExperiencia,
                                        p.NombrePosicion

                                    };

                        dataGridView1.DataSource = query.ToList();
                    }
                
                }
            }
        }
    }
    
}
