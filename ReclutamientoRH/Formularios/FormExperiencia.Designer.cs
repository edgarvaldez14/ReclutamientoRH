﻿namespace ReclutamientoRH.Formularios
{
    partial class FormExperiencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnVolver = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEditar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtFiltroExperiencia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.experienciasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.experienciaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CandidatoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidatoIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.organizacionExperienciaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salarioExperienciaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.experienciasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.AutoSize = true;
            this.btnVolver.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVolver.Depth = 0;
            this.btnVolver.Location = new System.Drawing.Point(857, 535);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnVolver.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Primary = false;
            this.btnVolver.Size = new System.Drawing.Size(63, 36);
            this.btnVolver.TabIndex = 20;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Location = new System.Drawing.Point(714, 535);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(74, 36);
            this.btnEliminar.TabIndex = 19;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.AutoSize = true;
            this.btnEditar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditar.Depth = 0;
            this.btnEditar.Location = new System.Drawing.Point(647, 535);
            this.btnEditar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEditar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Primary = false;
            this.btnEditar.Size = new System.Drawing.Size(59, 36);
            this.btnEditar.TabIndex = 18;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(565, 535);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 17;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtFiltroExperiencia
            // 
            this.txtFiltroExperiencia.Depth = 0;
            this.txtFiltroExperiencia.Hint = "";
            this.txtFiltroExperiencia.Location = new System.Drawing.Point(782, 103);
            this.txtFiltroExperiencia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtFiltroExperiencia.Name = "txtFiltroExperiencia";
            this.txtFiltroExperiencia.PasswordChar = '\0';
            this.txtFiltroExperiencia.SelectedText = "";
            this.txtFiltroExperiencia.SelectionLength = 0;
            this.txtFiltroExperiencia.SelectionStart = 0;
            this.txtFiltroExperiencia.Size = new System.Drawing.Size(200, 23);
            this.txtFiltroExperiencia.TabIndex = 16;
            this.txtFiltroExperiencia.UseSystemPasswordChar = false;
            this.txtFiltroExperiencia.Click += new System.EventHandler(this.txtFiltroExperiencia_Click);
            this.txtFiltroExperiencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltroExperiencia_KeyPress_1);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(649, 103);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(136, 19);
            this.materialLabel1.TabIndex = 15;
            this.materialLabel1.Text = "IdiomaExperiencia:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(152, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(830, 394);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Experiencia Laboral";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.experienciaIDDataGridViewTextBoxColumn,
            this.CandidatoID,
            this.candidatoIDDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.organizacionExperienciaDataGridViewTextBoxColumn,
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn,
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn,
            this.salarioExperienciaDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn2});
            this.dataGridView1.DataSource = this.experienciasBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(16, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(808, 346);
            this.dataGridView1.TabIndex = 0;
            // 
            // experienciasBindingSource
            // 
            this.experienciasBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Experiencias);
            // 
            // experienciaIDDataGridViewTextBoxColumn
            // 
            this.experienciaIDDataGridViewTextBoxColumn.DataPropertyName = "ExperienciaID";
            this.experienciaIDDataGridViewTextBoxColumn.HeaderText = "Codigo";
            this.experienciaIDDataGridViewTextBoxColumn.Name = "experienciaIDDataGridViewTextBoxColumn";
            this.experienciaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.experienciaIDDataGridViewTextBoxColumn.Width = 60;
            // 
            // CandidatoID
            // 
            this.CandidatoID.DataPropertyName = "CandidatoID";
            this.CandidatoID.HeaderText = "CandidatoID";
            this.CandidatoID.Name = "CandidatoID";
            this.CandidatoID.ReadOnly = true;
            this.CandidatoID.Visible = false;
            // 
            // candidatoIDDataGridViewTextBoxColumn
            // 
            this.candidatoIDDataGridViewTextBoxColumn.DataPropertyName = "CandidatoID";
            this.candidatoIDDataGridViewTextBoxColumn.HeaderText = "Candidato";
            this.candidatoIDDataGridViewTextBoxColumn.Name = "candidatoIDDataGridViewTextBoxColumn";
            this.candidatoIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.candidatoIDDataGridViewTextBoxColumn.Width = 60;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NombreCandidato";
            this.dataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // organizacionExperienciaDataGridViewTextBoxColumn
            // 
            this.organizacionExperienciaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.organizacionExperienciaDataGridViewTextBoxColumn.DataPropertyName = "OrganizacionExperiencia";
            this.organizacionExperienciaDataGridViewTextBoxColumn.HeaderText = "Organizacion de Experiencia";
            this.organizacionExperienciaDataGridViewTextBoxColumn.Name = "organizacionExperienciaDataGridViewTextBoxColumn";
            this.organizacionExperienciaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaEntradaExperienciaDataGridViewTextBoxColumn
            // 
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn.DataPropertyName = "FechaEntradaExperiencia";
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn.HeaderText = "Fecha de Entrada";
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn.Name = "fechaEntradaExperienciaDataGridViewTextBoxColumn";
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaEntradaExperienciaDataGridViewTextBoxColumn.Width = 120;
            // 
            // fechaSalidaExperienciaDataGridViewTextBoxColumn
            // 
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn.DataPropertyName = "fechaSalidaExperiencia";
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn.HeaderText = "Fecha de Salida";
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn.Name = "fechaSalidaExperienciaDataGridViewTextBoxColumn";
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaSalidaExperienciaDataGridViewTextBoxColumn.Width = 110;
            // 
            // salarioExperienciaDataGridViewTextBoxColumn
            // 
            this.salarioExperienciaDataGridViewTextBoxColumn.DataPropertyName = "SalarioExperiencia";
            this.salarioExperienciaDataGridViewTextBoxColumn.HeaderText = "Salario";
            this.salarioExperienciaDataGridViewTextBoxColumn.Name = "salarioExperienciaDataGridViewTextBoxColumn";
            this.salarioExperienciaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CandidatoID";
            this.dataGridViewTextBoxColumn2.HeaderText = "CandidatoID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // FormExperiencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 660);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtFiltroExperiencia);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormExperiencia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario de Experiencia Laboral";
            this.Load += new System.EventHandler(this.FormExperiencia_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.experienciasBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnVolver;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialFlatButton btnEditar;
        private MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtFiltroExperiencia;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn puestoExperienciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource experienciasBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn posicionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn experienciaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CandidatoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidatoIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn organizacionExperienciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaEntradaExperienciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaSalidaExperienciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salarioExperienciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}