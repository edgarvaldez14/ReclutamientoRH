﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.CRUD;
using ReclutamientoRH.Modelos;

namespace ReclutamientoRH.Formularios
{
    public partial class FormDepartamentos : MaterialForm 
    {
        public string FiltroDep;
        

        public FormDepartamentos()
        {
            InitializeComponent();
        }

        private void Departamentos_Load(object sender, EventArgs e)
        {
            using(DbContextRRHH db = new DbContextRRHH())
            {
                departamentosBindingSource.DataSource = db.Departamentos.ToList();
            }
        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH RH = new FormReclutamientoRH();
            RH.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarDepartamento agr = new AgregarDepartamento();
            agr.Show();
            this.Hide();
        }

        private void txtFiltroDepartamento_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltroDepartamento_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtFiltroDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
           

            if(e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroDepartamento.Text == "")
                    {

                        dataGridView1.DataSource = db.Departamentos.ToList();
                        departamentosBindingSource.DataSource = db.Departamentos.ToList();

                    }
                    else
                    { 
                        FiltroDep = txtFiltroDepartamento.Text;

                
                
                        var result = from d in db.Departamentos
                                     where d.DepartamentoID.ToString() == FiltroDep || d.NombreDepartamento == FiltroDep
                                     select d;
                    

                     dataGridView1.DataSource = result.ToList();
                    }
                }              
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else 
            {

                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                    if (bd.Posiciones.Any(q => q.DepartamentoID == id))
                    {
                        MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                    }
                    else
                    {

                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Departamentos.SingleOrDefault(d => d.DepartamentoID == id);

                            db.Departamentos.Remove(n);

                            db.SaveChanges();

                            dataGridView1.DataSource = db.Departamentos.ToList();
                        }
                    }
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if( dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {            
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarDepartamento dep = new AgregarDepartamento();
                dep.Id = int.Parse(row.Cells[0].Value.ToString());
                dep.Nombre = row.Cells[1].Value.ToString();
                dep.Estado = row.Cells[2].Value.ToString();
                dep.editar = true;
                dep.groupBox1.Text = "Editar Departamento";
                dep.btnAgregar.Text = "Editar";
                dep.Text = "Editar - Departamento";
                dep.txtCodigoDepartamento.Enabled = false;
                dep.Show();
                this.Hide();
            }
        }
    }
}

