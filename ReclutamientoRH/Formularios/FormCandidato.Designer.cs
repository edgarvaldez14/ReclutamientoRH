﻿namespace ReclutamientoRH.Formularios
{
    partial class FormCandidato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnVolver = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEditar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnAgregar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtFiltroCandidato = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.candidatoIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreCandidatoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidosCandidatoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CedulaCandidato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salarioCandidatoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PosicionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CapacitacionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompetenciaID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NacionalidadID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdiomaID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recomendadoPorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoCandidatoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidatosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnContratar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnExperiencias = new MaterialSkin.Controls.MaterialFlatButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.AutoSize = true;
            this.btnVolver.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVolver.Depth = 0;
            this.btnVolver.Location = new System.Drawing.Point(1378, 522);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnVolver.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Primary = false;
            this.btnVolver.Size = new System.Drawing.Size(63, 36);
            this.btnVolver.TabIndex = 13;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Location = new System.Drawing.Point(1235, 522);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(74, 36);
            this.btnEliminar.TabIndex = 12;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.AutoSize = true;
            this.btnEditar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditar.Depth = 0;
            this.btnEditar.Location = new System.Drawing.Point(1168, 522);
            this.btnEditar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEditar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Primary = false;
            this.btnEditar.Size = new System.Drawing.Size(59, 36);
            this.btnEditar.TabIndex = 11;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Location = new System.Drawing.Point(1086, 522);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = false;
            this.btnAgregar.Size = new System.Drawing.Size(74, 36);
            this.btnAgregar.TabIndex = 10;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtFiltroCandidato
            // 
            this.txtFiltroCandidato.Depth = 0;
            this.txtFiltroCandidato.Hint = "";
            this.txtFiltroCandidato.Location = new System.Drawing.Point(979, 73);
            this.txtFiltroCandidato.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtFiltroCandidato.Name = "txtFiltroCandidato";
            this.txtFiltroCandidato.PasswordChar = '\0';
            this.txtFiltroCandidato.SelectedText = "";
            this.txtFiltroCandidato.SelectionLength = 0;
            this.txtFiltroCandidato.SelectionStart = 0;
            this.txtFiltroCandidato.Size = new System.Drawing.Size(200, 23);
            this.txtFiltroCandidato.TabIndex = 9;
            this.txtFiltroCandidato.UseSystemPasswordChar = false;
            this.txtFiltroCandidato.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltroCandidato_KeyPress);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(892, 73);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(81, 19);
            this.materialLabel1.TabIndex = 8;
            this.materialLabel1.Text = "Candidato:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.dataGridView2);
            this.groupBox1.Location = new System.Drawing.Point(3, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1438, 394);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Candidatos";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.candidatoIDDataGridViewTextBoxColumn,
            this.nombreCandidatoDataGridViewTextBoxColumn,
            this.apellidosCandidatoDataGridViewTextBoxColumn,
            this.CedulaCandidato,
            this.salarioCandidatoDataGridViewTextBoxColumn,
            this.PosicionID,
            this.Column1,
            this.dataGridViewTextBoxColumn1,
            this.CapacitacionID,
            this.dataGridViewTextBoxColumn2,
            this.CompetenciaID,
            this.dataGridViewTextBoxColumn3,
            this.NacionalidadID,
            this.dataGridViewTextBoxColumn4,
            this.IdiomaID,
            this.dataGridViewTextBoxColumn5,
            this.recomendadoPorDataGridViewTextBoxColumn,
            this.estadoCandidatoDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.candidatosBindingSource;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView2.Location = new System.Drawing.Point(16, 42);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(1402, 346);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // candidatoIDDataGridViewTextBoxColumn
            // 
            this.candidatoIDDataGridViewTextBoxColumn.DataPropertyName = "CandidatoID";
            this.candidatoIDDataGridViewTextBoxColumn.HeaderText = "Candidato";
            this.candidatoIDDataGridViewTextBoxColumn.Name = "candidatoIDDataGridViewTextBoxColumn";
            this.candidatoIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nombreCandidatoDataGridViewTextBoxColumn
            // 
            this.nombreCandidatoDataGridViewTextBoxColumn.DataPropertyName = "NombreCandidato";
            this.nombreCandidatoDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreCandidatoDataGridViewTextBoxColumn.Name = "nombreCandidatoDataGridViewTextBoxColumn";
            this.nombreCandidatoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // apellidosCandidatoDataGridViewTextBoxColumn
            // 
            this.apellidosCandidatoDataGridViewTextBoxColumn.DataPropertyName = "ApellidosCandidato";
            this.apellidosCandidatoDataGridViewTextBoxColumn.HeaderText = "Apellidos";
            this.apellidosCandidatoDataGridViewTextBoxColumn.Name = "apellidosCandidatoDataGridViewTextBoxColumn";
            this.apellidosCandidatoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // CedulaCandidato
            // 
            this.CedulaCandidato.DataPropertyName = "CedulaCandidato";
            this.CedulaCandidato.HeaderText = "CedulaCandidato";
            this.CedulaCandidato.Name = "CedulaCandidato";
            this.CedulaCandidato.ReadOnly = true;
            // 
            // salarioCandidatoDataGridViewTextBoxColumn
            // 
            this.salarioCandidatoDataGridViewTextBoxColumn.DataPropertyName = "SalarioCandidato";
            this.salarioCandidatoDataGridViewTextBoxColumn.HeaderText = "Salario";
            this.salarioCandidatoDataGridViewTextBoxColumn.Name = "salarioCandidatoDataGridViewTextBoxColumn";
            this.salarioCandidatoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // PosicionID
            // 
            this.PosicionID.DataPropertyName = "PosicionID";
            this.PosicionID.HeaderText = "PosicionID";
            this.PosicionID.Name = "PosicionID";
            this.PosicionID.ReadOnly = true;
            this.PosicionID.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "NombreDepartamento";
            this.Column1.HeaderText = "Departamento";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NombrePosicion";
            this.dataGridViewTextBoxColumn1.HeaderText = "Posicion";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // CapacitacionID
            // 
            this.CapacitacionID.DataPropertyName = "CapacitacionID";
            this.CapacitacionID.HeaderText = "CapacitacionID";
            this.CapacitacionID.Name = "CapacitacionID";
            this.CapacitacionID.ReadOnly = true;
            this.CapacitacionID.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DescripcionCapacitacion";
            this.dataGridViewTextBoxColumn2.HeaderText = "Capacitacion";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // CompetenciaID
            // 
            this.CompetenciaID.DataPropertyName = "CompetenciaID";
            this.CompetenciaID.HeaderText = "CompetenciaID";
            this.CompetenciaID.Name = "CompetenciaID";
            this.CompetenciaID.ReadOnly = true;
            this.CompetenciaID.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "NombreCompetencia";
            this.dataGridViewTextBoxColumn3.HeaderText = "Competencia";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // NacionalidadID
            // 
            this.NacionalidadID.DataPropertyName = "NacionalidadID";
            this.NacionalidadID.HeaderText = "NacionalidadID";
            this.NacionalidadID.Name = "NacionalidadID";
            this.NacionalidadID.ReadOnly = true;
            this.NacionalidadID.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NombreNacionalidad";
            this.dataGridViewTextBoxColumn4.HeaderText = "Nacionalidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // IdiomaID
            // 
            this.IdiomaID.DataPropertyName = "IdiomaID";
            this.IdiomaID.HeaderText = "IdiomaID";
            this.IdiomaID.Name = "IdiomaID";
            this.IdiomaID.ReadOnly = true;
            this.IdiomaID.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "NombreIdioma";
            this.dataGridViewTextBoxColumn5.HeaderText = "Idioma";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // recomendadoPorDataGridViewTextBoxColumn
            // 
            this.recomendadoPorDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.recomendadoPorDataGridViewTextBoxColumn.DataPropertyName = "RecomendadoPor";
            this.recomendadoPorDataGridViewTextBoxColumn.HeaderText = "Recomendado Por";
            this.recomendadoPorDataGridViewTextBoxColumn.Name = "recomendadoPorDataGridViewTextBoxColumn";
            this.recomendadoPorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoCandidatoDataGridViewTextBoxColumn
            // 
            this.estadoCandidatoDataGridViewTextBoxColumn.DataPropertyName = "EstadoCandidato";
            this.estadoCandidatoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoCandidatoDataGridViewTextBoxColumn.Name = "estadoCandidatoDataGridViewTextBoxColumn";
            this.estadoCandidatoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // candidatosBindingSource
            // 
            this.candidatosBindingSource.DataSource = typeof(ReclutamientoRH.Modelos.Candidatos);
            // 
            // btnContratar
            // 
            this.btnContratar.AutoSize = true;
            this.btnContratar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnContratar.Depth = 0;
            this.btnContratar.Location = new System.Drawing.Point(708, 522);
            this.btnContratar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnContratar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnContratar.Name = "btnContratar";
            this.btnContratar.Primary = false;
            this.btnContratar.Size = new System.Drawing.Size(172, 36);
            this.btnContratar.TabIndex = 14;
            this.btnContratar.Text = "Contratar Candidato";
            this.btnContratar.UseVisualStyleBackColor = true;
            this.btnContratar.Click += new System.EventHandler(this.btnContratar_Click);
            // 
            // btnExperiencias
            // 
            this.btnExperiencias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExperiencias.AutoSize = true;
            this.btnExperiencias.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnExperiencias.Depth = 0;
            this.btnExperiencias.Location = new System.Drawing.Point(478, 522);
            this.btnExperiencias.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnExperiencias.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnExperiencias.Name = "btnExperiencias";
            this.btnExperiencias.Primary = false;
            this.btnExperiencias.Size = new System.Drawing.Size(160, 36);
            this.btnExperiencias.TabIndex = 20;
            this.btnExperiencias.Text = "Experiencia Laboral";
            this.btnExperiencias.UseVisualStyleBackColor = true;
            this.btnExperiencias.Click += new System.EventHandler(this.btnExperiencias_Click);
            // 
            // FormCandidato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1458, 620);
            this.Controls.Add(this.btnExperiencias);
            this.Controls.Add(this.btnContratar);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtFiltroCandidato);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormCandidato";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario de Candidatos";
            this.Load += new System.EventHandler(this.FormCandidato_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btnVolver;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialFlatButton btnEditar;
        private MaterialSkin.Controls.MaterialFlatButton btnAgregar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtFiltroCandidato;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn posicionesIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nacionalidadIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacitacionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn competenciaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn experienciaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idiomaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn posicionIDDataGridViewTextBoxColumn;
        private MaterialSkin.Controls.MaterialFlatButton btnContratar;
        private System.Windows.Forms.BindingSource candidatosBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidatoIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreCandidatoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidosCandidatoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CedulaCandidato;
        private System.Windows.Forms.DataGridViewTextBoxColumn salarioCandidatoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PosicionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CapacitacionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompetenciaID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn NacionalidadID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdiomaID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn recomendadoPorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoCandidatoDataGridViewTextBoxColumn;
        private MaterialSkin.Controls.MaterialFlatButton btnExperiencias;
    }
}