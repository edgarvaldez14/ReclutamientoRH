﻿using MaterialSkin.Controls;
using ReclutamientoRH.DAL;
using ReclutamientoRH.Formularios.Create;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReclutamientoRH.Formularios
{
    public partial class FormCompetencias : MaterialForm
    {
        public FormCompetencias()
        {
            InitializeComponent();
        }
        public string FiltroCom;

        private void FormCompetencias_Load(object sender, EventArgs e)
        {
            using (DbContextRRHH db = new DbContextRRHH())
            {
                competenciasBindingSource.DataSource = db.Competencias.ToList();
            }
            
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FormReclutamientoRH rh = new FormReclutamientoRH();
            rh.Show();
            this.Hide();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarCompetencias agr = new AgregarCompetencias();
            agr.Show();
            this.Hide();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione una fila", "ERROR EN LA ACCION");
            }
            else
            {
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                AgregarCompetencias com = new AgregarCompetencias();
                com.id = int.Parse(row.Cells[0].Value.ToString());
                com.des = row.Cells[1].Value.ToString();
                com.est = row.Cells[2].Value.ToString();
                com.editar = true;
                com.groupBox1.Text = "Editar Idioma";
                com.txtCodigoCompetencia.Enabled = false;
                com.Text = "Editar Idioma";
                com.btnAgregar.Text = "Editar";
                com.Show();
                this.Hide();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("ESTA SEGURO QUE DESEA CREAR ESTA ACCION", "Confirmacion", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Por favor Seleccione Una fila", "ERROR EN LA ACCION DE ELIMINAR");
            }
            else
            {
                DbContextRRHH bd = new DbContextRRHH();
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                int id = int.Parse(row.Cells[0].Value.ToString());
                if (bd.Candidatos.Any(q => q.CompetenciaID == id))
                {
                    MessageBox.Show("Este Registro Tiene Depedencia, No se puede eliminar", " ERROR AL ELIMINAR");
                }
                else
                {
                        using (DbContextRRHH db = new DbContextRRHH())
                        {
                            var n = db.Competencias.SingleOrDefault(d => d.CompetenciaID == id);

                            db.Competencias.Remove(n);

                            db.SaveChanges();

                            dataGridView1.DataSource = db.Competencias.ToList();
                        }
                    }
                }
            }
        }

        private void txtFiltroCompetencia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                using (DbContextRRHH db = new DbContextRRHH())
                {
                    if (txtFiltroCompetencia.Text == "")
                    {

                        dataGridView1.DataSource = db.Competencias.ToList();
                        competenciasBindingSource.DataSource = db.Competencias.ToList();

                    }
                    else
                    {
                        FiltroCom = txtFiltroCompetencia.Text;



                        var result = from d in db.Competencias
                                     where d.CompetenciaID.ToString() == FiltroCom || d.NombreCompetencia == FiltroCom
                                     select d;


                        dataGridView1.DataSource = result.ToList();
                    }
                }
            }
        }
    }
}
