namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH04 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Empleados", "EstadoEmpleado", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Empleados", "EstadoEmpleado", c => c.Boolean(nullable: false));
        }
    }
}
