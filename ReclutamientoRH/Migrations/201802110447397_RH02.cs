namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidatos", "ExperienciaID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Candidatos", "ExperienciaID");
        }
    }
}
