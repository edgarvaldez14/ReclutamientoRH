namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH06 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidatos", "CedulaCandidato", c => c.Long(nullable: false));
            AddColumn("dbo.Empleados", "CedulaEmpleado", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empleados", "CedulaEmpleado");
            DropColumn("dbo.Candidatos", "CedulaCandidato");
        }
    }
}
