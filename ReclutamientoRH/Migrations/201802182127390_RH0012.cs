namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH0012 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empleados", "NacionalidadIDEmpleado", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empleados", "NacionalidadIDEmpleado");
        }
    }
}
