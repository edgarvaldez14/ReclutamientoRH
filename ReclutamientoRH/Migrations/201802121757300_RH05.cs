namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH05 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Empleados", "Candidato_CandidatoID", "dbo.Candidatos");
            DropIndex("dbo.Empleados", new[] { "Candidato_CandidatoID" });
            DropColumn("dbo.Empleados", "Candidato_CandidatoID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Empleados", "Candidato_CandidatoID", c => c.Int());
            CreateIndex("dbo.Empleados", "Candidato_CandidatoID");
            AddForeignKey("dbo.Empleados", "Candidato_CandidatoID", "dbo.Candidatos", "CandidatoID");
        }
    }
}
