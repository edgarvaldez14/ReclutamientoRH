namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH031 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Empleados", "CandidatoID", "dbo.Candidatos");
            DropIndex("dbo.Empleados", new[] { "CandidatoID" });
            RenameColumn(table: "dbo.Empleados", name: "CandidatoID", newName: "Candidato_CandidatoID");
            AlterColumn("dbo.Empleados", "Candidato_CandidatoID", c => c.Int());
            CreateIndex("dbo.Empleados", "Candidato_CandidatoID");
            AddForeignKey("dbo.Empleados", "Candidato_CandidatoID", "dbo.Candidatos", "CandidatoID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empleados", "Candidato_CandidatoID", "dbo.Candidatos");
            DropIndex("dbo.Empleados", new[] { "Candidato_CandidatoID" });
            AlterColumn("dbo.Empleados", "Candidato_CandidatoID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Empleados", name: "Candidato_CandidatoID", newName: "CandidatoID");
            CreateIndex("dbo.Empleados", "CandidatoID");
            AddForeignKey("dbo.Empleados", "CandidatoID", "dbo.Candidatos", "CandidatoID", cascadeDelete: true);
        }
    }
}
