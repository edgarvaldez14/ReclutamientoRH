namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidatos",
                c => new
                    {
                        CandidatoID = c.Int(nullable: false),
                        NombreCandidato = c.String(),
                        ApellidosCandidato = c.String(),
                        SalarioCandidato = c.Int(nullable: false),
                        RecomendadoPor = c.String(),
                        EstadoCandidato = c.String(),
                        NacionalidadID = c.Int(nullable: false),
                        CapacitacionID = c.Int(nullable: false),
                        CompetenciaID = c.Int(nullable: false),
                        IdiomaID = c.Int(nullable: false),
                        PosicionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CandidatoID)
                .ForeignKey("dbo.Capacitaciones", t => t.CapacitacionID, cascadeDelete: true)
                .ForeignKey("dbo.Competencias", t => t.CompetenciaID, cascadeDelete: true)
                .ForeignKey("dbo.Posiciones", t => t.PosicionID, cascadeDelete: true)
                .ForeignKey("dbo.Idiomas", t => t.IdiomaID, cascadeDelete: true)
                .ForeignKey("dbo.Nacionalidades", t => t.NacionalidadID, cascadeDelete: true)
                .Index(t => t.NacionalidadID)
                .Index(t => t.CapacitacionID)
                .Index(t => t.CompetenciaID)
                .Index(t => t.IdiomaID)
                .Index(t => t.PosicionID);
            
            CreateTable(
                "dbo.Empleados",
                c => new
                    {
                        EmpleadoID = c.Int(nullable: false),
                        CandidatoID = c.Int(nullable: false),
                        NombreEmpleado = c.String(),
                        ApellidoEmpleado = c.String(),
                        NacionalidadIDEmpleado = c.Int(nullable: false),
                        FechaContratacionEmpleado = c.DateTime(nullable: false),
                        PosicionID = c.Int(nullable: false),
                        SalarioEmpleado = c.Int(nullable: false),
                        EstadoEmpleado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EmpleadoID)
                .ForeignKey("dbo.Candidatos", t => t.CandidatoID, cascadeDelete: true)
                .Index(t => t.CandidatoID);
            
            CreateTable(
                "dbo.Experiencias",
                c => new
                    {
                        ExperienciaID = c.Int(nullable: false),
                        CandidatoID = c.Int(nullable: false),
                        OrganizacionExperiencia = c.String(),
                        PosicionID = c.Int(nullable: false),
                        FechaEntradaExperiencia = c.DateTime(nullable: false),
                        fechaSalidaExperiencia = c.DateTime(nullable: false),
                        SalarioExperiencia = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ExperienciaID)
                .ForeignKey("dbo.Candidatos", t => t.CandidatoID, cascadeDelete: true)
                .Index(t => t.CandidatoID);
            
            CreateTable(
                "dbo.Capacitaciones",
                c => new
                    {
                        CapacitacionID = c.Int(nullable: false),
                        DescripcionCapacitacion = c.String(),
                        NivelCapacitacion = c.String(),
                        FechaDeInicioCapacitacion = c.DateTime(nullable: false),
                        FechaFinCapacitacion = c.DateTime(nullable: false),
                        InstitucionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CapacitacionID)
                .ForeignKey("dbo.Instituciones", t => t.InstitucionID, cascadeDelete: true)
                .Index(t => t.InstitucionID);
            
            CreateTable(
                "dbo.Instituciones",
                c => new
                    {
                        InstitucionID = c.Int(nullable: false),
                        NombreInstitucion = c.String(),
                        PaisInstitucion = c.String(),
                        ProvinciaInstitucion = c.String(),
                        MunicipioInstitucion = c.String(),
                        DireccionInstitucion = c.String(),
                    })
                .PrimaryKey(t => t.InstitucionID);
            
            CreateTable(
                "dbo.Competencias",
                c => new
                    {
                        CompetenciaID = c.Int(nullable: false),
                        NombreCompetencia = c.String(),
                        EstadoCompetencia = c.String(),
                    })
                .PrimaryKey(t => t.CompetenciaID);
            
            CreateTable(
                "dbo.Departamentos",
                c => new
                    {
                        DepartamentoID = c.Int(nullable: false),
                        NombreDepartamento = c.String(),
                        EstadoDepartamento = c.String(),
                    })
                .PrimaryKey(t => t.DepartamentoID);
            
            CreateTable(
                "dbo.Posiciones",
                c => new
                    {
                        PosicionID = c.Int(nullable: false),
                        NombrePosicion = c.String(),
                        RiesgoPosicion = c.String(),
                        SalarioMinimoPosicion = c.Int(nullable: false),
                        SalaroMaximoPosicion = c.Int(nullable: false),
                        EstadoPosicion = c.String(),
                        DepartamentoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PosicionID)
                .ForeignKey("dbo.Departamentos", t => t.DepartamentoID, cascadeDelete: true)
                .Index(t => t.DepartamentoID);
            
            CreateTable(
                "dbo.Idiomas",
                c => new
                    {
                        IdiomaID = c.Int(nullable: false),
                        NombreIdioma = c.String(),
                        EstadoIdioma = c.String(),
                    })
                .PrimaryKey(t => t.IdiomaID);
            
            CreateTable(
                "dbo.Nacionalidades",
                c => new
                    {
                        NacionalidadID = c.Int(nullable: false),
                        NombreNacionalidad = c.String(),
                    })
                .PrimaryKey(t => t.NacionalidadID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Usuario = c.String(),
                        Contrasenia = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Candidatos", "NacionalidadID", "dbo.Nacionalidades");
            DropForeignKey("dbo.Candidatos", "IdiomaID", "dbo.Idiomas");
            DropForeignKey("dbo.Posiciones", "DepartamentoID", "dbo.Departamentos");
            DropForeignKey("dbo.Candidatos", "PosicionID", "dbo.Posiciones");
            DropForeignKey("dbo.Candidatos", "CompetenciaID", "dbo.Competencias");
            DropForeignKey("dbo.Capacitaciones", "InstitucionID", "dbo.Instituciones");
            DropForeignKey("dbo.Candidatos", "CapacitacionID", "dbo.Capacitaciones");
            DropForeignKey("dbo.Experiencias", "CandidatoID", "dbo.Candidatos");
            DropForeignKey("dbo.Empleados", "CandidatoID", "dbo.Candidatos");
            DropIndex("dbo.Posiciones", new[] { "DepartamentoID" });
            DropIndex("dbo.Capacitaciones", new[] { "InstitucionID" });
            DropIndex("dbo.Experiencias", new[] { "CandidatoID" });
            DropIndex("dbo.Empleados", new[] { "CandidatoID" });
            DropIndex("dbo.Candidatos", new[] { "PosicionID" });
            DropIndex("dbo.Candidatos", new[] { "IdiomaID" });
            DropIndex("dbo.Candidatos", new[] { "CompetenciaID" });
            DropIndex("dbo.Candidatos", new[] { "CapacitacionID" });
            DropIndex("dbo.Candidatos", new[] { "NacionalidadID" });
            DropTable("dbo.Users");
            DropTable("dbo.Nacionalidades");
            DropTable("dbo.Idiomas");
            DropTable("dbo.Posiciones");
            DropTable("dbo.Departamentos");
            DropTable("dbo.Competencias");
            DropTable("dbo.Instituciones");
            DropTable("dbo.Capacitaciones");
            DropTable("dbo.Experiencias");
            DropTable("dbo.Empleados");
            DropTable("dbo.Candidatos");
        }
    }
}
