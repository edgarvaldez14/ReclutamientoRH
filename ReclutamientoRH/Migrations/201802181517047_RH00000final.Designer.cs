// <auto-generated />
namespace ReclutamientoRH.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class RH00000final : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(RH00000final));
        
        string IMigrationMetadata.Id
        {
            get { return "201802181517047_RH00000final"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
