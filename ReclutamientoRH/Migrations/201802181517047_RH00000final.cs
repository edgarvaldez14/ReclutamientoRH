namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH00000final : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Empleados", "NacionalidadIDEmpleado");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Empleados", "NacionalidadIDEmpleado", c => c.Int(nullable: false));
        }
    }
}
