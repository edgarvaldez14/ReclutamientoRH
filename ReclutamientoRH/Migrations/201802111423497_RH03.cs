namespace ReclutamientoRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RH03 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Candidatos", "ExperienciaID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Candidatos", "ExperienciaID", c => c.Int(nullable: false));
        }
    }
}
